from collections import OrderedDict
from configparser import ConfigParser
from os import name as os_name
from pathlib import Path

from numpy import sort as np_sort, array

from src.fme import fme_command
from src.lastools import lastool_command
from src.python import python_command


def workflow_parser(workflow_config_filepath, env_object,
                    source_arg, working_arg):
    """
    Parses a workflow configuration file.

    Parameters
    ----------
    workflow_config_filepath : str
        Filepath of a workflow configuration INI file
    env_object : src.utilities.run_environment.RetrieveRunEnv
        An object containing software paths as attributes
    source_arg : str or None
        Path to source folder if it was specified as a CLI arg, or None if not
    working_arg : str or None
        Path to working folder if it was specified as a CLI arg, or None if not

    Returns
    -------
    dict
        A dictionary of processing-step-definition dictionaries
    list
        A list of the external applications that this processing
        run depends on
    list
        A list of all directories used by this processing run,
        except for the pointcloud source directory
    str
        The directory to be used as working space for this processing run
    str
        The directory with the point cloud files to be processed
    bool
        Overrides the retention behavior specified in individual steps
    float
        The specification of a custom hang check interval

    """

    # Validation
    posix = os_name == 'posix'
    workflow_config_filepath_path = Path(workflow_config_filepath)
    if not workflow_config_filepath_path.is_file():
        raise Exception(f'{workflow_config_filepath} does not exist.')
    if workflow_config_filepath[-4:] != '.ini':
        raise Exception(f'{workflow_config_filepath} is not an INI file.')

    # Read workflow configuration file
    config = ConfigParser()
    config.read(workflow_config_filepath)

    # Parse path section
    try:
        paths_key = config.sections()[0]
        if paths_key != 'Paths':
            raise Exception('First section must be named "[Paths]".')
        else:
            config_paths = dict(config['Paths'])
            if source_arg:
                source_folder = str(source_arg)
                print(f'SourceFolder (command line): {source_folder}\n')
            else:
                try:
                    source_folder = config_paths.pop('sourcefolder')
                    print(f'SourceFolder (workflow INI): {source_folder}\n')
                except:
                    raise Exception('If the SourceFolder is not specified '
                                    'as a command-line argument it must be '
                                    'specified in the Paths section of the '
                                    'workflow INI')
            if working_arg:
                working_folder_path = Path(working_arg)
                print(f'WorkingFolder (command line): {working_folder_path}\n')
            else:
                try:
                    working_folder_path = \
                        Path(config_paths.pop('workingfolder'))
                    print(f'WorkingFolder (workflow '
                          f'INI): {working_folder_path}\n')
                except:
                    raise Exception('If the WorkingFolder is not specified '
                                    'as a command-line argument it must be '
                                    'specified in the Paths section of the '
                                    'workflow INI')

            # TODO: Change remaining path parameters to also have CLI option
            try:
                if config_paths.pop('retainall').lower() == 'false':
                    retain_all = False
                else:
                    retain_all = True
            except:
                retain_all = True
            print(f'Retain All: {retain_all}')
            try:
                hang_check_interval = \
                    float(config_paths.pop('hangcheckinterval'))
            except:
                hang_check_interval = 30  # minutes
            if not posix:
                print(f'Hangcheck interval: {hang_check_interval}')
    except:
        raise Exception('First section must be named "[Paths]".')

    # Parse processing steps
    section_keys = config.sections()[1:]
    if section_keys:
        if section_keys[0] != '1':
            raise Exception('Section numbering must start with 1.')
        section_numbers = \
            list(np_sort(array([int(section_key)
                                for section_key in section_keys])))
        if (len(section_numbers) != 1) \
                and (section_numbers != list(range(section_numbers[0],
                                                   section_numbers[-1] + 1))):
            raise Exception('There is a missing section number.')
    else:
        raise Exception('There must be at least one processing step defined.')

    cli_dict = OrderedDict()
    application_list = []
    dir_list = []
    for section_number in section_numbers:
        working_dict = dict(config[str(section_number)])
        pipeline = None  # For PDAL steps; needed in `cli_dict` for all steps
        try:
            module = working_dict.pop('module')
            tool = working_dict.pop('submodule')
        except:
            raise Exception(f'Section {section_number} is missing required '
                            f'Module and/or Submodule parameters')

        # Set input folder
        if section_number == 1:
            input_dir = source_folder
        elif tool in ['SetDeliveryTifCRS']:
            try:
                crs_output_dir = cli_dict[
                    int(working_dict.pop('crsdir').lower())
                ]['output_dir']
                input_dir = str(working_folder_path / crs_output_dir)
            except:
                input_dir = source_folder  # TODO: Message user about this
        elif tool == 'Tile_Delivery_Imagery_TIF':
            input_dir = str(working_folder_path / 'delivery')
        elif tool in ['CalcDeliveryTifStats',
                      'CalcDeliveryTifPyramids']:
            input_dir = None
        else:
            try:
                input_dir_value = int(working_dict.pop('inputdir').lower())
                if input_dir_value == 0:
                    input_dir = source_folder
                else:
                    input_dir = str(working_folder_path /
                                    cli_dict[input_dir_value]['output_dir'])
            except:
                step_output_dir = cli_dict[(section_number-1)]['output_dir']
                input_dir = str(working_folder_path / step_output_dir)
                # TODO: Message user about this

        # Set output folder
        if tool in ['SetDeliveryTifCRS',
                    'Tile_Delivery_Imagery_TIF',
                    'CalcDeliveryTifStats',
                    'CalcDeliveryTifPyramids']:
            output_dir = str(working_folder_path / 'delivery')
        elif tool in ['RenameRaster', 'lasindex']:
            output_dir = None
        else:
            try:
                if working_dict.pop('outputdir').lower() == 'deliveryfolder':
                    output_dir = str(working_folder_path / 'delivery')
            except:
                output_dir = str(working_folder_path / f'step_{section_number}')
                try:
                    result_name = working_dict.pop('resultname')
                    output_dir = output_dir + '_' + result_name
                except:
                    pass  # TODO: Message user about this

        # Set result retention
        try:
            if working_dict.pop('retainresult').lower() == 'true':
                retain_result = True
            else:
                retain_result = False
        except:
            retain_result = False  # TODO: Message user about this

        if module == 'LAStools':
            input_filter = working_dict.pop('inputfilter')
            if tool not in ['blast2dem', 'las2dem', 'lasindex']:
                try:
                    compress_output = working_dict.pop('compressoutput')
                except:
                    compress_output = 'True'  # TODO: Message user about this
            else:
                compress_output = 'False'
            try:
                cores = int(working_dict.pop('cores'))  # TODO: Message user about this
            except:
                cores = int(env_object.cores)
            if tool in ['lasindex', 'lastile'] and posix and cores > 1:
                print(f'WARNING Step {section_number}: {tool} cannot '
                      f'currently be multiprocessed on Linux; running as '
                      f'cores=1')
                cores = 1
            other_args = []
            for key, value in working_dict.items():
                key = key.split('|', 1)[1] if '|' in key else key
                other_args.append('-' + key + ' ' + value.strip('"').strip("'"))
            other_args.append(f'-cores {cores}')
            # Any unneeded parameters will be used for lastools arguments,
            # so check that none of the known list remain
            shared_parameters = [
                'Module'.lower(),
                'Submodule'.lower(),
                'InputFilter'.lower(),
                'InputDir'.lower(),
                'OutputDir'.lower(),
                'CompressOutput'.lower(),
                'ResultName'.lower(),
                'InputIndex'.lower(),
                'ImageryFile'.lower(),
                'RetainResult'.lower(),
                'Pipeline'.lower(),
                'HeightBreak'.lower(),
                'CellSize'.lower(),
            ]
            working_dict_keys = working_dict.keys()
            for shared_parameter in shared_parameters:
                if shared_parameter in working_dict_keys:
                    raise Exception(f"The {tool} tool does not require "
                                    f"the {shared_parameter} parameter; "
                                    f"please remove it in step "
                                    f"{section_number}'s definition.")

            cli_string = lastool_command(
                tool=tool,
                input_filter=input_filter,
                input_dir=input_dir,
                output_dir=output_dir,
                other_args=other_args,
                compress_output=compress_output,
            )
            application = str(Path(env_object.lastools_bin) / f'{tool}.exe')

        elif module == 'FileManagement':
            if tool in ['MultiprocessLidR']:
                if not posix:
                    raise Exception("The MultiprocessLidR submodule only "
                                    "runs on Linux.")
            if tool in ['MultiprocessFusion']:
                if posix:
                    raise Exception("The MultiprocessFusion submodule only "
                                    "runs on Windows.")
            if tool in ['QC_Pointclouds']:
                try:
                    input_filter = working_dict.pop('inputfilter')
                except:
                    raise Exception("The QC_Pointclouds submodule requires "
                                    "the InputFilter parameter.")
            else:
                input_filter = None
            if tool in ['RenameRaster']:
                try:
                    input_filename = working_dict.pop('inputfilename')
                    output_filename = working_dict.pop('outputfilename')
                    if input_filename.split('.')[-1] != \
                            output_filename.split('.')[-1]:
                        raise Exception("The filename must be renamed with the "
                                        "same file extension.")
                except:
                    raise Exception("The RenameRaster submodule requires "
                                    "InputFilename and OutputFilename "
                                    "parameters.")
            else:
                input_filename = None
                output_filename = None
            if tool in ['MultiprocessPDAL']:
                try:
                    pipeline = working_dict.pop('pipeline')
                except:
                    raise Exception("The MultiprocessPDAL submodule "
                                    "requires a Pipeline parameter.")
            if tool in ['MultiprocessFusion']:
                try:
                    heightbreak = working_dict.pop('heightbreak')
                except:
                    raise Exception("The MultiprocessFusion submodule "
                                    "requires a HeightBreak parameter.")
            else:
                heightbreak = None
            if tool in ['MultiprocessFusion', 'MultiprocessLidR']:
                try:
                    cellsize = working_dict.pop('cellsize')
                except:
                    raise Exception("The MultiprocessFusion and "
                                    "MultiprocessLidR submodules "
                                    "requires a CellSize parameter.")
            else:
                cellsize = None
            if tool in ['QC_Pointclouds', 'MultiprocessPDAL',
                        'MultiprocessFusion', 'MultiprocessLidR']:
                try:
                    cores = working_dict.pop('cores')
                except:
                    cores = env_object.cores
            else:
                cores = None
            if tool in ['QC_Pointclouds']:
                try:
                    index_dir = \
                        cli_dict[
                            int(working_dict.pop('inputindex').lower())
                        ]['output_dir']
                except:
                    index_dir = None
            else:
                index_dir = None

            if tool in ['QC_Pointclouds', 'MultiprocessPDAL',
                        'MultiprocessFusion', 'MultiprocessLidR',
                        'CopyFiles', 'CopyFolder']:
                application = env_object.dataprep_python_exe
            else:
                application = env_object.arcpy_exe

            cli_string = python_command(
                tool=tool,
                input_dir=input_dir,
                output_dir=output_dir,
                config_paths=env_object,
                input_filter=input_filter,
                input_filename=input_filename,
                output_filename=output_filename,
                cores=cores,
                heightbreak=heightbreak,
                cellsize=cellsize,
                pipeline=pipeline,
                index_dir=index_dir
            )

        elif module == 'FME':
            if tool in ['Tile_Delivery_Imagery_TIF']:
                try:
                    imagery_file = working_dict.pop('imageryfile').lower()
                    if imagery_file not in [
                        'dem_', 'dtm_', 'dsm_', 'dsm_first_only',
                        'dsm_spike_free', 'ndsm_first_only',
                        'ndsm_spike_free', 'intensity_'
                    ]:
                        raise Exception("The imagery file for tiling must be "
                                        "one of: "
                                        "dem_, dtm_, dsm_, dsm_first_only, "
                                        "dsm_spike_free, ndsm_first_only, "
                                        "ndsm_spike_free, intensity_")
                except:
                    raise Exception("The ImageryFile parameter is required "
                                    "for the FME imagery tiling tool.")
            else:
                imagery_file = None
            if tool in ['Tile_Pointclouds',
                        'Tile_Delivery_Imagery_TIF']:
                try:
                    index_dir = \
                        cli_dict[
                            int(working_dict.pop('inputindex').lower())
                        ]['output_dir']
                except:
                    raise Exception("The InputIndex parameter, set to the "
                                    "step number with the pointcloud index, "
                                    "is required for the FME tiling tools.")
            else:
                index_dir = None

            cli_string = fme_command(
                tool=tool,
                input_dir=input_dir,
                output_dir=output_dir,
                imagery_file=imagery_file,
                index_dir=index_dir,
            )
            application = env_object.fme_exe

        else:
            raise Exception(f'Unknown module "{module}" specified in the '
                            'workflow configuration.')

        cli_dict.update({section_number: {'application': application,
                                          'cli_string': cli_string,
                                          'module': module,
                                          'tool': tool,
                                          'pipeline': pipeline,
                                          'input_dir': input_dir,
                                          'output_dir': output_dir,
                                          'retain_result': retain_result,
                                          }})

        if application not in application_list:
            application_list.append(application)
        if input_dir not in dir_list and input_dir is not None:
            dir_list.append(input_dir)
        if output_dir not in dir_list and output_dir is not None:
            dir_list.append(output_dir)

    dir_list.remove(source_folder)

    return cli_dict, application_list, dir_list, str(working_folder_path), \
        str(source_folder), retain_all, hang_check_interval
