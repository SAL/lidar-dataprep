from io import StringIO
from os import kill
from platform import system
from subprocess import run, PIPE
from sys import argv

from pandas import read_csv


def task_killer(task_to_kill):
    """
    Kills all Windows processes with an image name that contains a
    specified (case insensitive) string.

    Parameters
    ----------
    task_to_kill : str
        Name of an operating system process

    Returns
    -------
    str

    """

    if system() != 'Windows':
        raise Exception('This code is only to be used on Windows.')

    tasklist_columns = ['Image Name', 'PID']

    process_tasklist = run('tasklist', stdout=PIPE, universal_newlines=True)
    tasklist_list = StringIO(process_tasklist.stdout).readlines()
    tasklist_exe_list = [task for task in tasklist_list if '.exe' in task]
    tasklist_exe_str = ''.join(tasklist_exe_list)

    tasklist_df = read_csv(
        StringIO(tasklist_exe_str),
        delim_whitespace=True,
        names=tasklist_columns,
        header=None,
        usecols=[0, 1]
    )

    tasks = tasklist_df['Image Name']
    selected_tasks = list(set([task for task in tasks
                               if task_to_kill.lower() in task.lower()]))
    tasklist_selection_df = \
        tasklist_df.loc[tasklist_df['Image Name'].isin(selected_tasks)]

    results = ''
    for index, row in tasklist_selection_df.iterrows():
        try:
            kill(row['PID'], 9)
            results += f'Killed task {row["Image Name"]} - pid {row["PID"]}\n'
        except:
            results += f'Tried, but could not kill task ' \
                       f'{row["Image Name"]} - pid {row["PID"]}\n'

    return results


if __name__ == '__main__':

    if argv.__len__() == 2:
        task_name = argv[1]
    else:
        raise Exception('A task name is the required argument.')

    results = task_killer(task_name)

    print(results)
