from configparser import ConfigParser
from os import name as os_name
if os_name == 'posix':
    from os import uname
from pathlib import Path
from tempfile import TemporaryDirectory
from textwrap import dedent

# TODO: Validate existence of specified executables and paths
# TODO: Add method to report these at runtime, and to add to report log
# TODO: Make a subdirectory temp folder for VACC, so whole temp directory
#  gets deleted each run and so file from last run can't be used by next run


class RetrieveRunEnv:

    def __init__(self):
        self.posix = None
        self.system = None
        self.node = None

        self.codebase_root = None
        self.software_paths_ini = None

        self.fme_exe = None
        self.lastools_bin = None
        self.arcpy_exe = None
        self.pdal_exe = None
        self.fusion_folder = None
        self.dataprep_python_exe = None
        self.lidr_env = None
        self.temp_path_str = None
        self.wine_exe = None
        self.cores = None
        self.remote_test_data = None

        self.temp_folder_object = None

        self.define_codebase()
        self.define_system()
        self.read_paths()

    def __repr__(self):
        repr_str = dedent(f"""
        Detected runtime system: {self.system}
        Runtime system is a Posix system? {self.posix}
        Runtime node (Posix systems only): {self.node}
        Codebase root folder: {str(self.codebase_root)}
        Software paths and processes (defined at {str(self.software_paths_ini)}):
          FME executable: {self.fme_exe}
          LAStools bin folder: {self.lastools_bin}
          ArcGIS python executable: {self.arcpy_exe}
          PDAL executable: {self.pdal_exe}
          FUSION folder: {self.fusion_folder}
          Dataprep python executable: {self.dataprep_python_exe}
          lidR environment: {self.lidr_env}
          Temp folder (VACC only): {self.temp_path_str}
          Wine executable (Posix systems only): {self.wine_exe}
          Parallel processes: {str(self.cores)}
          Remote test data: {str(self.remote_test_data)}
        """)
        return repr_str

    def cleanup_temp_folder_object(self):
        if self.system in ['Windows', 'VM']:
            self.temp_folder_object.cleanup()

    def define_codebase(self):
        self.codebase_root = Path(__file__).parents[2]
        self.software_paths_ini = self.codebase_root / 'software_paths.ini'
        if not self.software_paths_ini.is_file():
            raise Exception('`software_paths.ini` does not exist '
                            'in codebase root. Copy, and update as '
                            'necessary, from `software_paths_template.ini`')

    def define_system(self):
        self.posix = os_name == 'posix'
        self.node = uname().nodename if self.posix else None
        if self.posix:
            if 'localdomain' in self.node:
                self.system = 'VM'
            else:
                self.system = 'VACC'
        else:
            self.system = 'Windows'

    def read_paths(self):

        # Read software paths configuration file
        software_paths_parser = ConfigParser()
        software_paths_parser.read(self.software_paths_ini)

        # Parse paths
        software_paths_dict = dict(software_paths_parser[self.system])
        self.fme_exe = software_paths_dict['fme']
        self.lastools_bin = software_paths_dict['lastools']
        self.arcpy_exe = software_paths_dict['arcpython']
        self.pdal_exe = software_paths_dict['pdal']
        self.fusion_folder = software_paths_dict['fusion']
        self.dataprep_python_exe = software_paths_dict['datapreppython']
        self.lidr_env = software_paths_dict['lidr']
        self.temp_path_str = software_paths_dict['tempspace']
        self.wine_exe = software_paths_dict['wine']
        self.cores = software_paths_dict['parallelprocesses']
        self.remote_test_data = software_paths_dict['remotetestdata']

    def setup_temp_folder_object(self):
        if self.system in ['Windows', 'VM']:
            if self.temp_path_str == 'None':
                self.temp_folder_object = TemporaryDirectory()
                self.temp_path_str = self.temp_folder_object.name
