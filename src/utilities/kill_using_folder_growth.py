from os import scandir
from os.path import abspath, dirname, getsize
from sys import argv, path
from time import sleep

codebase_root = dirname(dirname(dirname(abspath(__file__))))
path.append(codebase_root)
from src.utilities.task_killer import task_killer


def kill_using_folder_growth(folder, task, interval):
    """
    Spawns a process that monitors, on a specified interval, the size of
    specified folder in order to detect when it starts growing in size and
    then when it stops changing in size it kills the specified operating
    system task feeding it. For use in killing orphaned processes holding
    up workflow execution.

    Parameters
    ----------
    folder : str
        A path string
    task : str
        Name of an operating system process
    interval : float
        Monitoring interval in minutes

    Returns
    -------
    str

    """

    last_folder_size = 0
    while True:
        sleep(float(interval) * 60)
        folder_size = 0
        with scandir(folder) as items:
            for entry in items:
                if not entry.name.startswith('.') and entry.is_file():
                    folder_size += getsize(entry.path)
        size_delta = folder_size - last_folder_size

        # For development of slow growth killing and for manually testing
        # that the monitoring process is being spawned
        # with open('size_monitoring.txt', 'a') as f:
        #     f.write('{} {} {}\n'.format(folder, folder_size, size_delta))

        if folder_size > 0:
            if size_delta == 0:
                results = task_killer(task)
                return results
            else:
                last_folder_size = int(folder_size)


if __name__ == '__main__':

    if argv.__len__() == 4:
        folder = argv[1]
        task = argv[2]
        interval = argv[3]

    else:
        raise Exception('(Folder, task, interval) are the required arguments.')

    results = kill_using_folder_growth(folder, task, interval)

    print(results)
