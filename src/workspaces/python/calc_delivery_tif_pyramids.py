from os import walk
from os.path import join
from sys import argv

from arcpy import (env, BuildPyramids_management, GetMessages, ExecuteError,
                   Describe)


def calc_delivery_tif_pyramids(output_dir):
    """
    Calculates pyramids for TIF raster files.

    Parameters
    ----------
    output_dir : str
        Directory of files to process

    Returns
    ------
    None

    """

    env.workspace = output_dir
                                                             
    delivery_files = []                                      
    for (dirpath, dirnames, filenames) in walk(output_dir):
        for file in filenames:                               
            delivery_files.extend([join(dirpath, file)])
    for delivery_file in delivery_files:                     
        if delivery_file.endswith('tif'):
            dsc = Describe(str(delivery_file))
            coord_sys = dsc.spatialReference
            coord_sys_name = coord_sys.name
            print(f'CRS read by Arc: {coord_sys_name}')
            if coord_sys_name == 'Unknown':
                raise Exception("CRS could not be read by Arc, exiting")
            try:
                BuildPyramids_management(delivery_file)
                print('Calculating pyramids for: ', delivery_file)
                print(GetMessages(0))
            except ExecuteError:
                print(GetMessages(2))
            except Exception as ex:                          
                print(ex.args[0])                            


if __name__ == '__main__':

    if argv.__len__() == 2:
        output_dir = argv[1]
    else:
        raise Exception('Output_dir is the required argument.')

    calc_delivery_tif_pyramids(output_dir)
