from multiprocessing.pool import ThreadPool
from os import name as os_name
if os_name == 'posix':
    from os import uname
from pathlib import Path
from shlex import split
from subprocess import Popen, PIPE
from sys import argv


def call_process(cmd):
    posix = os_name == 'posix'
    p = Popen(split(cmd, posix=posix),
              stdout=PIPE,
              stderr=PIPE,
              universal_newlines=True)
    stdout, stderr = p.communicate()
    return stdout, stderr


def multiprocess_fusion(input_dir, output_dir, cores,
                        heightbreak, cellsize, fusion_exe):
    """
    Multi-processes the Fusion GridMetrics tool over all files in the
    input directory with output going to another (existing) directory.

    Parameters
    ----------
    input_dir : str
    output_dir : str
    cores : str
    heightbreak : str
    cellsize : str
    fusion_exe : str

    Returns
    -------
    None

    """

    filepaths = sorted(Path(input_dir).glob('*'))

    pool = ThreadPool(int(cores))
    print(f'Running on {cores} parallel processes')
    results = []

    posix = os_name == 'posix'
    node = uname().nodename if posix else None
    if posix:
        wine_str = '/gpfs1/arch/x86_64-rhel7/wine-7.1/bin/wine64 ' \
            if 'vacc' in node else 'wine '
    else:
        wine_str = ''

    for filepath in filepaths:
        cmd_string = f'{wine_str}{fusion_exe} ' \
                     f'/noground ' \
                     f'/minht:2 ' \
                     f'{heightbreak} ' \
                     f'{cellsize} ' \
                     f'{str(Path(output_dir) / filepath.stem)} ' \
                     f'{str(filepath)}'
        results.append(pool.apply_async(call_process, [cmd_string]))

    # Close the pool and wait for each running task to complete
    pool.close()
    pool.join()

    # Report messages from all processes
    for result in results:
        process_out, process_error = result.get()
        if process_out:
            print('Communication from the process on '
                  'the stdout pipe:\n', process_out)
        if process_error:
            print('Communication from the process on '
                  'the stderr pipe:\n', process_error)


if __name__ == '__main__':
    if argv.__len__() == 7:
        input_dir_input = argv[1]
        output_dir_input = argv[2]
        cores_input = argv[3]
        heightbreak_input = argv[4]
        cellsize_input = argv[5]
        fusion_exe_input = argv[6]
    else:
        raise Exception('Input_dir, output_dir, cores, heightbreak, cellsize, '
                        'fusion_exe are the required arguments.')

    multiprocess_fusion(
        input_dir_input,
        output_dir_input,
        cores_input,
        heightbreak_input,
        cellsize_input,
        fusion_exe_input
    )
