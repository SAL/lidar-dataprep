from distutils.dir_util import copy_tree
from sys import argv


def copy_files(input_dir, output_dir):
    """
    Copies all files from one (existing) directory to another
    (existing) directory.

    Parameters
    ----------
    input_dir : str
        The source directory
    output_dir : str
        The target directory

    Returns
    -------
    None

    """

    copy_tree(input_dir, output_dir)


if __name__ == '__main__':

    if argv.__len__() == 3:
        input_dir = argv[1]
        output_dir = argv[2]
    else:
        raise Exception('Input_dir, output_dir are the required arguments.')

    copy_files(input_dir, output_dir)
