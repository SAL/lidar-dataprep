from datetime import datetime as dt
from pathlib import Path
from shutil import copy
from sys import argv, path

from arcpy import (env, Describe, GetMessages, ExecuteError,
                   DefineProjection_management)

codebase_root = Path(__file__).parents[3]
path.append(str(codebase_root))
from src.run_subprocess import run_subprocess
from src.utilities.run_environment import RetrieveRunEnv


def set_delivery_tif_crs(input_dir, output_dir, lastools_path):
    """
    Sets the coordinate reference system (CRS) of TIF raster files to
    match that of the point cloud files used to create them.

    Parameters
    ----------
    input_dir : str
        The directory containing the point cloud files with the
        CRS information
    output_dir : str
        The directory of raster files that will have their CRS set
    lastools_path : str
        The directory containing the las2las executable

    Returns
    -------
    None

    """
    filesize_threshold_for_CRS_extraction = 2000  # bytes

    run_env = RetrieveRunEnv()
    run_env.setup_temp_folder_object()
    env.workspace = output_dir
    input_path = Path(input_dir)
    filepaths = sorted(input_path.glob('*.las'))
    filepaths.extend(sorted(input_path.glob('*.laz')))

    timestamp = dt.now().strftime("%Y%m%d%H%M%S")
    for filepath in filepaths:
        if filepath.stat().st_size > filesize_threshold_for_CRS_extraction:
            if filepath.suffix == '.las':
                filepath_with_CRS = \
                    Path(run_env.temp_path_str) / \
                    f'{filepath.stem}_{timestamp}.las'
                copy(filepath, filepath_with_CRS)
                break
            if filepath.suffix == '.laz':
                print('Converting LAZ file to LAS for Arc, in '
                      'a temporary directory')
                filepath_with_CRS = \
                    Path(run_env.temp_path_str) / \
                    f'{filepath.stem}_{timestamp}.las'
                las2las_command = f'-i {filepath} -o {filepath_with_CRS} ' \
                                  f'-cores 1'
                print(f'las2las command: {las2las_command}')
                application_path = Path(lastools_path) / 'las2las.exe'
                process_out, process_error, process_time_minutes = \
                    run_subprocess(str(application_path),
                                   las2las_command,
                                   None, None, input_dir, None, None)
                break
    else:
        raise Exception('No suitable LiDAR file for extracting a CRS was found')
    print(f'CRS source file: {filepath_with_CRS}')

    try:
        dsc = Describe(str(filepath_with_CRS))
        coord_sys = dsc.spatialReference
        coord_sys_name = coord_sys.name
        print(f'CRS read by Arc: {coord_sys_name}')
        print(GetMessages(0))
    except ExecuteError:
        raise Exception(GetMessages(2))
    except Exception as ex:
        raise Exception(ex.args[0])

    if run_env.temp_folder_object:
        run_env.cleanup_temp_folder_object()
    else:
        filepath_with_CRS.unlink()

    if coord_sys_name == 'Unknown':
        raise Exception("CRS could not be read by Arc, exiting")

    delivery_filepaths = sorted(Path(output_dir).rglob('*.tif'))
    for delivery_filepath in delivery_filepaths:
        try:
            DefineProjection_management(str(delivery_filepath), coord_sys)
            print(f'Defining CRS for: {delivery_filepath.name}')
            print(GetMessages(0))
        except ExecuteError:
            raise Exception(GetMessages(2))
        except Exception as ex:
            raise Exception(ex.args[0])


if __name__ == '__main__':

    if argv.__len__() == 4:
        input_dir = argv[1]
        output_dir = argv[2]
        lastools_path = argv[3]
    else:
        raise Exception('Input_dir, output_dir, lastools_path are '
                        'the required arguments.')

    set_delivery_tif_crs(
        input_dir,
        output_dir,
        lastools_path
    )
