from sys import argv

from arcpy import env, Rename_management, GetMessages, ExecuteError


def rename_raster(input_dir, input_filename, output_filename):
    """
    Renames a raster file.

    Parameters
    ----------
    input_dir : str
        The raster source directory
    input_filename : str
        The file name of the raster to be renamed
    output_filename : str
        The new name of the raster file

    Returns
    -------
    None

    """

    env.workspace = input_dir

    try:
        Rename_management(input_filename, output_filename)
        print(GetMessages(0))
    except ExecuteError:
        print(GetMessages(2))
    except Exception as ex:
        print(ex.args[0])


if __name__ == '__main__':

    if argv.__len__() == 4:
        input_dir = argv[1]
        input_filename = argv[2]
        output_filename = argv[3]
    else:
        raise Exception('Input_dir, input_filename, output_filename are the '
                        'required arguments.')

    rename_raster(
        input_dir,
        input_filename,
        output_filename
    )
