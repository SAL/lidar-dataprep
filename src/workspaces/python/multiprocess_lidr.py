from multiprocessing.pool import ThreadPool
from os import name as os_name
if os_name == 'posix':
    from os import uname
from pathlib import Path
from shlex import split
from subprocess import Popen, PIPE
from sys import argv


def call_process(cmd):
    posix = os_name == 'posix'
    p = Popen(split(cmd, posix=posix),
              stdout=PIPE,
              stderr=PIPE,
              universal_newlines=True)
    stdout, stderr = p.communicate()
    return stdout, stderr


def multiprocess_lidr(input_dir, output_dir, cellsize, cores, lidr_env):
    """
    Multi-processes the running of lidR metrics over all files
    in the input directory with output going to another (existing) directory.

    Parameters
    ----------
    input_dir : str
    output_dir : str
    cellsize : str
    cores : str
    lidr_env : str

    Returns
    -------
    None

    """
    filepaths = sorted(Path(input_dir).glob('*.laz'))

    pool = ThreadPool(int(cores))
    print(f'Running on {cores} parallel processes')
    results = []

    posix = os_name == 'posix'
    node = uname().nodename if posix else None

    lidr_env_path = Path(lidr_env)
    env_path_parts = lidr_env_path.parts
    env_name_index = env_path_parts.index('envs') + 1
    env_name = env_path_parts[env_name_index]
    env_path_parts_count = len(env_path_parts)
    miniconda_path = \
        lidr_env_path.parents[env_path_parts_count - env_name_index]
    if posix:
        conda_exe = miniconda_path / 'bin/conda'
    else:
        raise Exception('Not configured for Windows')

    lidr_script = Path(__file__).parents[1] / 'r' / 'run_lidr_metrics.r'
    for filepath in filepaths:
        cmd_string = f'{conda_exe} run -n {env_name} --live-stream ' \
                     f'Rscript {lidr_script} ' \
                     f'-i {input_dir} ' \
                     f'-f {filepath.stem} ' \
                     f'-o {output_dir} ' \
                     f'-c {cellsize} '
        results.append(pool.apply_async(call_process, [cmd_string]))

    # Close the pool and wait for each running task to complete
    pool.close()
    pool.join()

    # Report messages from all processes
    for result in results:
        process_out, process_error = result.get()
        if process_out:
            print('Communication from the process '
                  'on the stdout pipe:\n', process_out)
        if process_error:
            print('Communication from the process '
                  'on the stderr pipe:\n', process_error)


if __name__ == '__main__':
    if argv.__len__() == 6:
        input_dir_input = argv[1]
        output_dir_input = argv[2]
        cellsize_input = argv[3]
        cores_input = argv[4]
        lidr_env_input = argv[5]
    else:
        raise Exception('Input_dir, output_dir, cellsize, cores, lidr_env '
                        'are the required arguments.')

    multiprocess_lidr(
        input_dir_input,
        output_dir_input,
        cellsize_input,
        cores_input,
        lidr_env_input
    )
