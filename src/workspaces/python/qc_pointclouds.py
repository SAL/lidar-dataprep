from re import compile, DOTALL, split
from pathlib import Path
from sys import argv, path

from numpy import nan, int64
from osgeo import ogr
from pandas import DataFrame, concat, isna

codebase_root = Path(__file__).parents[3]
path.append(str(codebase_root))
from src.lastools import lastool_command
from src.run_subprocess import run_subprocess


def qc_pointclouds(input_dir, input_filter, cores, output_dir, lastools_path,
                   index_dir=None, debug=False):
    """
    Reports the lasinfo output for each pointcloud file, summarizes values
    for the set of files, and performs and reports on some QC operations
    (check if LASversion/CRS match, value statistics, missing
    classifications, etc.)

    Parameters
    ----------
    input_dir : str
        The pointcloud source directory
    input_filter : str
        The pointcloud file type: 'las' or 'laz'
    cores : str
        The number of cores to use for lasinfo processing
    output_dir : str
        The directory in which to save output of this tool
    lastools_path : str
        The directory containing the lasinfo executable
    index_dir : str or None
        The directory containing the point cloud index shapefile to join
        the metrics to; if None, no joining will be attempted
    debug : bool
        If `True`, the lasinfo step is skipped, and existing lasinfo reports
        are expected in `output_dir`

    Returns
    -------
    None

    """
    if debug:
        print('Debug mode - skipping lasinfo.exe step, '
              'existing lasinfo reports are expected in `output_dir`')
    else:
        if 'lasinfo' in input_dir.lower():
            raise Exception("The phrase 'lasinfo' cannot be in the path "
                            "of the files to be processed.")

        print('\nProducing lasinfo reports for each file -- '
              'see result folder for progress')
        application_path = Path(lastools_path) / 'lasinfo.exe'
        args = ['-cores', str(cores), '-otxt',
                '-compute_density', '-no_check_outside']
        lasinfo_command = \
            lastool_command('lasinfo', input_filter, input_dir, output_dir,
                            other_args=args, compress_output='False')
        run_subprocess(str(application_path), lasinfo_command,
                       None, None, input_dir, None, None)

    pattern_warning = compile('WARNING.*?\n')
    pattern_filename = compile(r"report for '(\S+\.\w+)'")
    pattern_version = compile(r"version major.minor.+?(\d*\.\d+|\d)")
    pattern_classification = \
        compile(r".+?(\d+).+?([\w+\s]+).+?(\d+)", DOTALL)
    pattern_extended_classification = \
        compile(r".+?(\d+).+?extended classification.+?\((\d+)\)", DOTALL)
    pattern_area = compile(r"covered area.+?(\d*\.\d+|\d+)")
    pattern_density = \
        compile(r"point density: "
                r"all returns.+?(\d*\.\d+|\d+).+?last only.+?(\d*\.\d+|\d+)"
                r".+?spacing: "
                r"all returns.+?(\d*\.\d+|\d+).+?last only.+?(\d*\.\d+|\d+)",
                DOTALL)
    pattern_vlr_projection = compile(
        r"variable length header.+?"
        r"user ID.+?LASF_Projection.+?"
        r"description.+?'(.+?)'"
        r"[\r\n]+([^\r\n]+)"
        r"[\r\n]+([^\r\n]+)",
        DOTALL
    )
    pattern_evlr_projection = compile(
        r"extended variable length header.+?"
        r"user ID.+?LASF_Projection.+?"
        r"description.+?'(.+?)'"
        r"[\r\n]+([^\r\n]+)"
        r"[\r\n]+([^\r\n]+)",
        DOTALL
    )
    pattern_x_min_max = compile(r"X\s+([\d-]+)\s+([\d-]+)\s")
    pattern_y_min_max = compile(r"Y\s+([\d-]+)\s+([\d-]+)\s")
    pattern_z_min_max = compile(r"Z\s+([\d-]+)\s+([\d-]+)\s")
    pattern_scale_factors = \
        compile(r"scale factor x y z:\s+([\d.]+)\s+([\d.]+)\s+([\d.]+)\s")
    pattern_offsets = \
        compile(r"offset x y z:\s+([\d.-]+)\s+([\d.-]+)\s+([\d.-]+)\s")

    output_path = Path(output_dir)
    filepaths = sorted(output_path.glob('*.txt'))
    print('\nProcessing LASinfo output files:')

    report_df = DataFrame()
    class_columns = set()
    for filepath in filepaths:
        filename = filepath.stem
        print(filename)
        try:
            with open(filepath, 'rb') as f:
                single_file_output_raw = f.read().decode(errors='replace')

            # Extract warnings from file (so they don't mess up other
            #   extractions) and report to stdout
            warnings = pattern_warning.findall(single_file_output_raw)
            single_file_output = single_file_output_raw
            for warning in warnings:
                print(warning.strip())
                single_file_output = single_file_output.replace(warning, '', 1)

            match_filename = \
                pattern_filename.search(single_file_output).groups()
            result_filename = Path(match_filename[0]).stem

            match_version = pattern_version.search(single_file_output).groups()
            result_version = float(match_version[0])

            match_area = pattern_area.search(single_file_output).groups()
            result_area_in_square_units = float(match_area[0])

            match_density = pattern_density.search(single_file_output).groups()
            result_density_all = float(match_density[0])
            result_density_last = float(match_density[1])
            result_spacing_all = float(match_density[2])
            result_spacing_last = float(match_density[3])

            classification_section_raw = \
                split("histogram of classification of points:",
                      single_file_output)[-1]
            classification_section = classification_section_raw.split("+")[0]
            match_classification = \
                pattern_classification.findall(classification_section)
            classification_df = \
                DataFrame(match_classification,
                          columns=[filename, 'description', 'class_number'])
            try:
                extended_classification_section = \
                    classification_section_raw.split('histogram of extended '
                                                     'classification of '
                                                     'points:')[1]
                match_extended_classification = \
                    pattern_extended_classification.findall(
                        extended_classification_section
                    )
                extended_classification_df = \
                    DataFrame(match_extended_classification,
                              columns=[filename, 'class_number'])
                extended_classification_df['description'] = \
                    'extended classification'
                classification_df = \
                    classification_df.append(extended_classification_df,
                                             ignore_index=True)
            except:
                extended_classification_df = DataFrame()
            classification_df.set_index(['class_number', 'description'],
                                        inplace=True)
            result_classification_df = classification_df.T.astype(int64)
            result_classification_df['total points'] = \
                result_classification_df.sum(axis=1)
            result_classification_df.columns = \
                [' '.join(col).strip()
                 for col in result_classification_df.columns.values]
            class_columns = \
                class_columns | set(result_classification_df.columns)

            try:
                match_vlr_projection = \
                    pattern_vlr_projection.findall(single_file_output)
                result_vlr_projection = match_vlr_projection[-1]
                result_vlr_projection_description = result_vlr_projection[0]
                result_vlr_projection_definition = \
                    result_vlr_projection[1].lstrip().rstrip() + ' ' \
                    + result_vlr_projection[2].lstrip().rstrip()
            except:
                result_vlr_projection_description = None
                result_vlr_projection_definition = None

            try:
                match_evlr_projection = \
                    pattern_evlr_projection.findall(single_file_output)
                result_evlr_projection = match_evlr_projection[-1]
                result_evlr_projection_description = result_evlr_projection[0]
                result_evlr_projection_definition = \
                    result_evlr_projection[1].lstrip().rstrip() + ' ' \
                    + result_evlr_projection[2].lstrip().rstrip()
            except:
                result_evlr_projection_description = None
                result_evlr_projection_definition = None

            x_min_max = pattern_x_min_max.search(single_file_output).groups()
            x_min, x_max = int(x_min_max[0]), int(x_min_max[1])
            y_min_max = pattern_y_min_max.search(single_file_output).groups()
            y_min, y_max = int(y_min_max[0]), int(y_min_max[1])
            z_min_max = pattern_z_min_max.search(single_file_output).groups()
            z_min, z_max = int(z_min_max[0]), int(z_min_max[1])
            scale_factors = \
                pattern_scale_factors.search(single_file_output).groups()
            scale_factor_x = float(scale_factors[0])
            scale_factor_y = float(scale_factors[1])
            scale_factor_z = float(scale_factors[2])
            offsets = pattern_offsets.search(single_file_output).groups()
            offset_x = float(offsets[0])
            offset_y = float(offsets[1])
            offset_z = float(offsets[2])
            x_min_coord = round((x_min * scale_factor_x) + offset_x)
            y_min_coord = round((y_min * scale_factor_y) + offset_y)
            z_min_coord = round((z_min * scale_factor_z) + offset_z)
            x_max_coord = round((x_max * scale_factor_x) + offset_x)
            y_max_coord = round((y_max * scale_factor_y) + offset_y)
            z_max_coord = round((z_max * scale_factor_z) + offset_z)
            x_range_coord = x_max_coord - x_min_coord
            y_range_coord = y_max_coord - y_min_coord
            z_range_coord = z_max_coord - z_min_coord
            additional_parameter_values = [result_version,
                                           result_density_all,
                                           result_density_last,
                                           result_spacing_all,
                                           result_spacing_last,
                                           result_area_in_square_units,
                                           result_vlr_projection_description,
                                           result_vlr_projection_definition,
                                           result_evlr_projection_description,
                                           result_evlr_projection_definition,
                                           x_min_coord,
                                           x_max_coord,
                                           x_range_coord,
                                           y_min_coord,
                                           y_max_coord,
                                           y_range_coord,
                                           z_min_coord,
                                           z_max_coord,
                                           z_range_coord]

        except:
            print("File could not be read and analyzed; File may only be a "
                  "header with no points, or the expected lasinfo pattern "
                  "may have changed.")
            # Write a blank record to the quality report
            additional_parameter_values = [nan] * 6 + [None] * 4 + [nan] * 9
            result_classification_df = DataFrame()

        additional_parameters_dict = {
            'version': 'float',
            'point density': 'float',
            'pulse density': 'float',
            'point spacing': 'float',
            'pulse spacing': 'float',
            'area': 'float',
            'VLR projection description': 'str',
            'VLR projection definition': 'str',
            'EVLR projection description': 'str',
            'EVLR projection definition': 'str',
            'X min': 'float',
            'X max': 'float',
            'X range': 'float',
            'Y min': 'float',
            'Y max': 'float',
            'Y range': 'float',
            'Z min': 'float',
            'Z max': 'float',
            'Z range': 'float'
        }
        additional_parameters_df = \
            DataFrame(index=[filename],
                      data=[additional_parameter_values],
                      columns=additional_parameters_dict.keys())
        additional_parameters_df.astype(additional_parameters_dict)
        report_onefile_df = concat([additional_parameters_df,
                                    result_classification_df], axis=1)
        report_df = report_df.append(report_onefile_df)

    report_df.sort_index(inplace=True)
    columns_to_check_match = ['version',
                              'VLR projection description',
                              'VLR projection definition',
                              'EVLR projection description',
                              'EVLR projection definition']
    match_check_df = DataFrame(index=['Files all match? (includes missing)'],
                               columns=columns_to_check_match)
    for col in columns_to_check_match:
        match = report_df[col].astype(str).nunique(dropna=False) == 1
        match_check_df.loc['Files all match? (includes missing)', col] = \
            str(match)
    for col in class_columns:
        match = report_df[col].count() == report_df[col].size
        match_check_df.loc['Files all match? (includes missing)', col] = \
            str(match)

    point_totals_df = \
        report_df[list(class_columns)].sum().astype(int64).to_frame(
            name='Point totals').T
    point_percent_of_total_df = \
        point_totals_df / list(point_totals_df['total points'].values)[0] * 100
    point_percent_of_total_df.rename(
        index={'Point totals': 'Percentage of total points'}, inplace=True)
    point_percent_of_total_df.drop(columns=['total points'], inplace=True)

    point_stats_df = report_df.describe(include='all')
    point_stats_df.drop(index=['25%', '75%'], inplace=True)
    point_stats_df.rename(index={'50%': 'median'}, inplace=True)

    report_df = report_df.append(DataFrame(index=[None]))
    report_df = report_df.append(point_totals_df)
    report_df = report_df.append(point_percent_of_total_df)
    report_df = report_df.append(DataFrame(index=[None]))
    report_df = report_df.append(match_check_df)
    report_df = report_df.append(DataFrame(index=[None]))
    report_df = report_df.append(DataFrame(index=['Detailed stats (ignores '
                                                  'missing):']))
    report_df = report_df.append(point_stats_df)

    class_columns_sorted = list(class_columns)
    class_columns_sorted.sort()
    class_columns_sorted.sort(key=lambda x: len(x.split()[0]))
    report_columns_nonclass = [
        'version',
        'VLR projection description',
        'VLR projection definition',
        'EVLR projection description',
        'EVLR projection definition',
        'point density',
        'pulse density',
        'point spacing',
        'pulse spacing',
        'area',
        'X min',
        'X max',
        'X range',
        'Y min',
        'Y max',
        'Y range',
        'Z min',
        'Z max',
        'Z range',
    ]
    report_columns_sorted = report_columns_nonclass + class_columns_sorted
    report_df = report_df[report_columns_sorted]
    report_df.to_csv(output_path / 'QC_of_pointcloud_tiles.csv')

    # Join report columns to shapefile, if specified
    if index_dir and Path(index_dir).is_dir():
        field_dict = {
            'version': {'field_name': 'las_vers', 'field_type': ogr.OFTReal},
            'VLR projection description': {'field_name': 'vlr_desc',
                                           'field_type': ogr.OFTString},
            'VLR projection definition': {'field_name': 'vlr_def',
                                          'field_type': ogr.OFTString},
            'EVLR projection description': {'field_name': 'evlr_desc',
                                            'field_type': ogr.OFTString},
            'EVLR projection definition': {'field_name': 'evlr_def',
                                           'field_type': ogr.OFTString},
            'point density': {'field_name': 'point_dens',
                              'field_type': ogr.OFTReal},
            'pulse density': {'field_name': 'pulse_dens',
                              'field_type': ogr.OFTReal},
            'point spacing': {'field_name': 'point_spac',
                              'field_type': ogr.OFTReal},
            'pulse spacing': {'field_name': 'pulse_spac',
                              'field_type': ogr.OFTReal},
            'area': {'field_name': 'area', 'field_type': ogr.OFTReal},
            'X min': {'field_name': 'x_min', 'field_type': ogr.OFTReal},
            'X max': {'field_name': 'x_max', 'field_type': ogr.OFTReal},
            'X range': {'field_name': 'x_range', 'field_type': ogr.OFTReal},
            'Y min': {'field_name': 'y_min', 'field_type': ogr.OFTReal},
            'Y max': {'field_name': 'y_max', 'field_type': ogr.OFTReal},
            'Y range': {'field_name': 'y_range', 'field_type': ogr.OFTReal},
            'Z min': {'field_name': 'z_min', 'field_type': ogr.OFTReal},
            'Z max': {'field_name': 'z_max', 'field_type': ogr.OFTReal},
            'Z range': {'field_name': 'z_range', 'field_type': ogr.OFTReal},
            'total points': {'field_name': 'total_pnts',
                             'field_type': ogr.OFTInteger64},
        }
        in_file = Path(index_dir) / 'pointcloud_tile_index.shp'
        out_file = \
            Path(output_dir) / 'pointcloud_tile_index_with_quality_metrics.shp'
        in_ds = ogr.Open(str(in_file), update=False)
        in_lyr = in_ds.GetLayer()
        driver = ogr.GetDriverByName("ESRI Shapefile")
        out_ds = driver.CreateDataSource(str(out_file))
        out_lyr = out_ds.CreateLayer(str(out_file.with_suffix('')),
                                     in_lyr.GetSpatialRef(),
                                     ogr.wkbPolygon)

        # Copy the schema, then features, of the original shapefile
        #   to the destination shapefile
        lyr_def = in_lyr.GetLayerDefn()
        for i in range(lyr_def.GetFieldCount()):
            out_lyr.CreateField(lyr_def.GetFieldDefn(i))
        for feature in in_lyr:
            out_lyr.CreateFeature(feature)

        # Add new fields for the quality metrics
        xwalk_dict = dict()
        for new_field in report_df.columns:
            new_field_dict = field_dict.get(new_field, None)
            if new_field_dict:
                new_field_name = new_field_dict['field_name']
                new_field_type = new_field_dict['field_type']
            else:
                point_class = new_field.split()[0]
                new_field_name = 'points_' + point_class.zfill(3)
                new_field_type = ogr.OFTInteger64
            xwalk_dict[new_field] = new_field_name
            new_field_ogr = ogr.FieldDefn(new_field_name, new_field_type)
            out_lyr.CreateField(new_field_ogr)

        # Write quality metrics to the new fields
        for feature in out_lyr:
            shp_filename = feature.GetField('Name')
            for report_name, shapefile_name in xwalk_dict.items():
                if shp_filename in report_df.index.values:
                    report_value = report_df.loc[shp_filename, report_name]
                    if isna(report_value) and report_value is not None:
                        feature.SetField(shapefile_name, 0)
                    else:
                        feature.SetField(shapefile_name, report_value)
                    out_lyr.SetFeature(feature)


if __name__ == '__main__':

    if argv.__len__() in [6, 7, 8]:
        input_dir = argv[1]
        input_filter = argv[2]
        cores = argv[3]
        output_dir = argv[4]
        lastools_path = argv[5]

        if argv.__len__() == 8:
            index_dir = argv[6]
            debug = bool(argv[7])
            qc_pointclouds(
                input_dir,
                input_filter,
                cores,
                output_dir,
                lastools_path,
                index_dir,
                debug
            )
        elif argv.__len__() == 7:
            index_dir = argv[6]
            qc_pointclouds(
                input_dir,
                input_filter,
                cores,
                output_dir,
                lastools_path,
                index_dir
            )
        else:
            qc_pointclouds(
                input_dir,
                input_filter,
                cores,
                output_dir,
                lastools_path
            )
    else:
        raise Exception('Input_dir, input_filter, cores, output_dir, '
                        'lastools_path are the required arguments.')
