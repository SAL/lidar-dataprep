from distutils.dir_util import copy_tree
from os.path import basename, join
from sys import argv


def copy_folder(input_dir, output_dir):
    """
    Copies an (existing) directory (and all files in it) to
    another (existing) directory.

    Parameters
    ----------
    input_dir : str
        The source directory
    output_dir : str
        The target directory

    Returns
    -------
    None

    """

    folder_name = basename(input_dir)
    copy_tree(input_dir, join(output_dir, folder_name))


if __name__ == '__main__':

    if argv.__len__() == 3:
        input_dir = argv[1]
        output_dir = argv[2]
    else:
        raise Exception('Input_dir, output_dir are the required arguments.')

    copy_folder(input_dir, output_dir)
