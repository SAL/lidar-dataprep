from multiprocessing.pool import ThreadPool
from os import name as os_name
from pathlib import Path
from shlex import split
from subprocess import Popen, PIPE
from sys import argv


def call_process(cmd):
    posix = os_name == 'posix'
    p = Popen(split(cmd, posix=posix),
              stdout=PIPE,
              stderr=PIPE,
              universal_newlines=True)
    stdout, stderr = p.communicate()
    return stdout, stderr


def multiprocess_pdal(input_dir, pipeline_json_file, output_dir,
                      cores, pdal_exe):
    """
    Multi-processes a PDAL pipeline over all files in the input directory
    with output going to another (existing) directory.

    Parameters
    ----------
    input_dir : str
    pipeline_json_file : str
    output_dir : str
    cores : str
    pdal_exe : str

    Returns
    -------
    None

    """
    filepaths = sorted(Path(input_dir).glob('*'))

    pool = ThreadPool(int(cores))
    print(f'Running on {cores} parallel processes')
    results = []
    for filepath in filepaths:
        cmd_string = f'{pdal_exe} ' \
                     f'translate ' \
                     f'{str(filepath)} ' \
                     f'{str(Path(output_dir) / filepath.name)} ' \
                     f'--json {pipeline_json_file}'
        results.append(pool.apply_async(call_process, [cmd_string]))

    # Close the pool and wait for each running task to complete
    pool.close()
    pool.join()

    # Report messages from all processes
    for result in results:
        process_out, process_error = result.get()
        if process_out:
            print('Communication from the process on '
                  'the stdout pipe:\n', process_out)
        if process_error:
            print('Communication from the process on '
                  'the stderr pipe:\n', process_error)


if __name__ == '__main__':
    if argv.__len__() == 6:
        input_dir_input = argv[1]
        pipeline_json_file_input = argv[2]
        output_dir_input = argv[3]
        cores_input = argv[4]
        pdal_exe_input = argv[5]
    else:
        raise Exception('Input_dir, pipeline_json_file, output_dir, cores, '
                        'pdal_exe are the required arguments.')

    multiprocess_pdal(
        input_dir_input,
        pipeline_json_file_input,
        output_dir_input,
        cores_input,
        pdal_exe_input
    )
