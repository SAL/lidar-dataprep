from multiprocessing.pool import ThreadPool
from os import environ
from pathlib import Path
from shlex import split
from subprocess import Popen, PIPE, TimeoutExpired
from tempfile import TemporaryDirectory
from timeit import default_timer as timer
from sys import path

codebase_root = Path(__file__).parents[1]
path.append(str(codebase_root))
from src.utilities.run_environment import RetrieveRunEnv


def call_process_lastools(cmd):
    with TemporaryDirectory() as temp_directory:
        p = Popen(split(cmd, posix=True),
                  cwd=temp_directory,
                  stdout=PIPE,
                  stderr=PIPE,
                  universal_newlines=True)
        stdout, stderr = p.communicate()
    return stdout, stderr


def filter_log_messages(msg, posix):
    """
    Filters log messages of nuisance warnings.

    Parameters
    ----------
    msg : str
        Log message to be filtered
    posix : bool

    Returns
    -------
    str

    """

    # TODO: Switch to logging, and handle this the proper way - this way slows
    #  down the logging

    # List of nuisance warnings substrings
    #  Any message line containing one of these substrings will be filtered
    #  out of the log message.
    msg_items_to_filter = [
        ':err:wincodecs:PngEncoder_CreateInstance',
        ':fixme:ole:CoCreateInstanceEx',
        ':err:menubuilder:convert_to_native_icon error',
        ':err:svchost:AddServiceElem failed to load library',
        ':err:svchost:wmain Failed to load requested group',
        ':err:service:process_send_command service protocol error',
        ':fixme:service:scmdatabase_autostart_services Auto-start service',
        ':err:ntoskrnl:ZwLoadDriver failed to create driver',
        ':err:ntdll:RtlLeaveCriticalSection section',
        ':err:menubuilder:init_xdg error looking up the desktop directory',
        ]
    if posix:
        msg_split = msg.split('\n')
        msg_split_filtered = []
        for msg_line in msg_split:
            msg_line_stripped = msg_line.strip()
            if msg_line_stripped:
                filter_hit = False
                for filter_item in msg_items_to_filter:
                    if filter_item in msg_line_stripped:
                        filter_hit = True
                        break
                if not filter_hit:
                    msg_split_filtered.append(msg_line_stripped)
        msg_filtered = ''
        for msg_line_out in msg_split_filtered:
            msg_filtered += f'{msg_line_out.strip()}\n'
        return msg_filtered.strip()
    else:
        return msg.strip()


def run_subprocess(application, step_command, step_submodule,
                   step_outputdir, step_inputdir,
                   hang_check_interval, working_folder):
    """
    Runs a command string in a subprocess instance.

    Parameters
    ----------
    application : str
        Full path to application executable
    step_command : str
        Command string to send to the process running the executable
    step_submodule : str or None
        Name of the submodule tool; if this is None and the module is
        LAStools, the process monitor will not be run
    step_outputdir : str
        Full path to the step's output folder
    step_inputdir : str
        Full path to the step's input folder
    hang_check_interval : float
        Interval, in minutes, at which to check for hung LAStools processes
    working_folder : str
        Full path to the working folder for the run that contains this
        tool's processing, to be used as the location for the kill report
        that will be created if any LAStools processes hung and were killed

    Returns
    -------
    str
        The stdout captured from the process
    str
        The stderr captured from the process
    float
        The processing time in minutes
    """

    start_timing = timer()
    run_env = RetrieveRunEnv()  # TODO: Replace this call with function args
    output_reported_individually = False

    if 'python' in application.lower():
        application_path = Path(application)
        env_path_parts = application_path.parts
        env_name_index = env_path_parts.index('envs') + 1
        env_name = env_path_parts[env_name_index]
        print(f'Subprocessing on conda environment: {env_name}')
        env_path_parts_count = len(env_path_parts)
        miniconda_path = \
            application_path.parents[env_path_parts_count - env_name_index]
        if run_env.posix:
            conda_exe = miniconda_path / 'bin/conda'
            cmd = f'{conda_exe} run -n {env_name} python {step_command}'
            cmd_split = split(cmd, posix=True)
            env = dict(environ.copy())
        else:
            if 'ArcGIS' in env_path_parts:
                cmd_split = f'{run_env.arcpy_exe} {step_command}'
            else:
                conda_exe = miniconda_path / r'Scripts\conda.exe'
                cmd_split = \
                    f'"{conda_exe}" run -n {env_name} python {step_command}'
            env = None
        process = Popen(
            cmd_split,
            env=env,
            stdin=PIPE,
            stdout=PIPE,
            stderr=PIPE,
            universal_newlines=True,
        )
        process_out, process_error = process.communicate()

    elif 'lastools' in application.lower():

        if step_submodule and hang_check_interval and not run_env.posix:
            if step_submodule == 'lasindex':
                folder_to_monitor = step_inputdir
            else:
                folder_to_monitor = step_outputdir
            root_filepath = Path(__file__).parent
            monitor_cmd = f'{run_env.dataprep_python_exe} {root_filepath}' \
                          r'\utilities\kill_using_folder_growth.py ' \
                          f'{folder_to_monitor} {step_submodule} ' \
                          f'{hang_check_interval}'
            print(f'Starting monitoring process: {monitor_cmd}')
            monitor = Popen(
                monitor_cmd,
                stdout=PIPE,
                stderr=PIPE,
                universal_newlines=True
            )

        with TemporaryDirectory() as temp_directory:
            if run_env.posix:
                output_reported_individually = True
                application = application[:-4] + '64.exe'
                wine_str = run_env.wine_exe
                step_input_path = Path(step_inputdir)
                step_command_split = split(step_command, posix=True)
                is_single_file = False

                single_only = False
                if step_submodule in ['lasindex', 'lastile']:
                    single_only = True

                if not single_only and '-i' in step_command_split:
                    index_to_drop = step_command_split.index('-i')
                    input_filepath = Path(step_command_split[index_to_drop+1])
                    is_single_file = input_filepath.stem != '*'
                    input_filter = input_filepath.suffix
                    del step_command_split[index_to_drop:index_to_drop+2]
                if '-cores' in step_command_split:
                    index_to_drop = step_command_split.index('-cores')
                    cores = int(step_command_split[index_to_drop+1])
                    del step_command_split[index_to_drop:index_to_drop + 2]
                step_command_cleaned = ' '.join(step_command_split)

                if single_only:  # Single-threaded processing
                    print(f'Running as a single-core process, currently all that '
                          f'is possible for this tool on Linux-Wine-LAStools.')
                    cmd_string = f'{wine_str} {application} ' \
                                 f'{step_command_cleaned}'
                    process = Popen(
                        split(cmd_string, posix=True),
                        cwd=temp_directory,
                        stdout=PIPE,
                        stderr=PIPE,
                        universal_newlines=True)
                    process_out, process_error = process.communicate()
                    process_error = filter_log_messages(process_error,
                                                        run_env.posix)
                else:  # Multi-threaded processing
                    pool = ThreadPool(cores)
                    print(f'Running on {cores} parallel processes')
                    results = []
                    if is_single_file:
                        filepaths = [input_filepath]
                    else:
                        filepaths = sorted(step_input_path.glob(f'*{input_filter}'))
                    for filepath in filepaths:
                        cmd_string = f'{wine_str} {application} ' \
                                     f'{step_command_cleaned} ' \
                                     f'-i {str(filepath)}'
                        results.append(pool.apply_async(call_process_lastools,
                                                        [cmd_string]))

                    # Close the pool and wait for each running task to complete
                    pool.close()
                    pool.join()

                    # Report messages from all processes
                    process_out, process_error = '', ''
                    for result in results:
                        process_out_each, process_error_each = result.get()
                        if process_out_each:
                            print('Communication from the process '
                                  'on the stdout pipe:\n', process_out_each)
                            process_out = process_out + '\n' + process_out_each
                        process_error_each = \
                            filter_log_messages(process_error_each, run_env.posix)
                        if process_error_each:
                            print('Communication from the process '
                                  'on the stderr pipe:\n', process_error_each)
                            process_error = \
                                process_error + '\n' + process_error_each
            else:  # Windows
                cmd_split = [application, [step_command]]
                env = None
                process = Popen(
                    cmd_split,
                    cwd=temp_directory,
                    env=env,
                    stdout=PIPE,
                    stderr=PIPE,
                    universal_newlines=True,
                )
                process_out, process_error = process.communicate()
                process_error = filter_log_messages(process_error,
                                                    run_env.posix)

        if step_submodule and hang_check_interval and not run_env.posix:
            try:
                monitor_out, monitor_error = monitor.communicate(timeout=1)
                if len(monitor_out) > 1:
                    with open(
                            Path(working_folder) / 'kill_report.txt', 'a'
                    ) as f:
                        f.write(folder_to_monitor + '\n')
                        f.write(monitor_out)
            except TimeoutExpired:
                pass
            monitor.kill()

    elif 'fme' in application.lower():

        step_command_split = step_command.split('\n')
        fme_workspace = step_command_split[0]
        fme_command = ' '.join(step_command_split[1:])
        if run_env.posix:
            cmd_split = list(step_command_split)
            cmd_split.insert(0, application)
            env = dict(environ.copy())
        else:
            cmd_split = [[f'{application} {fme_workspace}'],
                         [fme_command]]
            env = None

        process = Popen(
            cmd_split,
            env=env,
            stdout=PIPE,
            stderr=PIPE,
            universal_newlines=True,
        )
        process_out, process_error = process.communicate()

    else:
        raise Exception(f'Application {application} is not allowed')

    if process_out and output_reported_individually is False:
        print('\nCommunication from the process '
              'on the stdout pipe: \n\n', process_out)

    process_error = filter_log_messages(process_error, run_env.posix)
    if process_error and output_reported_individually is False:
        print('\nCommunication from the process '
              'on the stderr pipe: \n\n', process_error)

    stop_timing = timer()
    process_time_minutes = (stop_timing - start_timing) / 60

    return process_out, process_error, process_time_minutes
