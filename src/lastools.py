from os import name as os_name
from pathlib import Path


def lastool_command(tool, input_filter, input_dir, output_dir,
                    other_args=[], compress_output='True'):
    """
    Generates a LAStools command string.

    Parameters
    ----------
    tool : str
        The name of the selected LAStool tool
    input_filter : str
        The pointcloud file type: 'las' or 'laz'
    input_dir : str
        The directory with the main input data
    output_dir : str
        The (existing) directory that will receive this tool's output
    other_args : list
        A list of strings, each of which must be a valid argument for
        the selected LAStools tool
    compress_output : str
        For tools with pointcloud output, determines if output will be
        as .las (uncompressed) or .laz (compressed)

    Returns
    -------
    str

    """

    # TODO: Make `compress_output` an actual bool

    tool_list = [
        'blast2dem',
        'las2dem',
        'las2las',
        'las2tin',
        'lasclassify',
        'lasground_new',
        'lasheight',
        'lasindex',
        'lasinfo',
        'lastile',
        'lassplit',
    ]

    tools_with_64bit_capability = [
        'las2dem',
        'las2las',
        'las2tin',
        'lasclassify',
        'lasground_new',
        'lasheight',
        'lasindex',
        'lasinfo',
        'lastile',
        'lassplit',
    ]

    # Validation
    posix = os_name == 'posix'
    if tool not in tool_list:
        raise Exception(f'{tool} is not a recognized LAStool.')

    if input_filter:
        if input_filter in ['las', 'laz']:
            input_filter_full = f'*.{input_filter}'
        else:
            raise Exception(f'{input_filter} is not a recognized input '
                            f'filter; must be "las" or "laz".')
    else:
        input_filter_full = '*.*'
    input_filepath_string = f'"{Path(input_dir) / input_filter_full}"'

    if tool in tools_with_64bit_capability and not posix:
        # Current version of lassplit can't take `-cpu64` argument,
        # even though it does have a 64bit tool
        if tool != 'lassplit':
            other_args.append('-cpu64')

    if tool not in tools_with_64bit_capability and posix:
        raise Exception(f'{tool} cannot be run as 64bit and so cannot '
                        f'currently be run on Linux')

    if eval(compress_output):
        other_args.append('-olaz')

    lastool_command_string = ' '.join([
        '-i', input_filepath_string,
        ' '.join(other_args)
    ])

    if output_dir:
        output_path_string = f'"{output_dir}"'
        lastool_command_string += f' -odir {output_path_string}'

    return lastool_command_string
