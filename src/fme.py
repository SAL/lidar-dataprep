from pathlib import Path


def fme_command(tool, input_dir, output_dir, imagery_file, index_dir):
    """
    Generates an FME command string.

    Parameters
    ----------
    tool : str
        The name of the selected FME tool
    input_dir : str
        The directory with the main input data
    output_dir : str
        The (existing) directory that will receive this tool's output
    imagery_file : str
        A filename, used by some tools
    index_dir : str
        The directory containing the point cloud index shapefile, used
        by the retiling tools

    Returns
    ------
    str

    """

    tool_list = [
        'LAZ_WebMercator',
        'LAZ_WebMercator_MultiProcess',
        'Mosaic_DEM',
        'Mosaic_DSM',
        'Mosaic_DTM',
        'Mosaic_INT',
        'Pointcloud_Tile_Index',
        'Tile_Delivery_Imagery_TIF',
        'Tile_Pointclouds',
        ]

    # Validation
    if tool not in tool_list:
        raise Exception(f'{tool} is not a recognized FME tool.')

    workspaces = {
        'LAZ_WebMercator': 'add_web_mercator_crs_to_laz.fmw',
        'LAZ_WebMercator_MultiProcess':
            'add_web_mercator_crs_to_laz_multiprocess.fmw',
        'Mosaic_DEM': 'dem_mosaic_compresslzw.fmw',
        'Mosaic_DSM': 'dsm_mosaic_compresslzw.fmw',
        'Mosaic_DTM': 'dtm_mosaic_compresslzw.fmw',
        'Mosaic_INT': 'intensity_mosaic_compresslzw.fmw',
        'Pointcloud_Tile_Index': 'pointcloud_tile_index_2.fmw',
        'Tile_Delivery_Imagery_TIF':
            'tile_imagery_mosaic_TIF_with_shapefile.fmw',
        'Tile_Pointclouds': 'tile_pointclouds_with_shapefile.fmw',
    }
    workspaces_path = Path(__file__).parent / 'workspaces' / 'fme'
    fme_workspace = str(workspaces_path / workspaces[tool])
    input_path = Path(input_dir)
    index_path = Path(index_dir) if index_dir is not None else None
    output_path = Path(output_dir)
    input_all_files = str(input_path / "*.*")
    input_laz_files = str(input_path / "*.laz")

    if tool in [
        'Mosaic_DEM',
        'Mosaic_DSM',
        'Mosaic_DTM',
        'Mosaic_INT',
    ]:
        input_file_tif = input_path / '*.tif'
        input_file_tiff = input_path / '*.tiff'
        input_file_itiff = input_path / '*.itiff'
        input_file_ovr = input_path / '*.ovr'

        fme_command_string = '\n'.join([
            fme_workspace,
            '--SourceDataset_GEOTIFF',
            f'\"\"\" {input_file_tif} {input_file_tiff} {input_file_itiff} '
            f'{input_file_ovr} \"\"\"',
            '--DestDataset_GEOTIFF', f'{output_dir}',
        ])

    elif tool == 'Pointcloud_Tile_Index':
        fme_command_string = '\n'.join([
            fme_workspace,
            '--DestDataset_ESRISHAPE', f'{output_dir}',
            '--SourceDataset_LAS_3', input_all_files,
        ])

    elif tool == 'Tile_Pointclouds':
        fme_command_string = '\n'.join([
            fme_workspace,
            '--SourceDataset_ESRISHAPE_2',
            str(index_path / 'pointcloud_tile_index.shp'),
            '--SourceDataset_LAS_2', input_all_files,
            '--DestDataset_LAS_3', f'{output_dir}',
        ])

    elif tool == 'Tile_Delivery_Imagery_TIF':
        fme_command_string = '\n'.join([
            fme_workspace,
            '--SourceDataset_TIFF_3',
            str(input_path / f'{imagery_file}.tif'),
            '--DestDataset_TIFF',
            str(output_path / f'{imagery_file}_tiled_from_mosaic'),
            '--SourceDataset_ESRISHAPE_3',
            str(index_path / 'pointcloud_tile_index.shp'),
        ])
    elif tool == 'LAZ_WebMercator':
        fme_command_string = '\n'.join([
            fme_workspace,
            '--SourceDataset_LAS', input_laz_files,
            '--DestDataset_LAS', output_dir
        ])
    elif tool == 'LAZ_WebMercator_MultiProcess':
        fme_command_string = '\n'.join([
            fme_workspace,
            '--SourceDataset_PATH', input_dir,
            '--WORKSPACE',
            str(workspaces_path / workspaces['LAZ_WebMercator']),
            '--OUTPUT', output_dir
        ])

    # Redirect FME logging to LiDAR Dataprep log
    fme_command_string += '\nLOG_STANDARDOUT\nYES'

    return fme_command_string
