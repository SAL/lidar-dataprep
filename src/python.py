from pathlib import Path


def python_command(tool, input_dir, output_dir, config_paths, input_filter,
                   input_filename, output_filename, cores, heightbreak,
                   cellsize, pipeline, index_dir):
    """
    Generates a Python command string.

    Parameters
    ----------
    tool : str
        The name of the selected FileManagement tool
    input_dir : str
        The directory with the main input data
    output_dir : str
        The (existing) directory that will receive this tool's output
    config_paths : src.utilities.run_environment.RetrieveRunEnv
        An object containing software paths as attributes, used by
        some tools
    input_filter : str or None
        The pointcloud file type: 'las' or 'laz', used by some tools
    input_filename : str
        A filename, used by some tools
    output_filename : str or None
        A filename, used by some tools
    cores: str or None
        The number of cores to use for processing, used by some tools
    heightbreak : int or None
        Height break for cover calculation, used by Fusion tools
    cellsize : int or None
        Desired grid cell size in the same units as LIDAR data, used
        by Fusion tools
    pipeline : str or None
        The stem of a pdal pipeline json file, used by some tools
    index_dir : str or None
        The directory containing the point cloud index shapefile, used
        by the qc tool

    Returns
    ------
    str

    """

    root_filepath = Path(__file__).parent

    tool_dict = {
        'CalcDeliveryTifPyramids':
            [
                'calc_delivery_tif_pyramids.py',
                [output_dir]
            ],
        'CalcDeliveryTifStats':
            [
                'calc_delivery_tif_stats.py',
                [output_dir]
            ],
        'CopyFiles':
            [
                'copy_files.py',
                [input_dir, output_dir]
            ],
        'CopyFolder':
            [
                'copy_folder.py',
                [input_dir, output_dir]
            ],
        'QC_Pointclouds':
            [
                'qc_pointclouds.py',
                [input_dir, input_filter, cores, output_dir,
                 config_paths.lastools_bin, index_dir]
                if index_dir
                else [input_dir, input_filter, cores, output_dir,
                      config_paths.lastools_bin]
            ],
        'RenameRaster':
            [
                'rename_raster.py',
                [input_dir, input_filename, output_filename]
            ],
        'SetDeliveryTifCRS':
            [
                'set_delivery_tif_crs.py',
                [input_dir, output_dir, config_paths.lastools_bin]
            ],
        'MultiprocessPDAL':
            [
                'multiprocess_pdal.py',
                [input_dir,
                 str(root_filepath / 'workspaces' /
                     'pdal' / f'{pipeline}.json'),
                 output_dir,
                 cores,
                 config_paths.pdal_exe]
            ],
        'MultiprocessFusion':
            [
                'multiprocess_fusion.py',
                [input_dir,
                 output_dir,
                 cores,
                 heightbreak,
                 cellsize,
                 str(Path(config_paths.fusion_folder) / 'gridmetrics64.exe')]
            ],
        'MultiprocessLidR':
            [
                'multiprocess_lidr.py',
                [input_dir,
                 output_dir,
                 cellsize,
                 cores,
                 config_paths.lidr_env]
            ],
    }

    if tool not in tool_dict.keys():
        raise Exception(f'{tool} is not a recognized Python tool.')
    else:
        script = Path(root_filepath) / 'workspaces' / \
                 'python' / tool_dict[tool][0]
        args = tool_dict[tool][1]
        python_command_string = f'{script} {" ".join(args)}'
        return python_command_string
