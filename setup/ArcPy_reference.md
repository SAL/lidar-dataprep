# ArcPy Reference

ArcGIS Server provides the option to use a Python 3 runtime for standalone 
ArcPy scripts run outside the context of a geoprocessing service.

Using this method, select tools can be executed using a Python 3 runtime 
that is installed independently of ArcGIS Server. This also allows for 
third-party Python 3 libraries to be imported and used alongside arcpy and 
the ArcGIS geoprocessing tools.

You will need to set up the Python 3 runtime for ArcGIS Server on the 
same machine where ArcGIS Server is installed. They both need to be 
installed under the same user account, and ArcGIS Server needs to be 
authorized properly.  For supported Linux operating systems, please refer 
to the System Requirements for ArcGIS Server.

## System Requirements
https://enterprise.arcgis.com/en/system-requirements/latest/linux/arcgis-server-system-requirements.htm

## Minimum System Requirements
Anaconda Python 3.x Linux Installer
ArcGIS Server (Linux)

## Install using Conda
The Python 3 runtime for ArcGIS Server on Linux is distributed via conda. 
Conda is a popular package and environment manager application that helps you 
install and update packages and their dependencies. To learn more about conda, 
please refer to the conda documentation:
https://conda.io/en/latest/

## Get Anaconda
Install the latest version of Anaconda for Python 3.x from the Anaconda 
website if you don't already have conda. The Python 3 runtime for ArcGIS 
Server on Linux requires a 64-bit installer: https://www.anaconda.com/


## Install arcgis-server-py3 package
1) If you have already created a conda environment, download and install the 
   Python 3 runtime for ArcGIS Server on Linux using the following command in 
   your terminal:
   `conda install -c esri arcgis-server-py3=10.9.0`

   Or, use conda to create a new environment and install 
   arcgis-server-py3 package with this command:
   `conda create -c esri -n <condaEnv> arcgis-server-py3=10.9.0`

2) In your terminal, set the ARCGISHOME variable to the installation directory 
   of ArcGIS Server:
   `export ARCGISHOME=/path/to/arcgis/server`

   You will need to set the ARCGISHOME variable every time to activate the 
   conda environment for the Python 3 runtime for ArcGIS Server. 

3) Activate the environment by using the following command:
   `conda activate <condaEnv>`

4) When you are done working with ArcPy, deactivate the environment with this 
   command:
   `conda deactivate <condaEnv>`
 
   For more information about managing conda environments, refer to the conda 
   documentation:
   http://conda.pydata.org/docs/using/envs.html
    
## Test your install
In the conda environment where the Python 3 runtime for ArcGIS Server is 
installed, run the following command to import arcpy:
```
(<condaEnv>) [user@servername ~]$ python
Python 3.7.6 (default, Jan  8 2020, 19:59:22) 
[GCC 7.3.0] :: Anaconda, Inc. on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import arcpy
>>> quit()
```

If the import command produced no messages, you're ready to work with ArcPy
on Linux.

Make sure the correct Python interpreter is being run by using the following 
command:
`which python`

The path to Python interpreter should be found at: 
`/path/to/anaconda3/envs/<condaEnv>/bin/python`

Now you are setup to use the Python 3 runtime for command-line ArcPy scripts.

## Known limitations
For multiprocessing functionality, the default Linux fork mode should be 
avoided for using Python 3 runtime for ArcGIS Server on Linux.  At this time, 
please use spawn or forkserver mode instead. These modes were introduced with 
Python 3.4 and work with Python 3 runtime for command line ArcPy scripts. For 
reference, see:
https://docs.python.org/3/library/multiprocessing.html#contexts-and-start-methods

## Full listing of supported geoprocessing tools for ArcGIS Server on Linux
The following geoprocessing tools are supported for use under the Python 3 
runtime for ArcGIS Server. Tools not listed below are not supported and are 
not guaranteed to be functional in this environment.

Not all classes and functions in the arcpy module are supported. Specifically, 
the ArcSDESQLExecute class is not supported under the Python 3 runtime for 
ArcGIS Server.

Tools and functions that are not supported under the Python 3 runtime for 
ArcGIS Server may be used via scripts published as geoprocessing services 
instead.

### Analysis toolbox
  - Buffer
  - Clip
  - CreateThiessenPolygons
  - EnrichLayer
  - Erase
  - Frequency
  - GenerateNearTable
  - GraphicBuffer
  - Identity
  - Intersect
  - MultipleRingBuffer
  - Near
  - PairwiseBuffer
  - PairwiseDissolve
  - PairwiseIntersect
  - PointDistance
  - PolygonNeighbors
  - Select
  - SpatialJoin
  - Split
  - SplitByAttributes
  - Statistics
  - SymDiff
  - TableSelect
  - TabulateIntersection
  - Union
  - Update
 
### Conversion toolbox
  - ASCIIToRaster
  - DEMToRaster
  - ExcelToTable
  - FeatureClassToFeatureClass
  - FeatureClassToGeodatabase
  - FeatureClassToShapefile
  - FeaturesToJSON
  - FeatureToRaster
  - FloatToRaster
  - GPXtoFeatures
  - JSONToFeatures
  - MultipatchToRaster
  - PointToRaster
  - PolygonToRaster
  - PolylineToRaster
  - RasterToASCII
  - RasterToFloat
  - RasterToGeodatabase
  - RasterToOtherFormat
  - RasterToPoint
  - RasterToPolygon
  - RasterToPolyline
  - TableToDBASE
  - TableToExcel
  - TableToGeodatabase
  - TableToTable
  - WFSToFeatureClass
 
### Data management toolbox
  - AddAttachments
  - AddCodedValueToDomain
  - AddColormap
  - AddFeatureclassToTopology
  - AddField
  - AddfieldConflictFilter
  - AddGeometryAttributes
  - AddIndex
  - AddJoin
  - AddRastersToMosaicDataset
  - AddRuleToTopology
  - AddSpatialIndex
  - AddSubtype
  - AddXY
  - AlterDomain
  - AlterField
  - AlterMosaicDatasetSchema
  - AlterVersion
  - AnalyzeDatasets
  - AnalyzeMosaicDataset
  - AnalyzeToolsForPro
  - Append
  - AppendControlPoints
  - ApplyBlockAdjustment
  - ApplySymbologyFromLayer
  - AssignDefaultToField
  - AssignDomainToField
  - BatchBuildPyramids
  - BatchCalculateStatistics
  - BatchProject
  - BearingDistanceToLine
  - BuildBoundary
  - BuildFootprints
  - BuildMosaicDatasetItemCache
  - BuildOverviews
  - BuildPyramids
  - BuildPyramidsandStatistics
  - BuildRasterAttributeTable
  - BuildSeamlines
  - BuildStereoModel
  - CalculateCellSizeRanges
  - CalculateDefaultClusterTolerance
  - CalculateDefaultGridIndex
  - CalculateEndTime
  - CalculateField
  - CalculateStatistics
  - CalculateValue
  - ChangePrivileges
  - ChangeVersion
  - CheckGeometry
  - ClearWorkspaceCache
  - Clip
  - ColorBalanceMosaicDataset
  - Compact
  - CompositeBands
  - Compress
  - ComputeBlockAdjustment
  - ComputeCameraModel
  - ComputeControlPoints
  - ComputeDirtyArea
  - ComputeMosaicCandidates
  - ComputePansharpenWeights
  - ComputeTiePoints
  - ConfigureGeodatabaseLogFileTables
  - ConvertCoordinateNotation
  - ConvertTimeField
  - ConvertTimeZone
  - Copy
  - CopyFeatures
  - CopyRaster
  - CopyRows
  - CreateCustomGeoTransformation
  - CreateDatabaseConnection
  - CreateDatabaseUser
  - CreateDatabaseView
  - CreateDomain
  - CreateEnterpriseGeodatabase
  - CreateFeatureclass
  - CreateFeatureDataset
  - CreateFileGDB
  - CreateFishnet
  - CreateFolder
  - CreateMosaicDataset
  - CreateRandomPoints
  - CreateRandomRaster
  - CreateRasterDataset
  - CreateReferencedMosaicDataset
  - CreateRelationshipclass
  - CreateRole
  - CreateSpatialReference
  - CreateTable
  - CreateTopology
  - CreateUnRegisteredFeatureclass
  - CreateUnRegisteredTable
  - CreateVersion
  - DefineOverviews
  - DefineProjection
  - Delete
  - DeleteCodedValueFromDomain
  - DeleteColormap
  - DeleteDomain
  - DeleteFeatures
  - DeleteField
  - DeleteIdentical
  - DeleteMosaicDataset
  - DeleteRasterAttributeTable
  - DeleteRows
  - DeleteSchemaGeodatabase
  - DeleteVersion
  - DiagnoseVersionTables
  - Dice
  - DisableArchiving
  - DisableAttachments
  - DisableEditorTracking
  - Dissolve
  - DomainToTable
  - DownloadRasters
  - EditRasterFunction
  - Eliminate
  - EliminatePolygonPart
  - EnableArchiving
  - EnableAttachments
  - EnableEditorTracking
  - EnableEnterpriseGeodatabase
  - ExportGeodatabaseConfigurationKeywords
  - ExportMosaicDatasetGeometry
  - ExportMosaicDatasetItems
  - ExportMosaicDatasetPaths
  - ExportRasterWorldFile
  - ExportTileCache
  - ExportTopologyErrors
  - ExtractSubDataset
  - FeatureCompare
  - FeatureEnvelopeToPolygon
  - FeatureToLine
  - FeatureToPoint
  - FeatureToPolygon
  - FeatureVerticesToPoints
  - FileCompare
  - FindIdentical
  - Flip
  - GenerateAttachmentMatchTable
  - GenerateExcludeArea
  - GeneratePointCloud
  - GeneratePointsAlongLines
  - GenerateRasterFromRasterFunction
  - GenerateTessellation
  - GenerateTileCacheTilingScheme
  - GeodeticDensify
  - GeoTaggedPhotosToPoints
  - GetCellValue
  - GetCount
  - GetRasterProperties
  - ImportGeodatabaseConfigurationKeywords
  - ImportMosaicDatasetGeometry
  - ImportTileCache
  - ImportXMLWorkspaceDocument
  - Integrate
  - InterpolateFromPointCloud
  - JoinField
  - MakeFeatureLayer
  - MakeQueryTable
  - MakeRasterLayer
  - MakeTableView
  - MakeWCSLayer
  - MakeXYEventLayer
  - ManageTileCache
  - MatchPhotosToRowsByTime
  - Merge
  - MergeMosaicDatasetItems
  - MigrateRelationshipClass
  - MinimumBoundingGeometry
  - Mirror
  - Mosaic
  - MosaicToNewRaster
  - MultipartToSinglepart
  - PivotTable
  - PointsToLine
  - PolygonToLine
  - Project
  - ProjectRaster
  - RasterCompare
  - RasterToDTED
  - RebuildIndexes
  - ReconcileVersions
  - RecoverFileGDB
  - RegisterAsVersioned
  - RegisterRaster
  - RegisterWithGeodatabase
  - RemoveAttachments
  - RemoveDomainFromField
  - RemoveFeatureClassFromTopology
  - RemoveFieldConflictFilter
  - RemoveIndex
  - RemoveJoin
  - RemoveRastersFromMosaicDataset
  - RemoveSpatialIndex
  - RemoveSubtype
  - Rename
  - RepairGeometry
  - RepairMosaicDatasetPaths
  - RepairVersionTables
  - Resample
  - Rescale
  - SaveToLayerFile
  - SelectLayerByAttribute
  - SelectLayerByLocation
  - SetClusterTolerance
  - SetDefaultSubtype
  - SetMosaicDatasetProperties
  - SetRasterProperties
  - SetSubtypeField
  - SetValueForRangeDomain
  - Shift
  - Sort
  - SortCodedValueDomain
  - SplitLine
  - SplitLineAtPoint
  - SplitMosaicDatasetItems
  - SplitRaster
  - SynchronizeMosaicDataset
  - TableCompare
  - TableToDomain
  - TableToEllipse
  - TableToRelationshipClass
  - TransposeFields
  - TruncateTable
  - UnregisterAsVersioned
  - UnsplitLine
  - UpgradeDataset
  - UpgradeGDB
  - ValidateTopology
  - Warp
  - WarpFromFile
  - WorkspaceToRasterDataset
  - XYToLine
 
### Editing toolbox
  - AlignFeatures
  - CalculateTransformationErrors
  - Densify
  - EdgematchFeatures
  - ErasePoint
  - ExtendLine
  - FlipLine
  - Generalize
  - GenerateEdgematchLinks
  - RubbersheetFeatures
  - Snap
  - TransformFeatures
  - TrimLine
 
### Multidimension toolbox
  - FeatureToNetCDF
  - MakeNetCDFFeatureLayer
  - MakeNetCDFRasterLayer
  - MakeNetCDFTableView
  - MakeOPeNDAPRasterLayer
  - RasterToNetCDF
  - SelectByDimension
  - TableToNetCDF
 
### Spatial Analyst toolbox
  - Abs
  - ACos
  - ACosH
  - Aggregate
  - AreaSolarRadiation
  - ASin
  - ASinH
  - Aspect
  - ATan
  - ATan2
  - ATanH
  - BandCollectionStats
  - Basin
  - BitwiseAnd
  - BitwiseLeftShift
  - BitwiseNot
  - BitwiseOr
  - BitwiseRightShift
  - BitwiseXOr
  - BlockStatistics
  - BooleanAnd
  - BooleanNot
  - BooleanOr
  - BooleanXOr
  - BoundaryClean
  - CellStatistics
  - ClassProbability
  - CombinatorialAnd
  - CombinatorialOr
  - CombinatorialXOr
  - Combine
  - Contour
  - ContourList
  - ContourWithBarriers
  - Corridor
  - Cos
  - CosH
  - CostAllocation
  - CostBackLink
  - CostConnectivity
  - CostDistance
  - CostPath
  - CreateConstantRaster
  - CreateNormalRaster
  - CreateRandomRaster
  - CreateSignatures
  - Curvature
  - CutFill
  - DarcyFlow
  - DarcyVelocity
  - Diff
  - Divide
  - EditSignatures
  - EqualTo
  - EqualToFrequency
  - EucAllocation
  - EucDirection
  - EucDistance
  - Exp
  - Exp10
  - Exp2
  - Expand
  - Extent
  - ExtractByAttributes
  - ExtractByCircle
  - ExtractByMask
  - ExtractByPoints
  - ExtractByPolygon
  - ExtractByRectangle
  - ExtractMultiValuesToPoints
  - ExtractValuesToPoints
  - Fill
  - Filter
  - Float
  - FloatDivide
  - FloorDivide
  - FlowAccumulation
  - FlowDirection
  - FlowLength
  - FocalFlow
  - FocalStatistics
  - FuzzyMembership
  - FuzzyOverlay
  - GreaterThan
  - GreaterThanEqual
  - GreaterThanFrequency
  - HighestPosition
  - Hillshade
  - Idw
  - InList
  - Int
  - IsNull
  - IsoCluster
  - KernelDensity
  - Kriging
  - LessThan
  - LessThanEqual
  - LessThanFrequency
  - LineDensity
  - LineStatistics
  - Ln
  - LocateRegions
  - Log10
  - Log2
  - Lookup
  - LowestPosition
  - MajorityFilter
  - Minus
  - MLClassify
  - Mod
  - NaturalNeighbor
  - Negate
  - Nibble
  - NotEqual
  - ObserverPoints
  - Over
  - ParticleTrack
  - PathAllocation
  - PathBackLink
  - PathDistance
  - Pick
  - Plus
  - PointDensity
  - PointsSolarRadiation
  - PointStatistics
  - Popularity
  - PorousPuff
  - Power
  - PrincipalComponents
  - Rank
  - ReclassByASCIIFile
  - ReclassByTable
  - Reclassify
  - RegionGroup
  - RescaleByFunction
  - RoundDown
  - RoundUp
  - Sample
  - SetNull
  - Shrink
  - Sin
  - SinH
  - Sink
  - Slice
  - Slope
  - SnapPourPoint
  - SolarRadiationGraphics
  - Spline
  - SplineWithBarriers
  - Square
  - SquareRoot
  - StreamLink
  - StreamOrder
  - StreamToFeature
  - TabulateArea
  - Tan
  - TanH
  - Test
  - Thin
  - Times
  - TopoToRaster
  - TopoToRasterByFile
  - Trend
  - Viewshed
  - Viewshed2
  - Visibility
  - Watershed
  - WeightedOverlay
  - WeightedSum
  - ZonalFill
  - ZonalGeometry
  - ZonalGeometryAsTable
  - ZonalHistogram
  - ZonalStatistics
  - ZonalStatisticsAsTable

### GeoStatistical Analyst Toolbox
  - Diffusion Interpolation With Barriers
  - EBK Regression Prediction
  - Empirical Bayesian Kriging
  - Global Polynomial Interpolation
  - IDW
  - Kernel Interpolation With Barriers
  - Local Polynomial Interpolation
  - Moving Window Kriging
  - Radial Basis Functions
  - Create Spatially Balanced Points
  - Densify Sampling Network
  - Gaussian Geostatistical Simulations
  - Extract Values To Table
  - Cross Validation
  - Generate Subset Polygons
  - Neighborhood Selection
  - Semivariogram Sensitivity
  - Subset Features
  - Areal Interpolation Layer To Polygons
  - Calculate Z Value
  - Create Geostatistical Layer
  - GA Layer to Contour
  - GA Layer to Grid
  - GA Layer to Points
  - GA Layer to Rasters
  - Get Model Parameter
  - Set Model Parameter

### Spatial Statistics Toolbox
  - Average Nearest Neighbor
  - High/Low Clustering
  - Incremental Spatial Autocorrelation
  - Multi-Distance Spatial Cluster Analysis
  - Spatial Autocorrelation
  - Cluster and Outlier Analysis
  - Density-based Clustering
  - Hot Spot Analysis
  - Multivariate Clustering
  - Optimized Hot Spot Analysis
  - Optimized Outlier Analysis
  - Similarity Search
  - Spatially Constrained Multivariate Clustering
  - Central Feature
  - Directional Distribution
  - Linear Directional Mean
  - Mean Center
  - Median Center
  - Standard Distance
  - Exploratory Regression
  - Forest-based Classification and Regression
  - Generate Network Spatial Weights Matrix (Has dependency on Network Analyst. 
  Tool can be excluded from DSX.)
  - Generate Spatial Weights Matrix
  - Geographically Weighted Regression
  - Ordinary Least Squares
  - Calculate Distance Band from Neighbor Count
  - Collect Events
  - Convert Spatial Weights Matrix to Table
  - Export Feature Attributes to ASCII

### Space Time Pattern Mining Toolbox
  - Create Space Time Cube By Aggregating Points
  - Create Space Time Cube From Defined Locations
  - Emerging Hot Spot Analysis
  - Local Outlier Analysis
  - Time Series Clustering
  - Fill Missing Values
  - Visualize Space Time Cube in 2D
  - Visualize Space Time Cube in 3D
 
Publishing Feature Layers and Tile Layers to ArcGIS Enterprise and federated 
servers is also supported.

UPDATED March 22nd, 2021
