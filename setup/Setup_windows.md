## Set up to run on Windows (Windows 10 and Windows Server 2016)

1. Install this Python codebase on your machine, using one of these methods:
    - Clone or download/unzip the appropriate release branch from the 
   [code repository](https://gitlab.uvm.edu/SAL/lidar-dataprep)
    - Locate the latest release zip file at  the standard repository of 
   SAL installers and unzip on your machine
2. Install dependent software 
   - See [Software_Dependencies.md](./Software_dependencies.md) 
   for required versions to install of:
      - Fusion
        - Copy the Fusion folder appropriate for this version of LiDAR 
        Dataprep to your local machine from the SAL standard-installers 
        repository
      - LAStools 
        - Copy the LAStools folder appropriate for this version of LiDAR 
        Dataprep to your local machine from the SAL standard-installers 
        repository
      - FME (Installed by SAL Admin)
      - Miniconda3 v4
        - Install only for your own user, in a regular user folder (like 
        `C:\\Barrett\Miniconda3`, not in a path that includes `.conda` 
        or similar)
        - Run `conda init` to add `conda` to your system path
        - Don't upgrade/update past v4, even though it recommends you to upgrade 
        when you run `conda`
3. Create conda environments using the included environment configuration files
   - `dataprep`
     - `> conda env create -f path\to\lidar-dataprep\setup\environment_dataprep.yaml`
   - `pdal`
     - `> conda env create -f path\to\lidar-dataprep\setup\environment_pdal.yaml`
   - Not needed on Windows:
     - `lidr` (Currently only runs on Linux)
   - These environments need to be created in the `envs` folder within the 
     Miniconda you installed above. If you see, with `conda info -e`, that they 
     are being created elsewhere, issue this command (updated to customize the 
     path to your Miniconda install:
     - `conda config --add envs_dirs <path>\Miniconda3\envs`

4. License dependent software
   - Log into ArcPro
     - If using "concurrent licensing", following the directions pinned 
     to the `#it` Slack channel, and if the OK button won't become active, 
     add `arclm.uvm.edu` to Internet Explorer Trusted Sites
   - Ensure a current LAStools license file (`lastoolslicense.txt`) is 
   copied to the LAStools `bin` folder
   - Ask SAL Admin about licensing FME
5. Test that all software is correctly configured for command-line use and 
licensed for your user. Paths referenced below can be found in local version of 
`software_paths.ini` in the root of this codebase
   - ArcPy (using ArcPro's conda)
       - `> "<path to>\ArcGIS\Pro\bin\Python\Scripts\activate.bat" arcgispro-py3`
       - `> "C:\Program Files\ArcGIS\Pro\bin\Python\Scripts\activate.bat" arcgispro-py3`
         - These messages on Windows Server can be ignored:
           - `CondaEnvironmentError: Cannot activate environment cmd.exe.`
           - `User does not have write access for conda symlinks.`
       - `> python`
       - `>>> import arcpy`
         - Should import within a few seconds, and give no error messages
       - `>>> quit()`
       - `> "C:\Program Files\ArcGIS\Pro\bin\Python\Scripts\deactivate.bat" arcgispro-py3`
   - LAStools
     - `> "<path to LAStools bin folder>"\las2dem -license`
       - Should return output that includes:
            ```
            LAStools (by info@rapidlasso.de) version 230330 (academic)
            holder:      Spatial_Analysis_Laboratory_University_Vermont_VT
            start date:  220923
            duration:    403 days
            maintenance: yes
            remaining:   188 days
            # of seats:  office-wide (this location)
            type:        academic
           ```
   - FME
     - `> "<path to FME.exe>"`
       - Should return output that includes:
            ```
                   FME 2021.1.0.1 (20210720 - Build 21614 - WIN64)
                           FME Database Edition (floating)
                        Machine host name is: STL-5FO46E4FKLE
             Operating System: Microsoft Windows 10 64-bit  (Build 19042)
                    Copyright (c) 1994 - 2021, Safe Software Inc.
                                  Safe Software Inc.
            ```
