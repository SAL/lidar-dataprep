# Small archives can be extracted with this script run directly on the
#   user node with:
#  `$ bash ~/code/lidar-dataprep/setup/unarchive_folder.sh <archive.tar> <output-folder>`
# For large archives, run this as a compute-node job with the slurm file:
#  `$ sbatch ~/code/lidar-dataprep/VACC_running/slurm_unarchive_folder.sh <archive.tar> <output-folder>`
#
# Note: Renaming the archive file before extraction will have no affect on the
#   folder name as extracted - its name will remain the same as it was when it
#   was archived.

[ -z "$1" ] && echo "Exiting: The archive to extract is a required parameter" && exit
[ -z "$2" ] && echo "Exiting: The destination folder to extract to is a required parameter" && exit

ARCHIVE=$(readlink --canonicalize "$1")
OUTPUT=$(readlink --canonicalize "$2")
echo "Extracting $ARCHIVE into $OUTPUT"

#ARCHIVE_NAME=$(basename "$ARCHIVE")
#echo Archive name: "$ARCHIVE_NAME"

PWD=$(pwd)
cd "$OUTPUT" || exit
tar -xvf "$ARCHIVE"

cd "$PWD" || exit
echo "Extraction complete for $ARCHIVE"

#echo "Giving everyone in the group read/copy access"
#chmod g+rx -R <folder>
