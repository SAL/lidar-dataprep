# For small folders, or for testing a subset of a large folder,
# run this directly on the user node with:
#  `$ bash ~/code/lidar-dataprep/setup/archive_folder.sh <folder>`
# For large folders, run this as a compute-node job with the slurm file:
#  `$ sbatch ~/code/lidar-dataprep/VACC_running/slurm_archive_folder.sh <folder>`

[ -z "$1" ] && echo "Exiting: The folder to archive is a required parameter" && exit

FOLDER=$(readlink --canonicalize "$1")
echo Folder that will be archived: "$FOLDER"

FOLDER_NAME=$(basename "$FOLDER")
echo Folder name: "$FOLDER_NAME"
FOLDER_PATH=$(dirname "$FOLDER")
echo Folder path: "$FOLDER_PATH"

ARCHIVE_FILE="$FOLDER_NAME.tar"  # For uncompressed archives
#ARCHIVE_FILE="$FOLDER_NAME.tar.gz"  # For compressed archives

ARCHIVE_FILEPATH="$FOLDER_PATH/$ARCHIVE_FILE"

echo "Archiving $FOLDER as $ARCHIVE_FILEPATH"
PWD=$(pwd)
cd "$FOLDER_PATH" || exit

tar -cvf "$ARCHIVE_FILE" "$FOLDER_NAME" # For uncompressed archives
#tar -Wcvf "$ARCHIVE_FILE" "$FOLDER_NAME" # For uncompressed archives, with verification
#tar -zcvf "$ARCHIVE_FILE" "$FOLDER_NAME" # For compressed archives

# This progress tracking works on the user node, but not the compute nodes
#   (since `pv` not installed there); Check if extract works on these archives
# tar -cvf - "$FOLDER_NAME" | pv -s $(du -sb "$FOLDER_NAME" | awk '{print $1}') | gzip > "$ARCHIVE_FILE"

# This option removes a file as soon as it is in in the archive, but since
#   if the archiving command fails or times out the archive is unusable, we
#   shouldn't use it in general:
# --remove-files

cd "$PWD" || exit
echo "Archiving complete for $FOLDER"

# To display contents of an archive:
# `tar -tvf project.tar.gz` (compressed) `tar -tvf project.tar` (uncompressed)

# To extract the archive into the `pwd`:
# Note: Will overwrite w/o warning
# `tar -zxvf project.tar.gz` (compressed) or `tar -xvf project.tar` (uncompressed)

#echo "Giving everyone in the group read/copy access"
#chmod g+rx -R <folder>
