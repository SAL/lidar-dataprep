# Run this with `$ bash ~/code/lidar-dataprep/setup/collect_config_data.sh`

# Create unique directory with user name and timestamp
OUTPUT_FOLDER=/netfiles/sal3/Temp/LD_on_VACC_debug/config_data_${USER}_$(date +%s)/
mkdir "${OUTPUT_FOLDER}"

# Collect data about conda envs
eval "$(conda shell.bash hook)"
for envName in base arcpy dataprep; do
  conda activate "${envName}"

  # Save info on installed packages
  conda list > "${OUTPUT_FOLDER}${envName}_env_pkgs_${USER}".txt

  # Save sorted env vars
  env -0 | sort -z | tr '\0' '\n' > "${OUTPUT_FOLDER}${envName}_env_env_vars_${USER}".txt
done

# Copy config files
cp ~/.bashrc "${OUTPUT_FOLDER}bashrc_${USER}"
cp ~/.bash_profile "${OUTPUT_FOLDER}bash_profile_${USER}"
cp ~/.condarc "${OUTPUT_FOLDER}condarc_${USER}"
cp ~/code/lidar-dataprep/software_paths.ini "${OUTPUT_FOLDER}software_paths_${USER}.ini"

# Make files readable by others
chmod g+rx -R "${OUTPUT_FOLDER}"
