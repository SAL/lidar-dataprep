# Set up to run on RedHat Linux on a Virtual Machine

1. Create and enter a RedHat virtual machine (on a Windows 10 machine)
   - Install latest versions of Vagrant and Virtual Box
   - Open a Command Prompt terminal
   - `> cd` to the root of a local copy or clone of this codebase
   - `> vagrant up` to create the (or start up an existing) VM
   - `> vagrant ssh` to enter the VM
     - Note: The codebase installed on the Windows host (and any host drive 
     locations configured in the `Vagrantfile`) are available 
     inside the VM at `/vagrant`
2. Acquire and configure a RedHat Enterprise account
   - Request a UVM RedHat Enterprise account at `saa@uvm.edu` 
     - A temporary/trial option: 
     [trial account](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux/try-it)
   - Note: All steps that follow are performed in the VM
   - `$ sudo subscription-manager register`
   - `$ sudo subscription-manager attach --auto`
   - Check RedHat version: 
     - `$ cat /etc/redhat-release` should return:
       - `Red Hat Enterprise Linux Server release 7.9 (Maipo)`
3. Install Miniconda3
   - `$ cd ~`
   - `$ bash /vagrant/setup/setup_vm.sh`
   - Accept all defaults, apart from replying 'yes' to the running 
   of `conda init`
   - After conda install `$ exit` the ssh session and re-enter 
   (`> vagrant ssh`) to be able to use conda
   - Check that conda is installed correctly: `$ conda` should return the conda 
   help info
4. Setup FME
   - Install FME Desktop
      - `$ sudo yum install createrepo`
      - `$ mkdir -p /home/vagrant/redhat_repos/fme-desktop-2021`
      - `$ wget --directory-prefix /home/vagrant/redhat_repos/fme-desktop-2021 https://downloads.safe.com/fme/2021/fme-desktop-2021-2021.1.3.0-21631.el7.x86_64.rpm`
      - `$ sudo chown -R root.root /home/vagrant/redhat_repos/fme-desktop-2021`
      - `$ sudo createrepo /home/vagrant/redhat_repos/fme-desktop-2021`
      - `$ sudo chmod -R o-w+r /home/vagrant/redhat_repos/fme-desktop-2021`
      - `$ sudo cp /vagrant/setup/fme-desktop-2021.repo /etc/yum.repos.d`
      - `$ sudo yum localinstall /home/vagrant/redhat_repos/fme-desktop-2021/fme-desktop-2021-2021.1.3.0-21631.el7.x86_64.rpm`
   - Configure license
     - `$ /opt/fme-desktop-2021/fmelicensingassistant --floating fmelm.uvm.edu database`
     - Check that FME is licensed: `$ /opt/fme-desktop-2021/fme`
       - If FME setup and licensing has been successfully completed, you will 
       see output that starts with these lines:
         - ```
           FME 2021.2.3.0 (20220131 - Build 21812 - linux-x64)
           FME Database Edition (floating)
           Machine host name is: vacc-user1.cluster
           Operating System: Red Hat Enterprise Linux Server 7.9 (Maipo)
           ```
   - To run FME on the VM command line
      - `$ /opt/fme-desktop-2021/fme <workbench> <args>`
5. Setup Arc
   - Install ArcServer
     - `$ mkdir ~/install_arc`
     - `$ cp "/vagrant/Odrive/Resources/ESRI/ArcGIS Server 10.9/ArcGIS_Server_Linux_109_177864.tar.gz" ~/install_arc/arc_installer.tar.gz`
       - Note: This installer is for ArcServer version 10.9.0 - if there 
       is a change in the version of ArcServer installed, ensure that the
       matching version is installed in the conda environment, below
     - `$ tar -xvf ~/install_arc/arc_installer.tar.gz --directory ~/install_arc`
       - Contents will be unzipped to a folder `ArcGISServer`
     - Update system limits for Arc install
       - `$ sudo cp /etc/security/limits.conf /etc/security/limits__original.conf`
       - `$ echo ''${USER}' soft nofile 65535' | sudo tee -a /etc/security/limits.conf`
       - `$ echo ''${USER}' hard nofile 65535' | sudo tee -a /etc/security/limits.conf`
       - `$ echo ''${USER}' soft nproc 25059' | sudo tee -a /etc/security/limits.conf`
       - `$ echo ''${USER}' hard nproc 25059' | sudo tee -a /etc/security/limits.conf`
       - exit and re-ssh
       - Check that updated limits are in place
         - `$ ulimit -Sn -Hn`
         - `$ ulimit -Su -Hu`
     - Install application
       - `$ mkdir ~/arc-server-10.9`
       - `$ ~/install_arc/ArcGISServer/Setup -m silent -l yes -d ~/arc-server-10.9`
     - Restore system limits
       - `$ sudo cp /etc/security/limits.conf /etc/security/limits__set_high.conf`
       - `$ sudo cp /etc/security/limits__original.conf /etc/security/limits.conf`
       - Restart & reenter VM
         - `$ exit`
         - `> vagrant ssh`
   - Configure license
     - `$ ~/arc-server-10.9/arcgis/server/tools/authorizeSoftware -f <path_to_license_file> -e <email_address>`
       - To check status of license
         - `$ ~/arc-server-10.9/arcgis/server/tools/authorizeSoftware -s`
   - Create ArcPy conda environment
     - `$ conda create -n arcpy -c esri arcgis-server-py3=10.9.0`
       - If `conda config` keys named `channels` or `channel_priority` are 
       set, ensure that the `default` channel will be used for all non-ESRI 
       packages during the creation of this environment
     - `$ conda activate arcpy`
       - Until the `arcpy` env is configured, you will see an error like this 
       when it is activated:
         - ```
           ERROR: The ARCGISHOME variable is not set properly or path is not accessible.
           Manually set the variable by running the following command:
           export ARCGISHOME=/path/to/arcgis/server
           Then rerun the script to initialize the environment:
           source /users/c/k/ckocheno/miniconda3/envs/arcpy/bin/activate arcpy
           ```
         - **Don't** follow the instructions in the error message - follow the 
         instructions that follow, they are a better way to configure the 
         `arcpy` env
     - `$ conda env config vars set ARCGISHOME=path/to/arc-server-10.9/arcgis/server`
     - `$ conda activate arcpy`
       - Note: Reactivation of environment is needed before use
     - To test if Arc is installed, licensed, and available through the 
     Python environment
         - `$ conda activate arcpy`
         - `$ python`
         - `>>> import arcpy`
           - Should import within a few seconds, and give no error messages
         - `>>> quit()`
     - To use the Python environment that includes Arc functionality
       - For info on developing with Python 3 Runtime for ArcServer, 
       see `setup/ArcPy_reference.md`
       - `$ conda activate arcpy`
6. Setup PDAL conda environment
   - Install PDAL
     - `$ conda env create -f /vagrant/setup/environment_pdal.yaml`
   - To use the Python environment that includes PDAL functionality: 
     - `$ conda activate pdal`
7. Setup LAStools
   - Install LAStools
     - `$ sudo cp -r /vagrant/Odrive/Tools/PythonScripts/Terry/Installers/LAStools/LAStools_30Mar2023_use_for_LiDAR_DataPrep_v2.0 /opt`
     - `$ sudo chmod -R 755 /opt/LAStools_30Mar2023_use_for_LiDAR_DataPrep_v2.0`
   - Configure license
     - Only needed if the current license file was not included in the above 
     copy
     - `$ sudo cp <current SAL license file> /opt/LAStools_30Mar2023_use_for_LiDAR_DataPrep_v2.0/bin`
   - Configure Wine, the Windows emulator included with ArcServer we'll use to 
   run LAStools
     - Add some paths to your .bash_profile:
       - `$ echo 'export LD_LIBRARY_PATH=/home/vagrant/arc-server-10.9/arcgis/server/framework/runtime/xvfb/Xvfb/lib64:/home/vagrant/arc-server-10.9/arcgis/server/bin/wine/lib64:/home/vagrant/arc-server-10.9/arcgis/server/bin/wine/lib64/wine:/home/vagrant/arc-server-10.9/arcgis/server/framework/runtime/tomcat/bin:/home/vagrant/arc-server-10.9/arcgis/server/lib:' >> ~/.bash_profile`
       - `$ echo 'export FONTCONFIG_FILE=/home/vagrant/arc-server-10.9/arcgis/server/framework/runtime/xvfb/Xvfb/etc/fonts/fonts.conf' >> ~/.bash_profile`
   - Check that your `.bash_profile` has been updated:
     - `$ cat ~/.bash_profile`
   - Reload your profile
     - `$ source ~/.bash_profile`
   - Check licensing is active
     - `$ /home/vagrant/arc-server-10.9/arcgis/server/bin/wine/bin/wine64 lascopy64.exe -license`
   - To use LAStools on the VM, run its 64bit tools via the 64bit Wine 
   emulator included with Arc Server, for example:
     - `$ /home/vagrant/arc-server-10.9/arcgis/server/bin/wine/bin/wine64 las2las64.exe`
8. Setup `dataprep` environment
   - `$ conda env create -f /vagrant/setup/environment_dataprep.yaml`
9. Setup lidR (R) conda environment
   - `$ conda env create -f /vagrant/setup/environment_lidr.yaml`

## VM Maintenance
- When starting the VM, if a message appears to the effect of "Guest Additions 
version doesn't match that of Host"
  - Install the VB-Guest plugin into the project's Virtual Box, which will 
  keep the Guest Additions version in sync:
    - `> vagrant plugin install vagrant-vbguest`
  - When the host's Virtual Box is upgraded, update the plugin with: 
    - `> vagrant plugin update vagrant-vbguest`
  - If the plugin gets corrupted: 
    - `> vagrant plugin repair vagrant-vbguest`

## FileZilla settings to connect to the VM file system
- Protocol: `SFTP`
- Host: `localhost`
- Port: `2222`
- Logon type: `Key file`
- User: `vagrant`
- Key file: `path\to\lidar-dataprep\.vagrant\machines\default\virtualbox\private_key`

## Saving a restore point for the VM
The current state of a running VM can be saved as a restorable "snapshot".
This can be useful to recover a corrupted VM. Note that it does not save the
current state of any files shared to the VM, only those files stored within it.

All commands below are issued from the codebase root.
- To save a snapshot:
  - `> vagrant snapshot save <snapshot_name>`
    - Recommended naming convention: 
      - `<YYYYMMDD>_<description_seperated_by_underscores>`
        - ex: `20230530_two_user_arc`
- To list saved snapshots:
  - `> vagrant snapshot list`
- To restore a machine to a snapshot state:
  - `> vagrant snapshot restore <snapshot_name>`

## Adding a front end (GUI) to an existing VM
- In the VM, as the default (`vagrant`) user:
  - `$ sudo yum groupinstall "Server with GUI"`
  - `$ systemctl get-default`
    - Record this default value, such as `multi-user.target`, so you may 
    restore it later, if needed)
  - `$ sudo systemctl set-default graphical.target`
  - `$ sudo systemctl isolate graphical.target`
  - `$ exit`
- Update the `Vagrant file` to have `vb.gui = true`
- `> vagrant reload`
- A window will pop up with GUI access to the VM, but wait for the VM to 
fully boot up (see terminal output) before interacting with this window
- Login to GUI as `vagrant`/`vagrant`
  - `vagrant` is currently the only user that can access the VM thru the GUI
