## Software Dependencies

Versions of dependent applications that this codebase has been tested on;
see README and associated setup guides for install guidance:
- Arc:
  - Windows: ArcGIS Pro 2.8, 3.0
  - Linux: ArcServer 10.9.0
- LAStools: 30 March 2023 (last date in CHANGES.txt)
- FME: 2021.1
- PDAL: 2.3
- Fusion: 4.21

This codebase is to be run on the Python 3.7 `dataprep` conda environment.



