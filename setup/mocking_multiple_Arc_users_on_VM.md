## Mocking the running of Arc (and its Wine) installed by another user

This is not a part of the standard deployment of LiDAR Dataprep; it only 
needs to be used for development related to multiple users.

### Enter VM as the default (`vagrant`) user
1. Update `Vagrantfile` to use the `vagrant` user
2. `> vagrant up`
3. `> vagrant ssh` 

### Create second user `team_admin`
1. `$ sudo su -` (Become `root`, the admin role on Linux)
2. `# groupadd team`
3. `# useradd -m -s /bin/bash -U team_admin -u 667 --groups wheel -G team`
4. `# cp -pr /home/vagrant/.ssh /home/team_admin`
5. `# chown -R team_admin:team /home/team_admin`
6. `# echo "%team_admin ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/team_admin`
7. `# exit` (Stop acting as root)
8. `$ exit` (Exit the VM ssh session)

### Install FME
1. Install FME and license, still as `vagrant` user, acc. to vm setup doc

### Install and license ArcServer under the `team_admin` user
1. Update `Vagrantfile` to use the `team_admin` user
2. `> vagrant up`
3. `> vagrant ssh`
4. Install ArcServer and license acc. to VM setup doc
5. Install conda, create `arcpy` env, configure `arcpy` env, test Arc is 
working and licensed -- all acc. to vm setup doc
6. Exit the VM ssh session

### Use `vagrant` user to create third user `team_member_1`
1. Update `Vagrantfile` to use the `vagrant` user
2. `> vagrant up`
3. `> vagrant ssh`
4. `$ sudo su -`
5. `# chown -R team_admin:team /home/team_admin` (To update group ownership 
of the arc files added since the first use of this command)
6. `# useradd -m -s /bin/bash -U team_member_1 -u 668 --groups wheel -G team`
7. `# cp -pr /home/vagrant/.ssh /home/team_member_1`
8. `# chown -R team_member_1:team /home/team_member_1`
9. `# echo "%team_member_1 ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/team_member_1`
10. `# exit` (superuser)
11. `$ exit` (VM)

### Setup `team_member_1 to run LiDAR Dataprep`
1. Update `Vagrantfile` to use the `team_member_1` user
2. `> vagrant up`
3. `> vagrant ssh`
4. Install conda
5. Create arcpy env
6. `sudo su -`
7. `# cp -pr /home/team_admin/arc-server-10.9/arcgis/server/framework/runtime/.wine /home/team_member_1`
8. `# chown -R team_member_1:team /home/team_member_1/.wine`
9. `# exit`
10. `$ sudo chmod -R g+rwx /home/team_admin/arc-server-10.9/arcgis/server/`
11. `$ sudo chmod g+rx /home/team_admin/arc-server-10.9/arcgis/`
12. `$ sudo chmod g+rx /home/team_admin/arc-server-10.9/`
13. `$ sudo chmod g+rx /home/team_admin/`
14. `$ conda env config vars set ARCGISHOME=/home/team_admin/arc-server-10.9/arcgis/server`
15. `$ conda env config vars set WINEPREFIX=/home/team_member_1/.wine`
16. Test if Arc is working and licensed for `team_member_1`
    - `$ conda activate arcpy` gives error "Conda must be run by the ArcGIS 
    Server install user", but environment activates and the 
    standard licensing test passes
    - Note: WINEPREFIX is reverted to how it is for team_admin
17. `$ conda deactivate`
18. `$ conda env create -f /vagrant/setup/environment_dataprep.yaml`
19. `$ conda env create -f /vagrant/setup/environment_pdal.yaml`
20. `$ conda env create -f /vagrant/setup/environment_lidr.yaml`
21. `$ sudo cp -r /vagrant/Odrive/Tools/PythonScripts/Terry/Installers/LAStools/LAStools_30Mar2023_use_for_LiDAR_DataPrep_v2.0 /opt` 
22. `$ sudo chmod -R 755 /opt/LAStools_30Mar2023_use_for_LiDAR_DataPrep_v2.0`
23. `$ echo 'export LD_LIBRARY_PATH=/home/team_admin/arc-server-10.9/arcgis/server/framework/runtime/xvfb/Xvfb/lib64:/home/team_admin/arc-server-10.9/arcgis/server/bin/wine/lib64:/home/team_admin/arc-server-10.9/arcgis/server/bin/wine/lib64/wine:/home/team_admin/arc-server-10.9/arcgis/server/framework/runtime/tomcat/bin:/home/team_admin/arc-server-10.9/arcgis/server/lib:' >> ~/.bash_profile`
24. `$ echo 'export FONTCONFIG_FILE=/home/team_admin/arc-server-10.9/arcgis/server/framework/runtime/xvfb/Xvfb/etc/fonts/fonts.conf' >> ~/.bash_profile`
25. `$ source ~/.bash_profile`
26. Test that LAStools is working, via Wine
    - `$ cd /opt/LAStools_30Mar2023_use_for_LiDAR_DataPrep_v2.0/bin`
    - `$ /home/team_admin/arc-server-10.9/arcgis/server/bin/wine/bin/wine64 lascopy64.exe -license`
    - LAStools looks to be working
27. License FME for `team_member_1` user

### Run automated tests for `team_member_1`
1. `$ cd `/vagrant` (To the root of the codebase in the VM)
2. `$ conda activate dataprep`
3. Update the VM section of `software_paths.ini`
4. `$ pytest`
    - All tests pass for `team_member_1` user with Arc and Wine installed 
    under `team_admin`
    - Later, however, use of Arc failed for both users

### Next test, that worked for two users to run Arc:
1. Recreate VM
2. As `vagrant` user:
   - Install Arc for `vagrant` user, in `vagrant`'s home space
   - License with first user's license file
   - Run Arc Python test - passed
3. Snapshot VM
4. As `root`: 
   - Create second user
5. As second user:
   - Install Arc for second user, in second user's home space
   - License with second user's license file
   - Run Arc Python test - passed
6. Check that Arc Python test still passes for `vagrant` user - Yes
