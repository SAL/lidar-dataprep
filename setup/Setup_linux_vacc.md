# Set up to run on RedHat Linux on the VACC High Performance Cluster

- Log in to a VACC node: `> ssh <NetID>@<vacc-user1.uvm.edu|vacc-user2.uvm.edu>` 
  - Choose either VACC node - operationally they are identical, 
  performance on a node depends on the number of user logged in to it
  - Use NetID password
- Install this repo on the chosen VACC user node with one of the these methods:
  - (Preferred) Using Pycharm Remote Deployment (requires PyCharm Pro)
    - [Tutorial](https://www.jetbrains.com/help/pycharm/tutorial-deployment-in-product.html)
    - Key benefit of this method: When testing out codebase changes this 
    method provides for syncing local dev-machine code to the code installed 
    on the VACC without pushing those changes to GitLab, which keeps the 
    commit history cleaner.
  - (Otherwise) Using Git
    - `$ mkdir code`
    - `$ cd code`
    - `$ git clone https://gitlab.uvm.edu/SAL/lidar-dataprep.git`
      - Use your GitLab (not NetID) credentials
    - `$ cd lidar-dataprep`
    - `$ git checkout <branch>`
- Install Miniconda
  - `$ cd ~`
  - `$ bash <path>/lidar-dataprep/setup/setup_vm.sh`
  - Accept all defaults, apart from replying 'yes' to the running 
  of `conda init`
  - `$ source .bashrc` (or `exit` and re-ssh)
  - Check that conda is installed and initialized: `conda` should echo 
  the conda help info
- Set up a way to transfer files to/from the VACC
  - [VACC Knowledge Base](https://www.uvm.edu/vacc/kb/knowledge-base/move-a-file-to-the-cluster/)
- Setup Arc
  - Install ArcServer (done by VACC admins)
    - Copy ARCServer install archive to your VACC user folder 
      - Request install by VACC admins, for the sole use by your user account 
        - Needs to be installed as VACC admin user and chown'ed to the user for 
        correct licensing and use 
    - License as in VM instructions
  - Create ArcPy conda environment
    - Follow VM instructions
      - For `ARCGISHOME` use the path to the ArcServer installed for your user
- Setup FME - Only one install for our group is needed; If FME is already 
  installed, just do the section "License FME for your user"
  - Download FME installer to user folder
    - `$ wget https://downloads.safe.com/fme/2021/fme-desktop-2021-2021.1.3.0-21631.el7.x86_64.rpm`'
    - Create a folder into which the local FME repo will be built
    - Move the .rpm file to that folder
    - Update the .repo file with the path to that folder
      - `$ sed -i 's#/home/vagrant/redhat_repos/fme-desktop-2021#/users/<first-character-of-NetID>/<second-character-of-NetID>/<NetID>/fme-desktop-2021#g' fme-desktop-2021.repo`
  - Request VACC admin install FME
  - License FME for your user 
    - Enter these commands on the VACC user node:
      - `$ export LD_LIBRARY_PATH=/gpfs1/arch/x86_64-rhel7/fmelibs/lib64:/gpfs1/arch/x86_64-rhel7/fmelibs/usr/lib64/`
      - `$ export PATH=${PATH}:/gpfs1/arch/x86_64-rhel7/fme-desktop-2021.2.3/opt/fme-desktop-2021:/gpfs1/arch/x86_64-rhel7/fmelibs/usr/bin`
      - `$ /gpfs1/arch/x86_64-rhel7/fme-desktop-2021.2.3/opt/fme-desktop-2021/fmelicensingassistant --floating fmelm.uvm.edu database`
    - Then check that FME is now licensed for your user with this command:
      - `$ /gpfs1/arch/x86_64-rhel7/fme-desktop-2021.2.3/opt/fme-desktop-2021/fme`
- Setup LAStools
  - Copy LAStools (unzipped) folders to your VACC user folder
  - Configure Wine, the Windows emulator included with ArcServer we'll use to 
  run LAStools
    - Add some paths to your .bash_profile (updating the ArcServer paths to 
    the one installed for your user):
      -  If using the ArcServer at the global install location:
        - `$ echo 'export LD_LIBRARY_PATH=/gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/framework/runtime/xvfb/Xvfb/lib64:/gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/bin/wine/lib64:/gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/bin/wine/lib64/wine:/gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/framework/runtime/tomcat/bin:/gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/lib:/gpfs1/arch/x86_64-rhel7/fmelibs/lib64:/gpfs1/arch/x86_64-rhel7/fmelibs/usr/lib64/:' >> ~/.bash_profile`
        - `$ echo 'export FONTCONFIG_FILE=/gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/framework/runtime/xvfb/Xvfb/etc/fonts/fonts.conf' >> ~/.bash_profile`
        - `$ echo 'export WINEPREFIX=/gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/framework/runtime/.wine' >> ~/.bash_profile`
        - `$ echo 'export PATH=${PATH}:/gpfs1/arch/x86_64-rhel7/fme-desktop-2021.2.3/opt/fme-desktop-2021:/gpfs1/arch/x86_64-rhel7/fmelibs/usr/bin' >> ~/.bash_profile`
      -  If using an ArcServer installed for your user only at `~/Arcserver-10.9`:
        - `$ echo 'export LD_LIBRARY_PATH=~/Arcserver-10.9/arcgis/server/framework/runtime/xvfb/Xvfb/lib64:~/Arcserver-10.9/arcgis/server/bin/wine/lib64:~/Arcserver-10.9/arcgis/server/bin/wine/lib64/wine:~/Arcserver-10.9/arcgis/server/framework/runtime/tomcat/bin:~/Arcserver-10.9/arcgis/server/lib:/gpfs1/arch/x86_64-rhel7/fmelibs/lib64:/gpfs1/arch/x86_64-rhel7/fmelibs/usr/lib64/:' >> ~/.bash_profile`
        - `$ echo 'export FONTCONFIG_FILE=~/Arcserver-10.9/arcgis/server/framework/runtime/xvfb/Xvfb/etc/fonts/fonts.conf' >> ~/.bash_profile`
        - `$ echo 'export WINEPREFIX=~/Arcserver-10.9/arcgis/server/framework/runtime/.wine' >> ~/.bash_profile`
        - `$ echo 'export PATH=${PATH}:/gpfs1/arch/x86_64-rhel7/fme-desktop-2021.2.3/opt/fme-desktop-2021:/gpfs1/arch/x86_64-rhel7/fmelibs/usr/bin' >> ~/.bash_profile`
  - Check that your `.bash_profile` has been updated:
    - `$ cat ~/.bash_profile`
  - Reload your profile
    - `$ source ~/.bash_profile`
    - Check status of LAStools license (updating the ArcServer paths to the 
    one installed for your user):
      - `$ /gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/bin/wine/bin/wine64 /users/t/c/tcbarret/LAStools_30Mar2023_use_for_LiDAR_DataPrep_v2.0/bin/lascopy64.exe -license`
- Setup `dataprep`, `pdal`, and `lidr` conda environments
  - Follow VM instructions, updating paths in the commands for your VACC 
  deployment of this codebase
  - These environments need to be created in the `envs` folder within the 
  Miniconda you installed, above. If you see, with `conda info -e`, that they 
  are being created elsewhere, issue this command (updated to customize the 
  path to your Miniconda install:
    - `conda config --add envs_dirs <path>/miniconda3/envs`
- Set up scratch-space folders
  - Create the folders `~/scratch/temp_processing` and 
  `~/scratch/temp_processing/fme_temp`

## FileZilla settings to connect to the VACC file system
- Protocol: `SFTP`
- Host: `vacc-user1.uvm.edu`
- Port: ... leave empty ...
- Logon type: `Ask`
- User: `<NetID>`
- Password (when asked): `<NetID password>`
