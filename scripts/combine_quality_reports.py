"""
Combines multiple stages of quality reports into a single report.
Run this on the "dataprep" conda environment.
"""

from pathlib import Path

from numpy import int64, format_float_positional, nan
from pandas import read_csv, DataFrame, Series
from pandas.api.types import is_numeric_dtype

# TODO: How to also subset the log parsing for the subfolder choice?


### Input Parameters ###

project_folders = ['/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_1of2',
                   '/gpfs2/scratch/tcbarret/data_alt/FIA/fia_03_processing_2of2']
# project_folders = [r'E:\USGS_Downloader\test_lidR_in_Full_workflow_on_VACC_20220808\DE_18mo_2']

# QC Stage: `None` to turn off this filter; Ensure column label, below, matches
# qc_stage = None
# qc_stage = 'step_3_qc_cleaned'
# qc_stage = 'step_18_qc_classified'
# qc_stage = 'step_24_qc_tree_only'
# qc_stage = 'qc_cleaned'
# qc_stage = 'qc_classified'
qc_stage = 'qc_tree_only'

output_folder = '/gpfs2/scratch/tcbarret/dataprep_working'
# output_folder = r'E:\USGS_Downloader'

qc_report_filename = 'QC_of_pointcloud_tiles.csv'

join_to_input_report = True

# Sorting Report Columns
# The two additional parameters are only used
#   if sort_input_report_columns = True
# sort_input_report_columns = True
sort_input_report_columns = False
input_report_left_columns_not_sort = 2
input_report_columns_starting_keyword_not_sort = 'Downloaded '

# Note: if there is more than one "input report", any summary stats
#  will not be carried through
# input_reports = [
#     '/gpfs1/home/t/c/tcbarret/data/FIA/fia_02_retrievals_initial_quality_check/Entwine_downloads_as_of_20220406__with_downloaded_quality_metrics.csv',
#     '/gpfs1/home/t/c/tcbarret/data/FIA/fia_02_retrievals_initial_quality_check/noaa_retrievals_20220628__with_downloaded_quality_metrics.csv',
# ]
# input_reports = [
#     r'E:\USGS_Downloader\test_slurm_runs/Entwine_downloads_as_of_20220406__with_downloaded_quality_metrics.csv',
#     r'E:\USGS_Downloader\test_slurm_runs/noaa_retrievals_20220628__with_downloaded_quality_metrics.csv',
# ]
# input_reports = [
#     '/gpfs2/scratch/tcbarret/dataprep_working/cleaned/lidar_dataprep_report_thru_cleaned.csv',
# ]
input_reports = [
    '/gpfs2/scratch/tcbarret/dataprep_working/classified/lidar_dataprep_report_thru_classified.csv',
]

# Note: Filtered input results in the last-two-record-summary-stats for all
#       but the latest metrics to be removed (as they may no longer be accurate)
# filter_input = True
filter_input = False
# filter_nonzero_points = True  # Only used if filter_input=True
filter_nonzero_points = False
nonzero_point_threshold = 100  # points; Only used if filter_nonzero_points=True
filter_input_column = 'Downloaded RunName'
filter_input_runs = [
    'state16_30mo',
    'state33_60mo',
    'ND_30mo_qc_01',
    'TN_18mo_qc_01',
]

# Set to `True` to update "ClipFile" field to primary-key(filename-only) form
# update_clipfile_from_path_to_filename = True
update_clipfile_from_path_to_filename = False

# Set to `True to add "CN_as_string" field
# calculate_cn_as_string = True
calculate_cn_as_string = False

# joined_report_name = 'lidar_dataprep_report_thru_cleaned.csv'
# joined_report_name = 'lidar_dataprep_report_thru_classified.csv'
joined_report_name = 'lidar_dataprep_report_thru_tree.csv'

# Metric Run Name: Used to prefix metric field names in joined report
# metric_run_name = 'Downloaded '
# metric_run_name = 'Cleaned '
# metric_run_name = 'Classified '
metric_run_name = 'TreeOnly '

# Checking logs for errors only provides correct results on single-qc
#   project runs
# check_logs_for_errors = True
check_logs_for_errors = False

drop_qc_errors_columns = True
# drop_qc_errors_columns = False

########################


def format_float(num):
    """
    Used as a float formatter by DataFrame.to_csv() so that no scientific
    notation is written, and so all numbers before and after the decimal
    point are written. This is important so that very large point counts
    and very small point fractions are both carried through all reporting
    steps without loss of accuracy.
    """
    return format_float_positional(num, trim='-')


columns_not_to_sort = [
    'RunName',
    'FileName',
    'version',
    'VLR projection description',
    'VLR projection definition',
    'EVLR projection description',
    'EVLR projection definition',
    'point density',
    'pulse density',
    'point spacing',
    'pulse spacing',
    'area',
    'X min',
    'X max',
    'X range',
    'Y min',
    'Y max',
    'Y range',
    'Z min',
    'Z max',
    'Z range',
    ]

output_path = Path(output_folder)
report_files = []
for project_folder in project_folders:
    report_files.extend(sorted(Path(project_folder).rglob(qc_report_filename)))
if qc_stage:
    report_files = [file for file in report_files
                    if qc_stage in file.parts[-2].lower()]

combined_df = DataFrame()
for report_file in report_files:
    run_name = report_file.parents[1].name
    print(f'Processing {run_name}')
    report_df = read_csv(report_file, skipfooter=16, engine='python')
    report_df.insert(0, 'RunName', run_name)
    report_df.rename(columns={'Unnamed: 0': 'FileName'}, inplace=True)
    report_df['FileName'] = report_df['FileName'] + '.laz'
    combined_df = combined_df.append(report_df)

class_columns = list(set(combined_df.columns) - set(columns_not_to_sort))
class_columns.sort()
class_columns.sort(key=lambda x: len(x.split()[0]))
columns_not_to_sort.extend(class_columns)
combined_report_df = combined_df[columns_not_to_sort]
combined_report_df.drop(columns=[]).sort_values(['RunName', 'FileName']).to_csv(
    output_path / 'combined_quality_reports.csv', index=False)

if join_to_input_report:
    print(f'\nJoining quality metrics to downloader report(s): {input_reports}')
    input_report_df = DataFrame()
    for input_subset_report in input_reports:
        input_subset_report_df = read_csv(input_subset_report)
        if len(input_reports) > 1:
            if 'Point totals:' in input_subset_report_df.iloc[:, 0].values:
                row_condition = \
                    input_subset_report_df['State'] == 'Point totals:'
                input_subset_report_df = \
                    input_subset_report_df[~row_condition]
            if 'Percentage of total points:' in \
                    input_subset_report_df.iloc[:, 0].values:
                row_condition = \
                    input_subset_report_df['State'] == \
                    'Percentage of total points:'
                input_subset_report_df = \
                    input_subset_report_df[~row_condition]
            if nan in \
                    input_subset_report_df.iloc[:, 0].values:
                row_condition = \
                    input_subset_report_df['State'].isnan()
                input_subset_report_df = \
                    input_subset_report_df[~row_condition]
        input_report_df = input_report_df.append(input_subset_report_df,
                                                 ignore_index=True)

    # Filter runs, if requested
    if filter_input:
        if filter_nonzero_points:
            print(f'\nFiltering input report to those records with non-zero '
                  f'point counts after Downloaded QC')
            nonzero_points_in_download_qc_filter = \
                input_report_df['Downloaded total points'] \
                > nonzero_point_threshold
        else:
            nonzero_points_in_download_qc_filter = ~input_report_df.index.isna()
        print(f'\nFiltering input report to these {filter_input_column} '
              f'runs: {filter_input_runs}')
        run_filter = \
            input_report_df[filter_input_column].isin(filter_input_runs)
        combined_filter = run_filter & nonzero_points_in_download_qc_filter
        input_report_df = input_report_df[combined_filter]

    # Update "ClipFile" field to primary-key(filename-only) form, if requested
    if update_clipfile_from_path_to_filename:
        input_report_df['ClipFile_original'] = input_report_df['ClipFile']
        input_report_df['ClipFile'] = input_report_df['ClipFile'].apply(
            lambda x: Path(x).parts[-1]
        )

    # Add "CN_as_string" field, if requested
    if calculate_cn_as_string:
        reformatted_cn_s = input_report_df['CN'].fillna('0').astype(int64).astype(str).replace('0', '')
        input_report_df['CN_as_string'] = 'r' + reformatted_cn_s

    # Update "State" field to be two-letter-code form, if necessary
    state_dict = {
        1: 'AL',
        2: 'AK',
        4: 'AZ',
        5: 'AR',
        6: 'CA',
        8: 'CO',
        9: 'CT',
        10: 'DE',
        12: 'FL',
        11: 'DC',
        13: 'GA',
        15: 'HI',
        16: 'ID',
        17: 'IL',
        18: 'IN',
        19: 'IA',
        20: 'KS',
        21: 'KY',
        22: 'LA',
        23: 'ME',
        24: 'MD',
        25: 'MA',
        26: 'MI',
        27: 'MN',
        28: 'MS',
        29: 'MO',
        30: 'MT',
        31: 'NE',
        32: 'NV',
        33: 'NH',
        34: 'NJ',
        35: 'NM',
        36: 'NY',
        37: 'NC',
        38: 'ND',
        39: 'OH',
        40: 'OK',
        41: 'OR',
        42: 'PA',
        44: 'RI',
        45: 'SC',
        46: 'SD',
        47: 'TN',
        48: 'TX',
        49: 'UT',
        50: 'VT',
        51: 'VA',
        53: 'WA',
        54: 'WV',
        55: 'WI',
        56: 'WY',
        60: 'AS',
        64: 'FM',
        66: 'GU',
        68: 'MH',
        69: 'MP',
        70: 'PW',
        72: 'PR',  # In NOAA retrieval, and in PR detail table of PDF
        77: 'PR',  # In summary table of PDF
        78: 'VI',
    }
    if is_numeric_dtype(input_report_df['State']):
        input_report_df['State'] = input_report_df['State'].apply(
            lambda x: state_dict.get(x, str(x))
        )
        input_report_df.drop(columns=['statecd'], inplace=True)

    # Sort columns (if requested), drop Unnamed columns, and write
    #   combined input report
    if sort_input_report_columns:
        columns = list(input_report_df.columns)
        keyword_columns = [
            col for col in columns
            if col.startswith(input_report_columns_starting_keyword_not_sort)
        ]
        columns_to_sort = sorted(
            set(columns[input_report_left_columns_not_sort:])
            - set(keyword_columns)
        )
        input_report_df = \
            input_report_df[columns[:input_report_left_columns_not_sort]
                            + columns_to_sort + keyword_columns]
    named_columns = [col for col in list(input_report_df.columns)
                     if not col.startswith('Unnamed')]
    input_report_df = input_report_df[named_columns]
    input_report_df.to_csv(output_path / 'combined_input_reports.csv',
                           index=False)

    # Prefix field names before merge
    prefixed_metric_field_names = [f'{metric_run_name}{col}'
                                   for col in combined_report_df.columns]
    combined_report_df.columns = prefixed_metric_field_names

    input_plus_qc_report_df = \
        input_report_df.merge(combined_report_df, how='left',
                              left_on='ClipFile',
                              right_on=f'{metric_run_name}FileName')

    print(f'\nFile Category counts: '
          f'\n{input_plus_qc_report_df["ClipFileSizeCat"].value_counts()}')

    possible_missed_metrics_df = input_plus_qc_report_df[
        (input_plus_qc_report_df['ClipFileSizeCat'] == 'CandidateFile')
        & (input_plus_qc_report_df[
               f'{metric_run_name}total points'].isna())].sort_values(
        ['ClipFileSize'], ascending=False)
    missed_filenames = sorted(possible_missed_metrics_df['ClipFile'])
    missed_filename_dict = {filename: '' for filename in missed_filenames}

    if check_logs_for_errors:
        # TODO: Update for multiple project paths
        print('\nReading in log files (.out) in the project root, if any, '
              'and extracting relevant messages for CandidateFiles missing '
              'quality metrics')
        log_files = sorted(project_path.glob('*.out'))
        log_list = []
        for log_file in log_files:
            with open(log_file, 'r') as f:
                print(f'Reading and parsing: {log_file}')
                line_previous = ''
                for line in f:
                    line_cleaned = line.strip()
                    for filename in missed_filenames:
                        if filename[:-4].lower() in line.lower():
                            missed_filename_dict[filename] \
                                += f'{line} | previous line: {line_previous}'
                    line_previous = line
                    if any([
                        line_cleaned.lower().startswith('error'),
                        line_cleaned.lower().startswith('warning'),
                        any([filename[:-4].lower() in line_cleaned.lower()
                             for filename in missed_filenames]),
                    ]):
                        log_list.extend([line_cleaned + '\n'])

        with open(output_path / 'cleaned_log.txt', 'w') as f:
            f.writelines(log_list)

        # Join relevant logged messages with the "missing metrics" DataFrame
        missed_filename_s = Series(data=missed_filename_dict,
                                   name=f'{metric_run_name}QcErrors')
        possible_missed_metrics_df = \
            possible_missed_metrics_df.merge(missed_filename_s,
                                             left_on='ClipFile',
                                             right_index=True)
    print(f'\nWriting report of records missing quality metrics')
    possible_missed_metrics_df.to_csv(
        output_path / 'possible_missed_metrics.csv',
        index=False
    )
    print(f'\nCount of CandidateFile with no points '
          f'counted (total: {len(possible_missed_metrics_df)}): '
          f'\n{possible_missed_metrics_df["State"].value_counts()}')

    prefixed_class_names = [f'{metric_run_name}{col}' for col in class_columns]
    point_totals_df = \
        input_plus_qc_report_df[
            prefixed_class_names
        ].sum().astype(int64).to_frame(name='Point totals').T
    point_percent_of_total_df = \
        point_totals_df / \
        list(point_totals_df[f'{metric_run_name}total points'].values)[0] * 100
    point_percent_of_total_df.rename(
        index={'Point totals': 'Percentage of total points'}, inplace=True)
    point_percent_of_total_df.drop(columns=[f'{metric_run_name}total points'],
                                   inplace=True)

    print('\n', point_totals_df.squeeze())
    print('\n', point_percent_of_total_df.squeeze())

    point_totals_df.loc['Point totals', 'State'] = 'Point totals:'
    point_percent_of_total_df.loc['Percentage of total points', 'State'] = \
        'Percentage of total points:'
    input_plus_qc_report_df = \
        input_plus_qc_report_df.drop(columns=[f'{metric_run_name}FileName'])
    if 'Point totals:' not in input_plus_qc_report_df['State'].values:
        input_plus_qc_report_df = \
            input_plus_qc_report_df.append(DataFrame(index=[None]))
        input_plus_qc_report_df = \
            input_plus_qc_report_df.append(point_totals_df)
        input_plus_qc_report_df = \
            input_plus_qc_report_df.append(point_percent_of_total_df)
    else:
        point_totals_row_condition = \
            input_plus_qc_report_df['State'] == 'Point totals:'
        for col in point_totals_df.columns:
            if col != 'State':
                input_plus_qc_report_df.loc[point_totals_row_condition, col] = \
                    point_totals_df.loc['Point totals', col]
        point_percent_row_condition = \
            input_plus_qc_report_df['State'] == 'Percentage of total points:'
        for col in point_percent_of_total_df.columns:
            if col != 'State':
                input_plus_qc_report_df.loc[
                    point_percent_row_condition, col
                ] = point_percent_of_total_df.loc['Percentage of total points',
                                                  col]

    if check_logs_for_errors:
        input_plus_qc_report_df = \
            input_plus_qc_report_df.merge(missed_filename_s,
                                          how='left',
                                          left_on='ClipFile',
                                          right_index=True)

    # Sort report, leaving summary rows at the bottom
    input_plus_qc_report_df = \
        input_plus_qc_report_df.loc[~input_plus_qc_report_df['State'].isnull()]
    condition = input_plus_qc_report_df['State'].isin([
        'Point totals:', 'Percentage of total points:'])
    rows_to_sort_df = input_plus_qc_report_df[~condition]
    rows_not_sort_df = input_plus_qc_report_df[condition]
    input_plus_qc_report_df = \
        rows_to_sort_df.sort_values(['State', 'CN']).append(
            DataFrame(index=[None])).append(rows_not_sort_df)

    # Drop QC Errors columns, if requested
    if drop_qc_errors_columns:
        qc_errors_columns = [col for col in input_plus_qc_report_df.columns
                             if 'QcErrors'.lower() in col.lower()]
        print(f'\nDropping QC Error columns, as requested: {qc_errors_columns}')
        input_plus_qc_report_df.drop(columns=qc_errors_columns, inplace=True)

    print(f'\nWriting joined report')
    # Note: Don't sort by index after totals added to bottom, above
    input_plus_qc_report_df.to_csv(output_path / f'{joined_report_name}',
                                   index=False, float_format=format_float)
