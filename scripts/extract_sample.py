"""
Extracts a random, thresholded subset of summary report records, and then
optionally copies the data products associated with the subset to a new folder
with folder/file naming designed to facilitate visual QC of the data
products.
Run this on the "dataprep" conda environment.
"""
from pathlib import Path
from shutil import copy
from time import sleep

from pandas import read_csv


# Parameters #

# Locations
source_data = '/gpfs1/home/t/c/tcbarret/sample_extraction_example/input/VT_USGS'
source_table = '/gpfs1/home/t/c/tcbarret/sample_extraction_example/input/All_USGS_NOAA_retrievals_Processed_by_SAL_thru_full_workflow_20230207.csv'
output_folder = '/gpfs1/home/t/c/tcbarret/sample_extraction_example/expected_output/extract_sample_VT_USGS_test'
report_basename = 'stratified_random_sampling_of_data_product_tile_sets'

# Sampling
number_of_samples_per_category = 3
copy_files = True

# Define table-to-data connections
# Keys must be QC-stage keywords in QC report column names
table_to_folder_dict = {
    'qc02_cleaned': {
        'pointclouds_folder_name': 'step_1_pointclouds_cleaned',
        'data_product_folder_names': {
            'QC': 'step_3_qc_cleaned',
            'DEM': str(Path('delivery') / 'step_4_dem_1m'),
            'DSM': str(Path('delivery') / 'step_6_dsm_1m'),
            'DTM': str(Path('delivery') / 'step_8_dtm_1m'),
            'INT': str(Path('delivery') / 'step_10_int_1m')
        }
    },
    'qc03_classified': {
        'pointclouds_folder_name': 'step_17_pointclouds_classified',
        'data_product_folder_names': {
            'QC': 'step_18_qc_classified',
            'nDSM': str(Path('delivery') / 'step_15_ndsm_1m'),
            'nDSMtree': str(Path('delivery') / 'step_19_ndsm_tree_1m'),
            'nDSMbuilding': str(Path('delivery') / 'step_21_ndsm_building_1m'),
        }
    },
    'qc04_treeonly': {
        'pointclouds_folder_name': 'step_23_pointclouds_tree_only',
        'data_product_folder_names': {
            'QC': 'step_24_qc_tree_only',
        }
    }
}

# Table configuration
footer_records_to_skip = 0  # Use to not read in "summary", non-record rows
filename_column = 'ClipFile'
run_column = 'constructed_runname'  # Will be created at runtime
viable_count_general = 1000
viable_count_tree = 100

# Table filtering
filter_input_column = 'constructed_runname'
filter_input_runs = [
    'VT_18mo',
    'VT_30mo',
    ]


# Processing #


def month_bin(months):
    if months >= 0.0:
        if months <= 18:
            return '18mo'
        elif months <= 30:
            return '30mo'
        elif months <= 60:
            return '60mo'
        else:
            return '>60mo'
    else:
        return ''


output_path = Path(output_folder)
print(f'Creating output folder {str(output_path)}')
output_path.mkdir()
print(f'Reading reference table {str(source_table)}')
report_df = read_csv(source_table, skipfooter=footer_records_to_skip,
                     engine='python')
report_df['month_duration_bin'] = report_df['MonthDelta'].apply(month_bin)
report_df['constructed_runname'] = report_df['State'] + '_' \
                                   + report_df['month_duration_bin']

# Filter to CandidateFile, USGS records only
report_df = report_df.loc[report_df['ClipFileSizeCat'].isin(['CandidateFile'])]
report_df = report_df.loc[report_df['ClipFile_original'].isna()]

if filter_input_runs:
    report_df = \
        report_df.loc[report_df[filter_input_column].isin(filter_input_runs)]
    print(f'Writing filtered input table to {str(output_path)}')
    report_df.to_csv(
        output_path / f'{report_basename}__filtered_input_table.csv'
    )



def group_downloaded(x):
    if report_df.loc[x, 'Downloaded 1  unclassified'] > viable_count_general \
            and report_df.loc[x, 'Downloaded 2  ground'] > viable_count_general:
        return 'downloaded_viable'
    else:
        return 'downloaded_nonviable'


def group_cleaned(x):
    if report_df.loc[x, 'Cleaned 1  unclassified'] > viable_count_general \
            and report_df.loc[x, 'Cleaned 2  ground'] > viable_count_general:
        return 'cleaned_viable'
    else:
        return 'cleaned_nonviable'


def group_classified(x):
    if report_df.loc[x, 'Classified 1  unclassified'] > viable_count_general \
            and report_df.loc[x, 'Classified 2  ground'] > viable_count_general:
        return 'classified_viable'
    else:
        return 'classified_nonviable'


def group_tree(x):
    if report_df.loc[x, 'TreeOnly total points'] > viable_count_tree:
        return 'tree_viable'
    else:
        return 'tree_nonviable'


report_grouped_raw = report_df.groupby([
    'p_method',
    group_downloaded,
    group_cleaned,
    group_classified,
    group_tree
], dropna=False)

for group_name in report_grouped_raw.groups:
    indices = list(report_grouped_raw.groups[group_name])
    report_df.loc[indices, 'count_in_sampling_group'] = len(indices)
    report_df.loc[indices, 'sampling_group'] = str(group_name)

print(f'Writing filtered input table (with new '
      f'fields added) to {str(output_path)}')
report_df.to_csv(output_path / f'{report_basename}__filtered_plus.csv')

report_grouped_labeled = report_df.groupby('sampling_group')
report_samples_df = report_grouped_labeled.sample(
    n=number_of_samples_per_category, replace=True)
report_samples_df.reset_index(inplace=True)
report_samples_df.drop_duplicates(subset='CN_as_string', ignore_index=True,
                                  inplace=True)

if copy_files:
    # For visual QC (and as a preview for how to deliver the project overall?)
    #  copy all outputs to a single directory for each input record,
    #  with files renamed for ease of import to Arc, etc
    source_path = Path(source_data)
    num_sample_records = len(report_samples_df)
    num_sample_records_digits = len(str(num_sample_records))
    for sample_index, sample_record in report_samples_df.iterrows():
        sample_basename = Path(sample_record['ClipFile']).stem
        run_name = sample_record[run_column]
        output_basename_path = \
            output_path / \
            f'item{str(sample_index).zfill(num_sample_records_digits)}_' \
            f'{sample_basename}'
        print(f'Creating folder: {str(output_basename_path)}')
        output_basename_path.mkdir(exist_ok=True)
        source_run_path = source_path / f'{run_name}_USGS'
        for level_name, level_dict in table_to_folder_dict.items():
            # Copy pointclouds for each level
            level_input_pointclouds_path = \
                source_run_path / level_dict['pointclouds_folder_name']
            level_input_pointcloud_filename = f'{sample_basename}.laz'
            level_input_pointclouds_filepath = \
                level_input_pointclouds_path / level_input_pointcloud_filename
            level_output_pointcloud_filename = \
                f'{level_name}_pointcloud__{sample_basename}.laz'
            level_output_pointcloud_filepath = \
                output_basename_path / level_output_pointcloud_filename
            pointcloud_column_name = f'{level_name}: Pointcloud'
            report_samples_df.loc[sample_index, pointcloud_column_name] = ''
            if not level_input_pointclouds_filepath.is_file():
                print(f'Source file does not '
                      f'exist: {str(level_input_pointclouds_filepath)}')
                report_samples_df.loc[sample_index,
                                      pointcloud_column_name] = 'no file'
            else:
                print(f'Copying {str(level_input_pointclouds_filepath)} to '
                      f'{str(level_output_pointcloud_filepath)}')
                try:
                    copy(level_input_pointclouds_filepath,
                         level_output_pointcloud_filepath)
                    sleep(.1)
                    report_samples_df.loc[sample_index,
                                          pointcloud_column_name] = \
                        str(level_output_pointcloud_filepath.stat().st_size)
                except Exception as e:
                    print(f'Unable to copy the file:\n{e}')
                    report_samples_df.loc[sample_index,
                                          pointcloud_column_name] = 'copy error'

            # Copy data products for each level
            for data_product, data_product_folder \
                    in level_dict['data_product_folder_names'].items():
                data_product_input_path = source_run_path / data_product_folder
                data_product_input_filepaths = \
                    sorted(data_product_input_path.glob(f'{sample_basename}*'))
                dataproduct_column_name = f'{level_name}: {data_product}'
                report_message = ''
                report_samples_df.loc[
                    sample_index, dataproduct_column_name] = report_message
                if not data_product_input_filepaths:
                    print(f'Source file(s) for the {data_product} data '
                          f'product do not exist')
                    report_samples_df.loc[
                        sample_index, dataproduct_column_name] = 'no file'
                else:
                    for input_filepath in data_product_input_filepaths:
                        output_filename = \
                            f'{level_name}_{data_product}__' \
                            f'{input_filepath.name.split(".")[0]}' \
                            f'{"".join(input_filepath.suffixes)}'
                        output_filepath = output_basename_path / output_filename
                        print(f'Copying {str(input_filepath)} '
                              f'to {str(output_filepath)}')
                        message_seperator = '' if report_message == '' else ', '
                        try:
                            copy(input_filepath, output_filepath)
                            sleep(.1)
                            report_message = \
                                f'{report_message}{message_seperator}' \
                                f'{str(output_filepath.stat().st_size)}'
                            report_samples_df.loc[
                                sample_index, dataproduct_column_name
                            ] = report_message
                        except Exception as e:
                            print(f'Unable to copy the file:\n{e}')
                            report_message = \
                                f'{report_message}{message_seperator}copy error'
                            report_samples_df.loc[
                                sample_index, dataproduct_column_name
                            ] = report_message

print(f'Writing sampling report to {str(output_path)}')
report_samples_df.to_csv(
    output_path / f'{report_basename}___sampling_report.csv'
)
