"""
Finds index shapefiles in multiple project folders, merges them,
creates a new geodatabase, imports the merged shapefile into that
gdb as a feature class, and joins a csv table of metrics to that
feature class.
Run this on the ArcGIS conda environment.
"""
from pathlib import Path

from arcpy import env, FeatureClassToGeodatabase_conversion, \
    Merge_management, AddJoin_management, CopyFeatures_management, \
    CalculateFields_management, CreateFileGDB_management


# Parameters #

project_folders = [
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_1_NRS',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_2_NRS',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_3_NRS',
    '/gpfs2/scratch/tcbarret/data_alt/FIA/fia_03_processing_4_NRS',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_1_PNW',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_2_PNW',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_3_PNW',
    '/gpfs2/scratch/tcbarret/data_alt/FIA/fia_03_processing_4_PNW',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_1_RMRS',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_2_RMRS',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_3_RMRS',
    '/gpfs2/scratch/tcbarret/data_alt/FIA/fia_03_processing_4_RMRS',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_1_SRS',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_2_SRS',
    '/gpfs1/home/t/c/tcbarret/data/FIA/fia_03_processing_3_SRS',
    '/gpfs2/scratch/tcbarret/data_alt/FIA/fia_03_processing_4_SRS',
]
# project_folders = [
#     r'E:\FIA_geodatabase\shp_indexes\NOAA',
#     r'E:\FIA_geodatabase\shp_indexes\USGS',
# ]

merged_shp = '/gpfs2/scratch/tcbarret/output/cleaned_index.shp'
# merged_shp = r'E:\FIA_geodatabase\shp_indexes\merged_indexes_4.shp'


joined_feature_name = 'quality_metrics_joined_to_cleaned_index_20230407'
gdb_name = 'quality_metrics_joined_to_cleaned_index_20230407.gdb'

gdb_location = '/gpfs2/scratch/tcbarret/output'
# gdb_location = r'E:\FIA_geodatabase\map'

metric_csv = '/gpfs1/home/t/c/tcbarret/data/FIA/fia_00_reporting/All_USGS_NOAA_retrievals_Processed_by_SAL_thru_full_workflow_20230207.csv'
# metric_csv = r'E:\FIA_geodatabase\metrics\All_USGS_NOAA_retrievals_Processed_by_SAL_thru_full_workflow_20230207.csv'

metric_join_field = 'ClipFile'
merged_index_join_field = 'Name'
index_folder_name = 'step_2_pointcloud_tile_index'


# Processing #

print('Creating new geodatabase')
CreateFileGDB_management(gdb_location, gdb_name)
gdb_filepath_str = str(Path(gdb_location) / gdb_name)
env.workspace = gdb_filepath_str

print('Merging shapefiles:')
shp_files_str = []
for project_folder in project_folders:
    shp_files = \
        sorted(Path(project_folder).glob(f'*/{index_folder_name}/*.shp'))
    shp_files_str.extend([str(file_path) for file_path in shp_files])
[print(shp) for shp in shp_files_str]
Merge_management(shp_files_str, merged_shp)

print('Converting merged shapefile to feature class')
FeatureClassToGeodatabase_conversion(merged_shp, gdb_filepath_str)

print('Joining table to feature class')
merged_index_feature_name = Path(merged_shp).stem
CalculateFields_management(merged_index_feature_name, "PYTHON3",
                           [[merged_index_join_field,
                             f"!{merged_index_join_field}! + '.laz'"]])
joined_index = AddJoin_management(merged_index_feature_name,
                                  merged_index_join_field,
                                  metric_csv,
                                  metric_join_field)
CopyFeatures_management(joined_index, joined_feature_name)

print('Processing complete')
