"""
Analyzes a combined QC report using Pandas GroupBy.
Run this on the "dataprep" conda environment.
"""
from pathlib import Path

from pandas import read_csv


# Parameters #

report_to_analyze = r'E:\USGS_Downloader\test_slurm_runs\quality_check_20220714_geiger_vs_linear_quality_metrics_thru_tree.csv'
both_summary_records_to_skip = 3
output_location = r'E:\USGS_Downloader\test_slurm_runs'

group_by_fields = ['p_method']
# group_by_fields = ['p_method', 'ClipFileSizeCat']

filter_input_column = 'constructed_runname'
filter_input_runs_thru_cleaning = [
    'GA_18mo',
    'IL_18mo',
    'IL_30mo',
    'IN_18mo',
    'NC_18mo',
    'NC_30mo',
    'ND_30mo',
    'ND_60mo',
    'SC_18mo',
    'SC_30mo',
    'SD_18mo',
    'SD_30mo',
    'SD_60mo',
    'TN_18mo',
    'TN_30mo',
    'VA_30mo',
    'WI_18mo',
    'WI_30mo',
]
filter_input_runs_thru_tree = [
    'GA_18mo',
    'IL_18mo',
    'IL_30mo',
    'IN_18mo',
    'ND_30mo',
    'ND_60mo',
    'SC_18mo',
    'SC_30mo',
    'SD_18mo',
    'SD_30mo',
    'SD_60mo',
    'TN_18mo',
    'TN_30mo',
    'VA_30mo',
    'WI_18mo',
    'WI_30mo',
]

viable_count_general = 1000
viable_count_tree = 100


# Processing #

output_path = Path(output_location)
report_df = read_csv(report_to_analyze,
                     skipfooter=both_summary_records_to_skip)
report_df.loc[report_df['ClipFileSizeCat'] ==
              'CandidateFile', 'is_candidate'] = 'CandidateFile'


def month_bin(months):
    if months >= 0.0:
        if months <= 18:
            return '18mo'
        elif months <= 30:
            return '30mo'
        elif months <= 60:
            return '60mo'
        else:
            return '>60mo'
    else:
        return ''


report_df['month_duration_bin'] = report_df['MonthDelta'].apply(month_bin)
report_df['constructed_runname'] = report_df['State'] + '_' \
                                   + report_df['month_duration_bin']

# Analyze thru-Downloaded runs
report_grouped_by_method = report_df.groupby(group_by_fields)
report_grouped_by_method_counts_df = report_grouped_by_method.count()
report_grouped_by_method_counts_subset_df = \
    report_grouped_by_method_counts_df[[
        'State',
        'is_candidate',
        'Downloaded total points',
    ]]
grouped_counts_subset_sum_s = report_grouped_by_method_counts_subset_df.sum()
grouped_counts_subset_sum_s.name = 'Total'
grouped_counts_subset_combined_df = \
    report_grouped_by_method_counts_subset_df.append(
        grouped_counts_subset_sum_s.T)
grouped_counts_subset_combined_df.to_csv(
    output_path / 'grouped_counts_subset_combined_df.csv'
)

# Analyze thru-Cleaned runs
print(f'\nFiltering input report to these {filter_input_column} runs, '
      f'processed through Cleaning: {filter_input_runs_thru_cleaning}')
run_filter_cleaned = \
    report_df[filter_input_column].isin(filter_input_runs_thru_cleaning)
report_run_subset_cleaned_df = report_df[run_filter_cleaned]
report_run_subset_cleaned_df.loc[
    (report_run_subset_cleaned_df['Downloaded 1  unclassified']
     > viable_count_general)
    & (report_run_subset_cleaned_df['Downloaded 2  ground']
       > viable_count_general),
    'Downloaded viable point cloud'] = 'viable'
report_run_subset_cleaned_df.loc[
    (report_run_subset_cleaned_df['Cleaned 1  unclassified']
     > viable_count_general)
    & (report_run_subset_cleaned_df['Cleaned 2  ground']
       > viable_count_general),
    'Cleaned viable point cloud'] = 'viable'

subset_cleaned_report_grouped_by_method = \
    report_run_subset_cleaned_df.groupby(group_by_fields)
subset_cleaned_report_grouped_by_method_counts_df = \
    subset_cleaned_report_grouped_by_method.count()
subset_cleaned_report_grouped_by_method_counts_subset_df = \
    subset_cleaned_report_grouped_by_method_counts_df[[
        'State',
        'is_candidate',
        'Downloaded total points',
        'Downloaded viable point cloud',
        'Cleaned total points',
        'Cleaned viable point cloud',
    ]]
subset_cleaned_grouped_counts_subset_sum_s = \
    subset_cleaned_report_grouped_by_method_counts_subset_df.sum()
subset_cleaned_grouped_counts_subset_sum_s.name = 'Total'
subset_cleaned_grouped_counts_subset_combined_df = \
    subset_cleaned_report_grouped_by_method_counts_subset_df.append(
        subset_cleaned_grouped_counts_subset_sum_s.T)
subset_cleaned_grouped_counts_subset_combined_df.to_csv(
    output_path / 'subset_cleaned_grouped_counts_subset_combined_df.csv')
report_run_subset_cleaned_df.to_csv(
    output_path / 'analyzed_combined_quality_report_subset_cleaned.csv')

# Analyze thru-Tree runs
print(f'\nFiltering input report to these {filter_input_column} runs, '
      f'processed through TreeOnly: {filter_input_runs_thru_tree}')
run_filter_tree = \
    report_df[filter_input_column].isin(filter_input_runs_thru_tree)
report_run_subset_tree_df = report_df[run_filter_tree]
report_run_subset_tree_df.loc[
    (report_run_subset_tree_df['Downloaded 1  unclassified']
     > viable_count_general)
    & (report_run_subset_tree_df['Downloaded 2  ground']
       > viable_count_general),
    'Downloaded viable point cloud'] = 'viable'
report_run_subset_tree_df.loc[
    (report_run_subset_tree_df['Cleaned 1  unclassified']
     > viable_count_general)
    & (report_run_subset_tree_df['Cleaned 2  ground']
       > viable_count_general),
    'Cleaned viable point cloud'] = 'viable'
report_run_subset_tree_df.loc[
    (report_run_subset_tree_df['Classified 1  unclassified']
     > viable_count_general)
    & (report_run_subset_tree_df['Classified 2  ground']
       > viable_count_general),
    'Classified viable point cloud'] = 'viable'
report_run_subset_tree_df.loc[
    report_run_subset_tree_df['TreeOnly total points'] > viable_count_tree,
    'TreeOnly viable point cloud'] = 'viable'

subset_tree_report_grouped_by_method = \
    report_run_subset_tree_df.groupby(group_by_fields)
subset_tree_report_grouped_by_method_counts_df = \
    subset_tree_report_grouped_by_method.count()
subset_tree_report_grouped_by_method_counts_subset_df = \
    subset_tree_report_grouped_by_method_counts_df[[
        'State',
        'is_candidate',
        'Downloaded total points',
        'Downloaded viable point cloud',
        'Cleaned total points',
        'Cleaned viable point cloud',
        'Classified total points',
        'Classified viable point cloud',
        'TreeOnly total points',
        'TreeOnly viable point cloud'
    ]]
subset_tree_grouped_counts_subset_sum_s = \
    subset_tree_report_grouped_by_method_counts_subset_df.sum()
subset_tree_grouped_counts_subset_sum_s.name = 'Total'
subset_tree_grouped_counts_subset_combined_df = \
    subset_tree_report_grouped_by_method_counts_subset_df.append(
        subset_tree_grouped_counts_subset_sum_s.T)
subset_tree_grouped_counts_subset_combined_df.to_csv(
    output_path / 'subset_tree_grouped_counts_subset_combined_df.csv')
report_run_subset_tree_df.to_csv(
    output_path / 'analyzed_combined_quality_report_subset_tree.csv')
