"""
Run LiDAR DataPrep
"""

from argparse import ArgumentParser
from pathlib import Path
from shutil import rmtree, copyfile
from sys import path
from time import sleep

from numpy import around
from pandas import DataFrame, Series

from src.run_subprocess import run_subprocess
from src.utilities.run_environment import RetrieveRunEnv
from src.workflow_parser import workflow_parser


# TODO: # Miscellaneous #
# TODO: Zero-pad step number folder names
# TODO: Add task killing for Linux and Windows Server
# TODO: Option to stop pipeline if any step fails (or in the case of FME,
#  completes with no output)
# TODO: Possible alternate methods of resetting CRS and units of mosaics:
#  GlobalMapper CLI (Noah), gdal (Ernie)
# TODO: Simple GUI that writes INI files, and runs them. Will obviate the
#  step re-numbering hassle, and allow for ease of defining sub-workflows of
#  multiple steps (classfying w/o edge effects, surface modeling, etc.)
# TODO: Validate ArcPro license is good (user is logged in) before running
#  workflow (see README for manual method)
# TODO: Check FME and LAStools licenses are active before running workflow
#  (see README for manual method)
# TODO: Re-test for restart capability throughout workflows; allow for
#  external tiling index for FME tools
# TODO: Way to measure the relevant parameters of a tile to know when
#  las2dem will fail for too many triangles

# TODO: # Process monitoring/interaction #
# TODO: Time estimation, cores/memory/disk-write optimization
# TODO: Record pid of every spawned subprocess so can kill it (and its
#  dependent processes) directly, not by image name as is done currently

# TODO: # Release prep #
# TODO: Review all TODOs of codebase
# TODO: Update release notes
# TODO: Update README (making sure to do all updates related to items in
#  release notes)
# TODO: Update the INI header notes with new/changed capabilities
# TODO: Incorporate INIs and branches developed by others
# TODO: Verify correct running and output of all test-dataset
#  workflows -- using CMD.exe, not (only) PyCharm
# TODO: Update and pass all unit tests
# TODO: Document updated Arc/FME/LAStools dependencies
# TODO: Bump version

# TODO: # Improvements for cleaner/faster processing #
# TODO: One core-count input (max cores to use), multiplying-factor
#  parameters (% of max cores to use)
# TODO: Remove need to (re)number sections (Will assist with editing
#  workflows, restarting after interruption, etc)
# TODO: Unit conversion (scaling tile size, etc for a change in units)
# TODO: For the FME submodules currently hardcoded to TIF, add that to
#  name, and later generalize to have format as a parameter
# TODO: Use lastools parameters to ignore noise classes instead of the
#  dropping steps (and extra copies they entail of whole source dataset)?
# TODO: Add applications used inside FileManagement tools to the list of
#  applications requiring verification as existing
# TODO: Add any OutputFilename from RenameRaster to the list of filenames
#  allowable as ImageryFile for Tile_Delivery_Imagery_TIF
# TODO: Step parameter SkipForProduction that will drop that step and
#  renumber the others before running -- useful for having two ways of
#  doing something for the sample tiles, and one way for full tile set

# TODO: # Parallel processing # --> "concurrent step processing with
#  multiple working drives"
# TODO: Produce concurrency data strucure based on input and output
#  directories, this will identify which steps can be parallelized
# TODO: Implement temp working folders for FME steps before
#  multiprocessing - Windows environment variable

# TODO: # QA/QC capabilities #
# TODO: LASinfo submodule: Grab withheld and overlap counts (whose lines
#  start with "+")
# TODO: Estimate hard-drive space that a workflow will need and check
#  against what is available on Working folder drive
# TODO: Check for non-orthogonal pointcloud tiles (mismatch of index
#  tiles vs. actual tile boundaries)
# TODO: Check for points outside the bounding box, a situation that
#  happened in CMS CT_6 and prevented processing

# TODO: # New Workflow - tile imagery to match source pointclouds, and
#  classify pointclouds without edge effects #
# TODO: Explore MRF format recommended by ESRI for fastest raster display:
#  http://doc.arcgis.com/en/imagery/workflows/standard-workflow/preparing-your-data/data-sources-and-formats.htm
# TODO: Add capability to produce IMG/MRF rasters
# TODO: Set CRS on specified folder, not only on the Delivery folder
# TODO: Convert pyramids/stats tools to use the "Batch" versions of the
#  arcpy methods to get multiprocessing speed-up -- or use multiprocessing
#  module to do this
# TODO: FME clipping tools: Speed up with source-order tweaks, provide for
#  CRS transform
# TODO: Provide for selection of output format (LAS/LAZ, 1.2/1.4) with
#  Tile_Pointclouds FME tool (currently hardcoded to 1.2 LAZ)
# TODO: New tool - Retile debuffered surface model tiles like is done
#  with leaf on/off -- useful (and quicker) if mosaic surface models not needed
# TODO: New "all-buffered-tiles" output, including a BufferTileIndex tool


# ------------------------------------------------------------------------------
# -- Internal Parameters

# Bump, with '-beta', after creating a new branch from develop, just after
# a release; When ready to release, remove the "beta"
VERSION = '2.0.0'


# ------------------------------------------------------------------------------

def parse_args():
    parser = ArgumentParser(description='Run LiDAR Dataprep')
    parser.add_argument('workflow_config_filepath', type=str,
                        help="Filepath of a workflow configuration INI file")
    parser.add_argument('-r', '--skip_review', action='store_true',
                        help="Add this flag to skip user review of "
                             "interpreted workflow")
    parser.add_argument('-o', '--skip_ask_overwrite', action='store_true',
                        help="Add this flag to skip user-authorized overwrite")
    parser.add_argument('-s', '--source_folder', type=str, default=None,
                        help="Absolute path of folder of input "
                             "pointcloud LAS/LAZ files")
    parser.add_argument('-w', '--working_folder', type=str, default=None,
                        help="Absolute path of folder in which to output "
                             "intermediate and final data products and reports")
    return parser.parse_args()


def dataprep(workflow_config_filepath, skip_review=False,
             skip_ask_overwrite=False, source_arg=None, working_arg=None):
    """

    Parameters
    ----------
    workflow_config_filepath : str
        Path to the workflow configuration INI file
    skip_review : bool
        Flag to skip user review of configured steps
    skip_ask_overwrite : bool
        Flag to skip user being asked to overwrite output
    source_arg : str or None
        Path to source data; overrides value in INI if not None
    working_arg : str or None
        Path to working folder; overrides value in INI if not None

    Returns
    -------
    None

    """
    # Report version number and info on the project
    print(f'\nLiDAR DataPrep (version {VERSION}) - UVM SAL')

    # Add the codebase's root to the Python path
    codebase_root = Path(__file__).parent
    path.append(str(codebase_root))

    # Retrieve OS-specific run environment
    run_env = RetrieveRunEnv()
    print(run_env)

    # Parse the workflow
    (cli_dict,
     application_list,
     dir_list,
     working_folder,
     source_folder,
     retain_all,
     hang_check_interval) = \
        workflow_parser(workflow_config_filepath,
                        run_env,
                        source_arg,
                        working_arg)

    source_path = Path(source_folder)
    working_path = Path(working_folder)

    # Validate that working folder is not in source folder
    if source_folder in working_folder:
        raise Exception('The Working folder must not be in the Source folder.')

    # Validate that source folder is not in working folder
    if working_folder in source_folder:
        raise Exception('The Source folder must not be in the Working folder.')

    # Validate/create folders
    if not source_path.is_dir():
        raise Exception(f'The source folder {source_folder} does not exist.')
    if working_path.drive.lower() == 'c:':
        c_drive_protect = \
            input('\nThe working folder is on the C: drive - are you '
                  'sure you want to work there? y/[n] > ')
        if c_drive_protect != 'y':
            raise Exception('Aborting run to protect C: drive.')
    if working_path.is_dir():
        if skip_ask_overwrite:
            raise Exception(f'Working folder {working_folder} exists; '
                            f'delete and retry')
        else:
            delete_working = \
                input(f'\nThe working folder {working_folder} already '
                      f'exists - delete its contents and start over? y/[n] > ')
            if delete_working == 'y':
                try:
                    rmtree(working_folder)
                    sleep(0.1)  # pause to allow for release of lock
                except OSError as e:
                    print(f'Error: {e.filename} - {e.strerror}.')
                    raise Exception(f'Could not delete working '
                                    f'folder {working_folder}')
    try:
        for folder in dir_list:
            Path(folder).mkdir(parents=True)
            print('Created folder: ', folder)
    except:
        raise Exception(f'Could not create folders in {working_folder}')

    # Validate applications
    for application in application_list:
        if not Path(application).is_file():
            raise Exception(f'The application {application} does not exist.')

    # Validate working folder path length for FME
    if 'fme' in ' '.join(application_list).lower():
        len_working_folder = len(working_folder)
        if len_working_folder > 80:
            print(f'\nWARNING: Working folder path length of '
                  f'{len_working_folder} characters exceeds '
                  f'the maximum FME could traditionally handle (80).')

    # Set up folder deletion
    folders_to_delete = []
    for step, value in cli_dict.items():
        output_dir = value['output_dir']
        retain_result = value['retain_result']
        delivery_folder = str(working_path / 'delivery')
        if output_dir and (not retain_result) \
                and (output_dir != delivery_folder):
            folders_to_delete.append(value['output_dir'])
    if folders_to_delete:
        delete_after_step = {}
        step_deletions = {}
        for step, value_dict in cli_dict.items():
            step_deletions[step] = []
            for key, value in value_dict.items():
                if isinstance(value, str):
                    for folder in folders_to_delete:
                        if folder in value:
                            delete_after_step[folder] = step
        for key, value in delete_after_step.items():
            current_list = step_deletions[value]
            current_list.append(key)
            step_deletions[value] = current_list

    # Preview the workflow
    print('\nDataPrep workflow:\n')
    for key, value in cli_dict.items():
        tool = value['tool']
        pipeline = value['pipeline'] if value['pipeline'] else ''
        if tool not in []:
            print_cli_string = value['cli_string'].replace('\n', ' ') + '\n'
        else:
            print_cli_string = ''
        print(key,
              value['module'], tool, pipeline, '\n',
              print_cli_string,
              'From:', value['input_dir'], '\n',
              'To:', value['output_dir'], '\n')

    # Run the workflow if requested
    if skip_review:
        go_nogo = 'y'
    else:
        go_nogo = input('\nRun the above pipeline? y/[n] > ')

    if go_nogo == 'y':

        number_steps = len(cli_dict)
        report_df = DataFrame()
        total_runtime_minutes = 0

        # TODO: Replace [Path] items overridden by dataprep.py CLI args
        #  before writing "reprocess" INI
        reprocess_ini_filepath = \
            str(working_path /
                f'workflow_to_reprocess_with_v{VERSION}_code.ini')
        copyfile(workflow_config_filepath, reprocess_ini_filepath)
        workflow_config_file = Path(workflow_config_filepath).name

        for step_number, step_data in cli_dict.items():

            application = step_data['application']
            step_command = step_data['cli_string']
            step_module = step_data['module']
            step_submodule = step_data['tool']
            step_pipeline = \
                step_data['pipeline'] if step_data['pipeline'] else ''
            step_inputdir = step_data['input_dir']
            step_outputdir = step_data['output_dir']
            print(f'\nRunning step {step_number} of {number_steps} '
                  f'({step_module} - {step_submodule} {step_pipeline}): \n')

            process_out, process_error, process_time_minutes = \
                run_subprocess(application,
                               step_command,
                               step_submodule,
                               step_outputdir,
                               step_inputdir,
                               hang_check_interval,
                               working_folder)
            total_runtime_minutes += process_time_minutes

            step_s = Series(data={
                'Application': application,
                'Command': step_command.replace('\n', ' '),
                'ProcessTime[min]': process_time_minutes,
            },
                name=str(step_number))

            report_df = report_df.append(step_s)
            report_df.index.name = 'Step'
            report_df.to_csv(working_path /
                             f'{Path(workflow_config_file).stem}_report.csv')

            with open(
                    working_path /
                    f'{Path(workflow_config_file).stem}_report.log',
                    'a'
            ) as f:
                f.write(f'\nRunning step {step_number} of {number_steps} '
                        f'({step_module} - {step_submodule} {step_pipeline}): '
                        f'\n\n{step_command}')
                if process_out:
                    f.write(f'\n\nCommunication from the step {step_number} '
                            f'process on the stdout pipe: \n\n{process_out}')
                if process_error:
                    f.write(f'\n\nCommunication from the step {step_number} '
                            f'process on the stderr pipe: \n\n{process_error}')
                f.write(f'\nStep {step_number} complete in '
                        f'{around(process_time_minutes, 1)} minutes.\n')

            print(f'\nStep {step_number} complete in '
                  f'{around(process_time_minutes, 1)} minutes.')

            if not retain_all:
                step_deletions_list = step_deletions[step_number]
                if step_deletions_list:
                    for folder_to_delete in step_deletions_list:
                        try:
                            rmtree(folder_to_delete)
                            sleep(0.1)  # allow for release of lock
                            print('\nFolder deleted: ', folder_to_delete)
                            with open(
                                    working_path /
                                    f'{Path(workflow_config_file).stem}'
                                    f'_report.log',
                                    'a'
                            ) as f:
                                f.write(f'\nFolder deleted: '
                                        f'{folder_to_delete}\n')
                        except OSError:
                            print(f'\nWARNING: Folder {folder_to_delete} could '
                                  'not be deleted')
                            with open(
                                    working_path /
                                    f'{Path(workflow_config_file).stem}'
                                    f'_report.log',
                                    'a'
                            ) as f:
                                f.write(f'\nWARNING: Folder {folder_to_delete} '
                                        f'could not be deleted\n')

        with open(
                working_path /
                f'{Path(workflow_config_file).stem}_report.log',
                'a'
        ) as f:
            f.write(f'\nTotal processing time: '
                    f'{around(total_runtime_minutes, 1)} minutes')

        print(f'\nTotal processing time: '
              f'{around(total_runtime_minutes, 1)} minutes')


if __name__ == '__main__':

    args = parse_args()
    dataprep(
        args.workflow_config_filepath,
        args.skip_review,
        args.skip_ask_overwrite,
        args.source_folder,
        args.working_folder
    )
