from io import StringIO

from dataprep import dataprep
from pytest_config import pytest_config_obj
from test.utilities.utilities import configure_paths, delete_folder, \
    assert_dataprep_folder_names_and_file_suffixes

posix = pytest_config_obj.run_env_obj.posix

# Test parameters
input_path_folders = ['pointclouds', 'isolated_tiles_delaware']
output_path_folders = ['test_dataprep_iso']
ini_file = 'workflow_test_isolated_tiles.ini'

# Test processing
test_dict = {
    'working': {
        'file_count': 3,
        'suffixes': ['.csv', '.ini', '.log']
    },
    'step_1': {
        'file_count': 4,
        'suffixes': ['.laz']
    },
    'step_2': {
        'file_count': 4,
        'suffixes': ['.dbf', '.prj', '.shp', '.shx']
    },
    'step_3': {
        'file_count': 9,
        'suffixes': ['.csv', '.dbf', '.prj', '.shp', '.shx', '.txt']
    },
    'step_4': {
        'file_count': 8,
        'suffixes': ['.tfw', '.tif']
    },
    'step_6': {
        'file_count': 8,
        'suffixes': ['.tfw', '.tif']
    },
    'step_8': {
        'file_count': 8,
        'suffixes': ['.tfw', '.tif']
    },
    'step_10': {
        'file_count': 8,
        'suffixes': ['.tfw', '.tif']
    },
    'step_12': {
        'file_count': 4,
        'suffixes': ['.laz']
    },
    'step_13': {
        'file_count': 4,
        'suffixes': ['.laz']
    },
    'step_14': {
        'file_count': 4,
        'suffixes': ['.laz']
    },
    'step_15': {
        'file_count': 8,
        'suffixes': ['.tfw', '.tif']
    },
    'step_17': {
        'file_count': 4,
        'suffixes': ['.laz']
    },
    'step_18': {
        'file_count': 9,
        'suffixes': ['.csv', '.dbf', '.prj', '.shp', '.shx', '.txt']
    },
    'step_19': {
        'file_count': 8,
        'suffixes': ['.tfw', '.tif']
    },
    'step_21': {
        'file_count': 8,
        'suffixes': ['.tfw', '.tif']
    },
    'step_23': {
        'file_count': 4,
        'suffixes': ['.laz']
    },
    'step_24': {
        'file_count': 9,
        'suffixes': ['.csv', '.dbf', '.prj', '.shp', '.shx', '.txt']
    },
    'delivery': {
        'file_count': 0,
        'suffixes': [],
        'folder_count': 7,
        'subfolders': {
            'step_4': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'step_6': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'step_8': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'step_10': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'step_15': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'step_19': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'step_21': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
        }
    },
}
ini_file_path = str(pytest_config_obj.test_data_path / ini_file)


def test_dataprep_isolated_tiles(monkeypatch, test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    delete_folder(output_path)

    if output_path.drive.lower() == 'c:':
        user_input_to_allow_working_folder_on_c_drive = StringIO('y\n')
        monkeypatch.setattr('sys.stdin',
                            user_input_to_allow_working_folder_on_c_drive)

    dataprep(
        ini_file_path,
        skip_review=True,
        skip_ask_overwrite=True,
        source_arg=input_path,
        working_arg=output_path
    )
    assert_dataprep_folder_names_and_file_suffixes(output_path, test_dict)
