from pathlib import Path
from shutil import rmtree

from pytest_config import pytest_config_obj
from src.utilities.run_environment import RetrieveRunEnv

# TODO: When the FME text encoding changes from that of the FME workbench (as it
#  does always on Linux and sometimes on various Windows deployments) then FME
#  adds a .cpg file when it creates shapefiles. This behavior is currently
#  not tested for because .cpg files are currently ignored by the automated
#  tests.


def assert_dataprep_folder_names_and_file_suffixes(folder_path, file_dict):

    print(f'Testing working root folder')

    root_filepaths = sorted(folder_path.glob('*.*'))
    assert len(root_filepaths) == file_dict['working']['file_count']

    root_suffixes = \
        sorted([filepath.suffix for filepath in root_filepaths])
    assert root_suffixes == file_dict['working']['suffixes']

    print(f'Testing folders')

    folders = \
        sorted(set(folder_path.glob('*')).difference(
            set(root_filepaths)))
    step_folder_names = list(file_dict.keys())
    step_folder_names.remove('working')
    assert len(folders) == len(step_folder_names)

    for step_name in step_folder_names:
        print(f'Testing {step_name}')

        step_path = folder_path / step_name
        step_filepaths_raw = sorted(step_path.glob('*.*'))
        step_suffixes_raw = \
            sorted(set([filepath.suffix for filepath in step_filepaths_raw]))
        step_filepaths, step_suffixes = remove_extension(step_filepaths_raw,
                                                         step_suffixes_raw)

        assert len(step_filepaths) == file_dict[step_name]['file_count']
        assert step_suffixes == file_dict[step_name]['suffixes']

        step_subfolders = \
            sorted(set(step_path.glob('*')).difference(
                set(step_filepaths_raw)))

        if step_name is not 'delivery':
            assert len(step_subfolders) == 0

        if step_name is 'delivery' and step_subfolders \
                or 'subfolders' in file_dict[step_name].keys():
            print('Testing step sub-folders')

            assert len(step_subfolders) == \
                   file_dict[step_name]['folder_count']

            step_subfolder_names = \
                list(file_dict[step_name]['subfolders'].keys())
            for step_subfolder_name in step_subfolder_names:
                print(f'Testing subfolder {step_subfolder_name}')

                step_subfolder_path = step_path / step_subfolder_name
                step_subfolder_path_filepaths_raw = \
                    sorted(step_subfolder_path.glob('*.*'))
                step_subfolder_suffixes_raw = \
                    sorted(set([filepath.suffix for filepath
                                in step_subfolder_path_filepaths_raw]))
                (step_subfolder_path_filepaths,
                 step_subfolder_suffixes) = \
                    remove_extension(step_subfolder_path_filepaths_raw,
                                     step_subfolder_suffixes_raw)

                assert len(step_subfolder_path_filepaths) \
                       == file_dict[step_name][
                           'subfolders'
                       ][step_subfolder_name]['file_count']
                assert step_subfolder_suffixes \
                       == file_dict[step_name][
                           'subfolders'
                       ][step_subfolder_name]['suffixes']


def configure_paths(input_folders, output_folders, output_base):
    run_env_obj = RetrieveRunEnv()
    remote_path = Path(run_env_obj.remote_test_data)
    if pytest_config_obj.local_test_data:
        input_path = \
            pytest_config_obj.local_test_data_path.joinpath(*input_folders)
    else:
        input_path = remote_path.joinpath(*input_folders)
    output_path = output_base.joinpath(*output_folders)
    return input_path, output_path


def delete_folder(folder_path):
    if folder_path.is_dir():
        rmtree(folder_path)


def remove_extension(filepaths, extensions, extension_to_drop='.cpg'):
    filepaths_cleaned = []
    for filepath in filepaths:
        if filepath.suffix != extension_to_drop:
            filepaths_cleaned.extend([filepath])

    extensions_cleaned = []
    for extension in extensions:
        if extension != extension_to_drop:
            extensions_cleaned.extend([extension])

    return filepaths_cleaned, extensions_cleaned


def reset_folder(folder_path):
    if folder_path.is_dir():
        rmtree(folder_path)
    folder_path.mkdir(parents=True)
