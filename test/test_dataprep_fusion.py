from io import StringIO

from pytest import mark, raises

from dataprep import dataprep
from pytest_config import pytest_config_obj
from test.utilities.utilities import configure_paths, delete_folder, \
    assert_dataprep_folder_names_and_file_suffixes

posix = pytest_config_obj.run_env_obj.posix

# Test parameters
input_path_folders = ['pointclouds', 'iso_final_pointclouds']
output_path_folders = ['test_dataprep_fusion']
ini_file = 'workflow_test_fusion.ini'

# Test processing
test_dict = {
    'working': {
        'file_count': 3,
        'suffixes': ['.csv', '.ini', '.log']
    },
    'step_1': {
        'file_count': 16,
        'suffixes': ['.csv', '.txt']
    },
}
ini_file_path = str(pytest_config_obj.test_data_path / ini_file)


@mark.skipif(posix, reason="Fusion only runs on Windows, for now")
def test_dataprep_fusion_on_windows(monkeypatch, test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    delete_folder(output_path)

    if output_path.drive.lower() == 'c:':
        user_input_to_allow_working_folder_on_c_drive = StringIO('y\n')
        monkeypatch.setattr('sys.stdin',
                            user_input_to_allow_working_folder_on_c_drive)

    dataprep(
        ini_file_path,
        skip_review=True,
        skip_ask_overwrite=True,
        source_arg=input_path,
        working_arg=output_path
    )
    assert_dataprep_folder_names_and_file_suffixes(output_path, test_dict)


@mark.skipif(not posix, reason="Test needed for Linux only")
def test_dataprep_fusion_on_linux(test_output):
    expected_message_element = 'MultiprocessFusion submodule only ' \
                               'runs on Windows'

    input_path, output_path = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    delete_folder(output_path)

    with raises(Exception, match=expected_message_element):
        dataprep(
            ini_file_path,
            skip_review=True,
            skip_ask_overwrite=True,
            source_arg=input_path,
            working_arg=output_path
        )
