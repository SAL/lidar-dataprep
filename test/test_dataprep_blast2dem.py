from io import StringIO

from pytest import mark, raises

from dataprep import dataprep
from pytest_config import pytest_config_obj
from test.utilities.utilities import configure_paths, delete_folder, \
    assert_dataprep_folder_names_and_file_suffixes

posix = pytest_config_obj.run_env_obj.posix

# Test parameters
input_path_folders = ['pointclouds', 'burlington_subset']
output_path_folders = ['test_dataprep_blast2dem']
ini_file = 'workflow_test_blast2dem.ini'
test_dict = {
    'working': {
        'file_count': 3,
        'suffixes': ['.csv', '.ini', '.log']
    },
    'step_1': {
        'file_count': 12,
        'suffixes': ['.kml', '.tfw', '.tif']
    },
}

# Test processing
blast2dem_ini_file = str(pytest_config_obj.test_data_path / ini_file)


@mark.skipif(posix, reason="Blast2DEM only runs on Windows, for now")
def test_dataprep_blast2dem(monkeypatch, test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    delete_folder(output_path)
    if output_path.drive.lower() == 'c:':
        user_input_to_allow_working_folder_on_c_drive = StringIO('y\n')
        monkeypatch.setattr('sys.stdin',
                            user_input_to_allow_working_folder_on_c_drive)
    dataprep(
        blast2dem_ini_file,
        skip_review=True,
        skip_ask_overwrite=True,
        source_arg=input_path,
        working_arg=output_path
    )

    assert_dataprep_folder_names_and_file_suffixes(output_path, test_dict)


@mark.skipif(not posix, reason="Test needed for Linux only")
def test_dataprep_blast2dem_on_linux(test_output):
    expected_message_element = 'blast2dem cannot be run as 64bit and ' \
                               'so cannot currently be run on Linux'

    input_path, output_path = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    delete_folder(output_path)

    with raises(Exception, match=expected_message_element):
        dataprep(
            blast2dem_ini_file,
            skip_review=True,
            skip_ask_overwrite=True,
            source_arg=input_path,
            working_arg=output_path
        )
