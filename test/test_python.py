from pytest import raises

from pytest_config import pytest_config_obj
from src.python import python_command


# Test processing
codebase_root = pytest_config_obj.codebase_root_path
if pytest_config_obj.run_env_obj.posix:
    mocked_input_folder = '/vagrant/input'
    mocked_output_folder = '/vagrant/output'
else:
    mocked_input_folder = r'c:\input'
    mocked_output_folder = r'c:\output'


def test_python_command_with_rename_raster():
    command = python_command('RenameRaster',
                             mocked_input_folder, mocked_output_folder,
                             pytest_config_obj.run_env_obj, None,
                             'test_in.txt', 'test_out.dat',
                             None, None, None, None, None)
    submodule_path = codebase_root / 'src' / 'workspaces' / 'python' / \
                     'rename_raster.py'
    expected_command = \
        rf'{submodule_path} {mocked_input_folder} test_in.txt test_out.dat'

    assert command == expected_command


def test_python_command_with_bad_tool_name():
    wrong_tool_name = 'Rename'
    expected_message_element = 'Rename is not a recognized Python tool'

    with raises(Exception, match=expected_message_element):
        command = python_command(wrong_tool_name,
                                 r'c:\input', r'c:\output',
                                 pytest_config_obj.run_env_obj, None,
                                 'test_in.txt', 'test_out.dat',
                                 None, None, None, None, None)
