from io import StringIO

from dataprep import dataprep
from pytest_config import pytest_config_obj
from test.utilities.utilities import configure_paths, delete_folder, \
    assert_dataprep_folder_names_and_file_suffixes

posix = pytest_config_obj.run_env_obj.posix

# Test parameters
input_path_folders = ['pointclouds', 'laz_with_unreadable_CRS']
output_path_folders = ['test_dataprep_Arc_unreadable_CRS_ArcPro3']
ini_file = 'workflow_test_Arc_reading_CRS_ArcPro3.ini'

# Test processing
test_dict = {
    'working': {
        'file_count': 3,
        'suffixes': ['.csv', '.ini', '.log']
    },
    'step_1': {
        'file_count': 4,
        'suffixes': ['.las']
    },
    'step_2': {
        'file_count': 12,
        'suffixes': ['.kml', '.tfw', '.tif']
    },
    'delivery': {
        'file_count': 0,
        'suffixes': [],
        'folder_count': 1,
        'subfolders': {
            'step_2': {
                'file_count': 20,
                'suffixes': ['.kml', '.tfw', '.tif', '.xml']
            },
        }
    },
}
ini_file_path = str(pytest_config_obj.test_data_path / ini_file)


def test_dataprep_Arc_unreadable_CRS(monkeypatch, test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    delete_folder(output_path)

    if output_path.drive.lower() == 'c:':
        user_input_to_allow_working_folder_on_c_drive = StringIO('y\n')
        monkeypatch.setattr('sys.stdin',
                            user_input_to_allow_working_folder_on_c_drive)

    dataprep(
        ini_file_path,
        skip_review=True,
        skip_ask_overwrite=True,
        source_arg=input_path,
        working_arg=output_path
    )
    assert_dataprep_folder_names_and_file_suffixes(output_path, test_dict)
