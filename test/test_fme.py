from pytest import raises

from pytest_config import pytest_config_obj
from src.fme import fme_command


def test_fme_command_for_mosaic_tools():
    if pytest_config_obj.run_env_obj.posix:
        mocked_input_folder = '/vagrant/input'
        mocked_output_folder = '/vagrant/output'
        expected_command = '\n'.join([
            f'{str(pytest_config_obj.codebase_root_path)}'
            f'/src/workspaces/fme/dsm_mosaic_compresslzw.fmw',
            '--SourceDataset_GEOTIFF',
            '""" /vagrant/input/*.tif /vagrant/input/*.tiff '
            '/vagrant/input/*.itiff /vagrant/input/*.ovr """',
            '--DestDataset_GEOTIFF',
            '/vagrant/output',
            'LOG_STANDARDOUT',
            'YES'
            ])
    else:
        mocked_input_folder = r'c:\input'
        mocked_output_folder = r'c:\output'
        expected_command = '\n'.join([
            rf'{str(pytest_config_obj.codebase_root_path)}'
            rf'\src\workspaces\fme\dsm_mosaic_compresslzw.fmw',
            '--SourceDataset_GEOTIFF',
            r'""" c:\input\*.tif c:\input\*.tiff '
            r'c:\input\*.itiff c:\input\*.ovr """',
            '--DestDataset_GEOTIFF',
            r'c:\output',
            'LOG_STANDARDOUT',
            'YES'
            ])

    command = fme_command('Mosaic_DSM',
                          mocked_input_folder, mocked_output_folder,
                          None, None)
    command_one_line = command.replace('\n', ' ')
    expected_command_one_line = expected_command.replace('\n', ' ')

    assert command == expected_command
    assert command_one_line == expected_command_one_line


def test_fme_command_with_bad_tool_name():
    wrong_tool_name = 'Mosaic'
    expected_message_element = 'Mosaic is not a recognized FME tool'

    with raises(Exception, match=expected_message_element):
        command = fme_command(wrong_tool_name,
                              r'c:\input', r'c:\output',
                              None, None)
