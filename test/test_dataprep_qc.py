from io import StringIO

from pytest import raises, mark

from dataprep import dataprep
from pytest_config import pytest_config_obj
from test.utilities.utilities import configure_paths, delete_folder, \
    assert_dataprep_folder_names_and_file_suffixes

posix = pytest_config_obj.run_env_obj.posix

# Test parameters
input_path_folders = ['pointclouds', 'burlington_subset']
output_path_folders = ['test_dataprep_qc']
ini_file = 'workflow_test_qc_source_pointclouds.ini'
test_dict = {
    'working': {
        'file_count': 3,
        'suffixes': ['.csv', '.ini', '.log']
    },
    'step_1': {
        'file_count': 4,
        'suffixes': ['.dbf', '.prj', '.shp', '.shx']
    },
    'step_2': {
        'file_count': 9,
        'suffixes': ['.csv', '.dbf', '.prj', '.shp', '.shx', '.txt']
    },
}

# Test processing
qc_ini_file = str(pytest_config_obj.test_data_path / ini_file)


@mark.skipif(posix, reason="Only needed on Windows")
def test_dataprep_c_drive_protection(monkeypatch, test_output):
    expected_message_element = 'Aborting run to protect C: drive'

    input_path, _ = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    user_input_to_not_have_working_folder_on_c_drive = StringIO('n\n')
    monkeypatch.setattr('sys.stdin',
                        user_input_to_not_have_working_folder_on_c_drive)

    with raises(Exception, match=expected_message_element):
        dataprep(
            qc_ini_file,
            skip_review=True,
            skip_ask_overwrite=True,
            source_arg=input_path,
            working_arg="C:/Temp/test"
        )


def test_dataprep_qc(monkeypatch, test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    delete_folder(output_path)
    if output_path.drive.lower() == 'c:':
        user_input_to_allow_working_folder_on_c_drive = StringIO('y\n')
        monkeypatch.setattr('sys.stdin',
                            user_input_to_allow_working_folder_on_c_drive)
    dataprep(
        qc_ini_file,
        skip_review=True,
        skip_ask_overwrite=True,
        source_arg=input_path,
        working_arg=output_path
    )

    assert_dataprep_folder_names_and_file_suffixes(output_path, test_dict)
