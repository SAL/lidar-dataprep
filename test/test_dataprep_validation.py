from pytest import raises

from dataprep import dataprep


def test_dataprep_with_no_input_args():
    expected_message_element = 'missing 1 required positional ' \
                               'argument: \'workflow_config_filepath\''

    with raises(Exception, match=expected_message_element):
        dataprep()
