from pathlib import Path

from pytest import raises

from pytest_config import pytest_config_obj
from src.lastools import lastool_command


if pytest_config_obj.run_env_obj.posix:
    mocked_input_folder = '/vagrant/input'
    mocked_output_folder = '/vagrant/output'
else:
    mocked_input_folder = r'c:\input'
    mocked_output_folder = r'c:\output'


def test_lastool_command_with_las2las():
    command = lastool_command('las2las', 'laz',
                              mocked_input_folder, mocked_output_folder,
                              [], 'True')
    mocked_input_data = Path(mocked_input_folder) / '*.laz'
    if pytest_config_obj.run_env_obj.posix:
        expected_command = f'-i "{mocked_input_data}" ' \
                           f'-olaz -odir "{mocked_output_folder}"'
    else:
        expected_command = rf'-i "{mocked_input_data}" ' \
                           rf'-cpu64 ' \
                           rf'-olaz -odir "{mocked_output_folder}"'

    assert expected_command == command


def test_lastool_command_with_bad_tool_name():
    wrong_tool_name = 'las2laz'
    expected_message_element = 'las2laz is not a recognized LAStool'

    with raises(Exception, match=expected_message_element):
        command = lastool_command(wrong_tool_name, 'laz',
                                  mocked_input_folder, mocked_output_folder,
                                  [], 'True')


def test_lastool_command_with_bad_input_filter():
    bad_input_filter = 'tif'
    expected_message_element = 'tif is not a recognized input filter'

    with raises(Exception, match=expected_message_element):
        command = lastool_command('las2las', bad_input_filter,
                                  mocked_input_folder, mocked_output_folder,
                                  [], 'True')


def test_lastool_command_with_no_input_filter():
    no_input_filter = None
    command = lastool_command('las2las', no_input_filter,
                              mocked_input_folder, mocked_output_folder,
                              [], 'True')
    mocked_input_data = Path(mocked_input_folder) / '*.*'
    if pytest_config_obj.run_env_obj.posix:
        expected_command = f'-i "{mocked_input_data}" ' \
                           f'-olaz -odir "{mocked_output_folder}"'
    else:
        expected_command = rf'-i "{mocked_input_data}" ' \
                           rf'-cpu64 ' \
                           rf'-olaz -odir "{mocked_output_folder}"'

    assert expected_command == command
