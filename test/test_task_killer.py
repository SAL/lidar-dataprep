from subprocess import Popen, PIPE
from time import sleep

from pytest import mark

from pytest_config import pytest_config_obj
from src.utilities.task_killer import task_killer
from test.utilities.utilities import configure_paths, reset_folder

# Test parameters
input_path_folders = ['pointclouds', 'burlington']

# Test processing
posix = pytest_config_obj.run_env_obj.posix


@mark.skipif(posix, reason="Only setup for Windows so far")
def test_task_killer_with_las2las_fully_in_process(test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, ['test_task_killer_1'], test_output)
    reset_folder(output_path)
    application = rf'{pytest_config_obj.run_env_obj.lastools_bin}\las2las.exe'
    step_command = rf'-i "{input_path}\*.laz" -cores 7 ' \
                   rf'-drop_classification 7 18 ' \
                   rf'-cpu64 -olaz -odir "{output_path}" '
    process = Popen(
        [application, [step_command]],
        cwd=output_path,
        stdout=PIPE,
        stderr=PIPE,
        universal_newlines=True,
    )
    sleep(0.1)
    kill_results = task_killer('las2las')
    process_out, process_error = process.communicate()

    assert 'las2las.exe' in kill_results
    assert 'las2las64.exe' in kill_results
    assert process_out == ''
    assert process_error == ''


@mark.skip(reason="Test needs refinement")
# @mark.skipif(posix, reason="Only setup for Windows so far")
def test_task_killer_with_las2las_partially_in_process(test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, ['test_task_killer_2'], test_output)
    reset_folder(output_path)
    application = rf'{pytest_config_obj.run_env_obj.lastools_bin}\las2las.exe'
    step_command = rf'-i "{input_path}\*.laz" -cores 7 ' \
                   rf'-drop_classification 7 18 ' \
                   rf'-cpu64 -olaz -odir "{output_path}" '
    process = Popen(
        [application, [step_command]],
        cwd=output_path,
        stdout=PIPE,
        stderr=PIPE,
        universal_newlines=True,
    )
    sleep(1)
    kill_results = task_killer('las2las')
    process_out, process_error = process.communicate()

    assert 'Tried, but could not kill task' in kill_results
    assert process_out == ''
    assert process_error == ''


@mark.skipif(posix, reason="Only setup for Windows so far")
def test_task_killer_with_las2las_completed(test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, ['test_task_killer_3'], test_output)
    reset_folder(output_path)
    application = rf'{pytest_config_obj.run_env_obj.lastools_bin}\las2las.exe'
    step_command = rf'-i "{input_path}\*.laz" -cores 7 ' \
                   rf'-drop_classification 7 18 ' \
                   rf'-cpu64 -olaz -odir "{output_path}" '
    process = Popen(
        [application, [step_command]],
        cwd=output_path,
        stdout=PIPE,
        stderr=PIPE,
        universal_newlines=True,
    )
    process_out, process_error = process.communicate()
    sleep(0.1)
    kill_results = task_killer('las2las')

    assert kill_results == ''
    assert process_out == ''
    assert process_error == ''
