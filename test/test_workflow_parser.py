from pytest import raises

from pytest_config import pytest_config_obj
from src.workflow_parser import workflow_parser


def test_workflow_parser_with_no_input():
    expected_message_element = 'workflow_test_lidrXXX.ini does not exist'

    with raises(Exception, match=expected_message_element):
        cli_dict, application_list, dir_list, \
            working_folder, source_folder, \
            retain_all, hang_check_interval = \
            workflow_parser(
                str(pytest_config_obj.test_data_path
                    / 'workflow_test_lidrXXX.ini'),
                None,
                False,
                False
            )


def test_workflow_parser_with_wrong_format_input():
    expected_message_element = 'workflow_test_lidr.txt is not an INI file'

    with raises(Exception, match=expected_message_element):
        cli_dict, application_list, dir_list, \
            working_folder, source_folder, \
            retain_all, hang_check_interval = \
            workflow_parser(
                str(pytest_config_obj.test_data_path
                    / 'workflow_test_lidr.txt'),
                None,
                False,
                False
            )
