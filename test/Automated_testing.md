## Automated Testing

### The files and folders in the codebase involved in automated testing
```
lidar-dataprep
├── htmlcov (An interactive report on test coverage is written to this folder; 
│             open the "index.html" file in a browser to view)
├── test
│   ├── data
│   │   └── (INI files used by the tests)
│   ├── external_data (Tests look for input data here if 
│   │   │               "local_test_data = True" in pytest_config.py)
│   │   └── pointclouds
│   │       └── (subfolders of pointcloud data)
│   ├── utilities
│   │   └── utilities.py (Utility functions related to automated testing)
│   └── (Multiple pytest tests, in files named as "test_<name_of_test>.py" )
├── .coverage (SQLite database that tracks test coverage)
├── .coveragerc (List of files to not inspect for test coverage)
├── conftest.py (Custom pytest "fixtures")
├── pytest.ini (Pytest command-line configuration options)
└── pytest_config.py (User test configuration setting: local/remote input data)
```

### Main uses for automated testing
- When deploying this codebase to a new machine, or when a machine running 
this codebase is otherwise updated, to check that this codebase is 
installed, configured, and running correctly.
- To test that a new version of dependent software (Arc, FME, LAStools, 
Fusion, LidR, PDAL, Wine, Conda) is compatible with the current version of this 
software. 
- When developing a new feature, fixing a bug in this codebase, or evaluating 
a new version of dependent software - to check that all functionality 
in the codebase (not just the section being changed) still works as before 
the changes.

### Running the test suite
In all deployments, use the command `pytest` to run all tests or 
`pytest test/<test_python_file>` to run the subset of tests found in one 
Python test file.

- Windows, Linux VM
  - In a terminal:
      - `cd` to the root of the local codebase folder
      - `conda activate dataprep`
      - `pytest`
  - In PyCharm:
    - "Run" an individual test (PyCharm will detect that this is a pytest, 
    and run it accordingly in its test runner)
    - OR
    - "Run" the `test` folder to run all tests in the test runner
- VACC
  - cd to `batch_working`
  - `sbatch ../code/lidar-dataprep/VACC_running/slurm_pytest_dataprep.sh`
  - Test results will be written to the OUT log

### Test results
- Results reported to stdout (or in the Test Runner window, if PyCharm):
    - Pass/Failure results of each test, and details on failures, if any
    - Coverage summary
- Results written to a html report, with index page at: `htmlcov/index.html`
    - Detailed and interactive coverage report 
- If wish to inspect the test outputs, consult the logging to see where 
the temp output folder is located. The outputs are auto-maintained by the pytest 
program; typically it keeps the last three sets of test data and deletes runs
older than that.
- WARNING: It is possible to specify where the pytest outputs should go 
(and this is required on the VACC) but note that since pytest auto-manages 
whichever folder is specified, everything in that folder will be deleted by 
pytest without warning - so choose this folder carefully!
