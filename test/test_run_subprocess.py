from pathlib import Path

from pytest_config import pytest_config_obj
from src.run_subprocess import run_subprocess
from test.utilities.utilities import configure_paths, reset_folder

# Test parameters
input_path_folders = ['pointclouds', 'burlington_subset']
output_path_folders = ['test_run_subprocess']


def test_run_subprocess_with_las2las_and_no_process_monitoring(test_output):
    input_path, output_path = configure_paths(input_path_folders,
                                              output_path_folders,
                                              test_output)
    reset_folder(output_path)
    exe = Path(pytest_config_obj.run_env_obj.lastools_bin) / 'las2las.exe'
    input_data = input_path / '*.laz'
    if pytest_config_obj.run_env_obj.posix:
        las2las_command = f'-i "{input_data}" -cores 4 ' \
                          f'-drop_classification 7 18 ' \
                          f'-olaz -odir "{output_path}"'
    else:
        las2las_command = rf'-i "{input_data}" -cores 4 ' \
                          rf'-drop_classification 7 18 ' \
                          rf'-cpu64 -olaz -odir "{output_path}"'

    process_out, process_error, process_time_minutes = run_subprocess(
        rf'{exe}',
        las2las_command,
        None,  # Not needed, since not running process monitor
        output_path,
        input_path,
        0,  # To turn off process monitor
        output_path  # Won't be used, since no process monitoring
    )

    assert process_out == ''
    assert process_error == ''
    assert process_time_minutes < 1.0
