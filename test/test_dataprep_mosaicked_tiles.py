from io import StringIO

from dataprep import dataprep
from pytest_config import pytest_config_obj
from test.utilities.utilities import configure_paths, delete_folder, \
    assert_dataprep_folder_names_and_file_suffixes

posix = pytest_config_obj.run_env_obj.posix

# Test parameters
input_path_folders = ['pointclouds', 'burlington_subset']
output_path_folders = ['test_dataprep_mosaic']
ini_file = 'workflow_test_mosaicked_tiles.ini'

# Test processing
test_dict = {
    'working': {
        'file_count': 3,
        'suffixes': ['.csv', '.ini', '.log']
    },
    'step_1': {
        'file_count': 8,
        'suffixes': ['.lax', '.laz']
    },
    'step_3': {
        'file_count': 4,
        'suffixes': ['.dbf', '.prj', '.shp', '.shx']
    },
    'step_4': {
        'file_count': 32,
        'suffixes': ['.lax', '.laz']
    },
    'step_6': {
        'file_count': 16,
        'suffixes': ['.laz']
    },
    'step_7': {
        'file_count': 16,
        'suffixes': ['.laz']
    },
    'step_8': {
        'file_count': 16,
        'suffixes': ['.laz']
    },
    'step_9': {
        'file_count': 4,
        'suffixes': ['.laz']
    },
    'step_10': {
        'file_count': 48,
        'suffixes': ['.kml', '.tfw', '.tif']
    },
    'step_11': {
        'file_count': 1,
        'suffixes': ['.tif']
    },
    'step_13': {
        'file_count': 48,
        'suffixes': ['.kml', '.tfw', '.tif']
    },
    'step_14': {
        'file_count': 2,
        'suffixes': ['.tif', '.xml']
    },
    'step_17': {
        'file_count': 48,
        'suffixes': ['.kml', '.tfw', '.tif']
    },
    'step_18': {
        'file_count': 2,
        'suffixes': ['.tif', '.xml']
    },
    'step_21': {
        'file_count': 48,
        'suffixes': ['.kml', '.tfw', '.tif']
    },
    'step_22': {
        'file_count': 1,
        'suffixes': ['.tif']
    },
    'step_24': {
        'file_count': 48,
        'suffixes': ['.kml', '.tfw', '.tif']
    },
    'step_25': {
        'file_count': 1,
        'suffixes': ['.tif']
    },
    'delivery': {
        'file_count': 20,
        'suffixes': ['.ovr', '.tif', '.xml'],
        'folder_count': 5,
        'subfolders': {
            'dem__tiled_from_mosaic': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'dsm_first_only_tiled_from_mosaic': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'dsm_spike_free_tiled_from_mosaic': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'dtm__tiled_from_mosaic': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
            'intensity__tiled_from_mosaic': {
                'file_count': 20,
                'suffixes': ['.ovr', '.tfw', '.tif', '.xml']
            },
        }
    },
}
ini_file_path = str(pytest_config_obj.test_data_path / ini_file)


def test_dataprep_mosaicked_tiles(monkeypatch, test_output):
    input_path, output_path = \
        configure_paths(input_path_folders, output_path_folders, test_output)
    delete_folder(output_path)

    if output_path.drive.lower() == 'c:':
        user_input_to_allow_working_folder_on_c_drive = StringIO('y\n')
        monkeypatch.setattr('sys.stdin',
                            user_input_to_allow_working_folder_on_c_drive)

    dataprep(
        ini_file_path,
        skip_review=True,
        skip_ask_overwrite=True,
        source_arg=input_path,
        working_arg=output_path
    )
    assert_dataprep_folder_names_and_file_suffixes(output_path, test_dict)
