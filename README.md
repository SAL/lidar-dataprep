## LiDAR DataPrep

### Overview of this codebase
```
lidar-dataprep
├── scripts (Example scripts for post-processing, analysis, and reporting)
├── setup (Files and procedures for deploying this application)
├── src
│   ├── utilities (Utility functions for definition of runtime-environment  
│   │               and task killing)
│   ├── workspaces
│   │   ├── fme (FME workbenches used for the FME module's submodules)
│   │   ├── pdal (PDAL filter definitions for use by the File Management
│   │   │          MultiprocessPDAL submodule)
│   │   ├── python (Python-language File Management submodules)
│   │   └── r (R-language File Management submodules) 
│   ├── fme.py (The FME module)    
│   ├── lastools.py (The LAStools module)    
│   ├── python.py (The File Management module)    
│   ├── run_subprocess.py (Executes each step as a subprocess at runtime)    
│   └── workflow_parser.py (Parses the INI workflow, validates its workflow 
│                            settings, and sets up the step processing)    
├── test (Files involved in automated testing; see "test/Automated_testing.md" 
│          for a description of this folder's contents)
├── VACC_running (Slurm and batch-Slurm bash scripts)
├── workflow_configuration_templates (Example INI workflows)
├── .gitignore (Defines files/folders and file/folder patterns that are 
│                not to be tracked by version control)
├── dataprep.py (The LiDAR Dataprep command-line-interface tool main script)
├── README.md (This file)
├── Release_notes.md (Changelists for each release)
├── software_paths_template.ini (This file is copied to a git-ignored file  
│                                 "software_paths.ini" and edited for each 
│                                 deployment)
├── Vagrantfile (Configuration file for a RedHat virtual machine that 
│                 this code can run on, a space for developing VACC code and 
│                 debugging workflow running there)
├── Workflow_definition.md (Guide to configuring INI workflows)
└── Workflow_running.md (Guide to running INI workflows)
```

### Set up
1. Set up this program using the appropriate guide(s):
   - [Windows](./setup/Setup_windows.md)
   - [VACC Linux](./setup/Setup_linux_vacc.md)
   - [VM Linux](./setup/Setup_linux_vm.md)
2. Configure install paths and settings:
   - Copy `software_paths_template.ini` to `software_paths.ini` and modify 
   as appropriate for the current deployment
3. Run built-in program self-tests to confirm the program is properly setup:
   - [Automated testing](./test/Automated_testing.md)

### Defining and running a workflow
1. [Defining a workflow](./Workflow_definition.md)
2. [Running a workflow](./Workflow_running.md)

### Template scripts
Miscellaneous scripts that have been used to analyze and post-process data
products produced by this application have been included here (see `scripts` 
folder) for use as start-point templates for similar needs in future projects.

### References
- [Release notes](./Release_notes.md)
  - A list of each released version's new capabilities, improvements, 
  and bug fixes
- [Software dependencies](./setup/Software_dependencies.md)
  - A list of the applications (with versions) this codebase leverages 
    that need to be installed for this codebase to fully function
- [ArcPy Reference](./setup/ArcPy_reference.md)
- [VACC KnowledgeBase](https://vacckb.helpline.w3.uvm.edu/vacc/kb/)
  - [Running jobs on Bluemoon](https://www.uvm.edu/vacc/kb/knowledge-base/write-submit-job-bluemoon/)
  - [Bluemoon specs](https://www.uvm.edu/vacc/kb/knowledge-base/bluemoon/)
  - [Useful commands](https://www.uvm.edu/vacc/kb/knowledge-base/useful-commands/)
- lidR
  - [The lidR package](https://r-lidar.github.io/lidRbook/)
  - [lidR CRAN page](https://cran.r-project.org/package=lidR)
