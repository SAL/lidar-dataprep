from pytest import fixture


@fixture(scope="session")
def test_output(tmp_path_factory):
    return tmp_path_factory.mktemp("pytesting")
