"""Pytest Configuration"""

from pathlib import Path
from types import SimpleNamespace

from src.utilities.run_environment import RetrieveRunEnv


# General testing parameters
local_test_data = False


# Processing

codebase_root_path = Path(__file__).parent

pytest_config_obj = SimpleNamespace(
    codebase_root_path=codebase_root_path,
    local_test_data_path=codebase_root_path / 'test' / 'external_data',
    test_data_path=codebase_root_path / 'test' / 'data',
    run_env_obj=RetrieveRunEnv(),
    local_test_data=local_test_data,
)
