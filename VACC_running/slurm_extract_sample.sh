#!/bin/bash

### Slurm header section ###

# Job name
#SBATCH --job-name=extract_sample

# Partition
# `short`: jobs < 3 hrs; `bluemoon`/`bigmem` < 30 hrs; `week`/`bigmemwk`: < 7 days
#SBATCH --partition=short
# #SBATCH --partition=bluemoon
# #SBATCH --partition=week

# Walltime (hh:mm:ss)
# End job if hits this to not run afoul of partition guideline
#SBATCH --time=3:00:00
# #SBATCH --time=30:00:00
# #SBATCH --time=7-00:00:00

# Nodes
#SBATCH --nodes=1

# Processes/Tasks per Node
#SBATCH --ntasks=1

# Cores per Process/Task
#SBATCH --cpus-per-task=2

# Memory (default is 1G, for entire job, can specify by entire job --mem=64GB or by cpu --mem-per-cpu=2GB)
#SBATCH --mem=8GB

# Email
#SBATCH --mail-type=ALL

### Executable section ###

# Echo each command to the log file
set -x

# Link log file to one with a preferred name
ln -f "slurm-$SLURM_JOB_ID.out" "${SLURM_JOB_NAME}_$SLURM_JOB_ID.out"

# Run code
conda run -n dataprep python ~/code/lidar-dataprep/scripts/extract_sample.py
