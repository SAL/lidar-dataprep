#!/bin/bash

### Slurm header section ###

# Job name
#SBATCH --job-name=lastools

# Partition
# `short`: jobs < 3 hrs; `bluemoon`/`bigmem` < 30 hrs; `week`/`bigmemwk`: < 7 days
#SBATCH --partition=short
# #SBATCH --partition=bluemoon
# #SBATCH --partition=week

# Walltime (hh:mm:ss)
# End job if hits this to not run afoul of partition guideline
#SBATCH --time=3:00:00
# #SBATCH --time=30:00:00
# #SBATCH --time=7-00:00:00

# Nodes
#SBATCH --nodes=1

# Processes/Tasks per Node
#SBATCH --ntasks=1

# Cores per Process/Task
#SBATCH --cpus-per-task=2

# Memory (default is 1G, for entire job, can specify by entire job --mem=64GB or by cpu --mem-per-cpu=2GB)
#SBATCH --mem=16GB

# Email
#SBATCH --mail-type=ALL

# Rename log file to "<myjob>_<jobid>.out" -- Not working this way, so implemented differently, below
# #SBATCH –-output=%x_%j.out

### Executable section ###

# Echo each command to the log file
set -x

# Link log file to one with a preferred name, and in the output folder
# TODO: Add working folder stem to OUT log file name
ln -f "slurm-${SLURM_JOB_ID}.out" "${SLURM_JOB_NAME}_${SLURM_JOB_ID}.out"

# Export environment variables
export LD_LIBRARY_PATH=/gpfs1/arch/x86_64-rhel7/fmelibs/lib64:/gpfs1/arch/x86_64-rhel7/fmelibs/usr/lib64/
export PATH=${PATH}:/gpfs1/arch/x86_64-rhel7/fme-desktop-2021.2.3/opt/fme-desktop-2021:/gpfs1/arch/x86_64-rhel7/fmelibs/usr/bin

# Working directory
#  Needed only if commands that follow (other than the `ln` command) have relative paths
#  Must be on user node (not `scratch`, etc)
cd "${HOME}/code/lidar-dataprep"

# Run single-processed LAStools 64-bit command
/gpfs1/arch/x86_64-rhel7/Arc-Server-10.9/arcgis/server/bin/wine/bin/wine64 \
/users/t/c/tcbarret/LAStools_30Mar2023_use_for_LiDAR_DataPrep_v2.0/bin/lastile64 \
-i "/netfiles/salshare/Tools/PythonScripts/Terry/workflow_processing/Mansfield_2014_laz_peak_buffered_no_indexing_01/step_1_pointclouds_noise_dropped/*.laz" \
-o tile.laz \
-tile_size 500 \
-buffer 50 \
-olaz \
-odir "/netfiles/salshare/Tools/PythonScripts/Terry/workflow_processing/Linux_tiling_01"
