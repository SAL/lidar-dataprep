INI=/users/t/c/tcbarret/code/lidar_dataprep/workflow_configuration_templates/workflows_in_development/workflow_Downloader_Full_01.ini

SOURCE_BASE=/gpfs1/home/t/c/tcbarret/data/FIA/fia_01_retrievals
USGS_18_EASTERN=all_states_20220207_0_to_18_months/eastern_us_20220204
USGS_18_WESTERN=all_states_20220207_0_to_18_months/western_us_20220207
USGS_30_ALL=all_states_20220315_18_to_30_months
USGS_60_WESTERN=western_states_20220316_30_to_60_months
NOAA_30=noaa_retrievals_20220628/NOAA_0_30/STATES
NOAA_60=noaa_retrievals_20220628/NOAA_30_60/STATES

WORKING_BASE=/gpfs2/scratch/tcbarret/dataprep_working

DATAPREP=/users/t/c/tcbarret/code/lidar_dataprep/VACC_running/slurm_dataprep.sh


sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/AK/clips" "${WORKING_BASE}/AK_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/AK/clips" "${WORKING_BASE}/AK_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/AK/clips" "${WORKING_BASE}/AK_60mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/2" "${WORKING_BASE}/AK_30mo_NOAA"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_60}/2" "${WORKING_BASE}/AK_60mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/AL/clips" "${WORKING_BASE}/AL_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/AL/clips" "${WORKING_BASE}/AL_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/1" "${WORKING_BASE}/AL_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/AR/clips" "${WORKING_BASE}/AR_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/AR/clips" "${WORKING_BASE}/AR_30mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/AZ/clips" "${WORKING_BASE}/AZ_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/AZ/clips" "${WORKING_BASE}/AZ_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/AZ/clips" "${WORKING_BASE}/AZ_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/CA/clips" "${WORKING_BASE}/CA_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/CA/clips" "${WORKING_BASE}/CA_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/CA/clips" "${WORKING_BASE}/CA_60mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/6" "${WORKING_BASE}/CA_30mo_NOAA"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_60}/6" "${WORKING_BASE}/CA_60mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/CO/clips" "${WORKING_BASE}/CO_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/CO/clips" "${WORKING_BASE}/CO_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/CO/clips" "${WORKING_BASE}/CO_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/CT/clips" "${WORKING_BASE}/CT_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/CT/clips" "${WORKING_BASE}/CT_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/9" "${WORKING_BASE}/CT_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/DE/clips" "${WORKING_BASE}/DE_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/DE/clips" "${WORKING_BASE}/DE_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/10" "${WORKING_BASE}/DE_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/FL/clips" "${WORKING_BASE}/FL_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/FL/clips" "${WORKING_BASE}/FL_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/12" "${WORKING_BASE}/FL_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/GA/clips" "${WORKING_BASE}/GA_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/GA/clips" "${WORKING_BASE}/GA_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/13" "${WORKING_BASE}/GA_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/IA/clips" "${WORKING_BASE}/IA_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/IA/clips" "${WORKING_BASE}/IA_30mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/ID/clips" "${WORKING_BASE}/ID_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/ID/clips" "${WORKING_BASE}/ID_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/ID/clips" "${WORKING_BASE}/ID_60mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/16" "${WORKING_BASE}/ID_30mo_NOAA"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_60}/16" "${WORKING_BASE}/ID_60mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/IL/clips" "${WORKING_BASE}/IL_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/IL/clips" "${WORKING_BASE}/IL_30mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/IN/clips" "${WORKING_BASE}/IN_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/IN/clips" "${WORKING_BASE}/IN_30mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/KS/clips" "${WORKING_BASE}/KS_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/KS/clips" "${WORKING_BASE}/KS_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/KS/clips" "${WORKING_BASE}/KS_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/KY/clips" "${WORKING_BASE}/KY_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/KY/clips" "${WORKING_BASE}/KY_30mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/LA/clips" "${WORKING_BASE}/LA_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/LA/clips" "${WORKING_BASE}/LA_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/22" "${WORKING_BASE}/LA_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/MA/clips" "${WORKING_BASE}/MA_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/MA/clips" "${WORKING_BASE}/MA_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/25" "${WORKING_BASE}/MA_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/MD/clips" "${WORKING_BASE}/MD_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/MD/clips" "${WORKING_BASE}/MD_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/24" "${WORKING_BASE}/MD_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/ME/clips" "${WORKING_BASE}/ME_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/ME/clips" "${WORKING_BASE}/ME_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/23" "${WORKING_BASE}/ME_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/MI/clips" "${WORKING_BASE}/MI_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/MI/clips" "${WORKING_BASE}/MI_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/26" "${WORKING_BASE}/MI_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/MN/clips" "${WORKING_BASE}/MN_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/MN/clips" "${WORKING_BASE}/MN_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/27" "${WORKING_BASE}/MN_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/MO/clips" "${WORKING_BASE}/MO_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/MO/clips" "${WORKING_BASE}/MO_30mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/MS/clips" "${WORKING_BASE}/MS_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/MS/clips" "${WORKING_BASE}/MS_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/28" "${WORKING_BASE}/MS_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/MT/clips" "${WORKING_BASE}/MT_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/MT/clips" "${WORKING_BASE}/MT_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/MT/clips" "${WORKING_BASE}/MT_60mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/30" "${WORKING_BASE}/MT_30mo_NOAA"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_60}/30" "${WORKING_BASE}/MT_60mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/NC/clips" "${WORKING_BASE}/NC_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/NC/clips" "${WORKING_BASE}/NC_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/37" "${WORKING_BASE}/NC_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/ND/clips" "${WORKING_BASE}/ND_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/ND/clips" "${WORKING_BASE}/ND_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/ND/clips" "${WORKING_BASE}/ND_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/NE/clips" "${WORKING_BASE}/NE_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/NE/clips" "${WORKING_BASE}/NE_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/NE/clips" "${WORKING_BASE}/NE_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/NH/clips" "${WORKING_BASE}/NH_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/NH/clips" "${WORKING_BASE}/NH_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/33" "${WORKING_BASE}/NH_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/NJ/clips" "${WORKING_BASE}/NJ_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/NJ/clips" "${WORKING_BASE}/NJ_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/34" "${WORKING_BASE}/NJ_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/NM/clips" "${WORKING_BASE}/NM_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/NM/clips" "${WORKING_BASE}/NM_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/NM/clips" "${WORKING_BASE}/NM_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/NV/clips" "${WORKING_BASE}/NV_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/NV/clips" "${WORKING_BASE}/NV_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/NV/clips" "${WORKING_BASE}/NV_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/NY/clips" "${WORKING_BASE}/NY_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/NY/clips" "${WORKING_BASE}/NY_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/36" "${WORKING_BASE}/NY_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/OH/clips" "${WORKING_BASE}/OH_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/OH/clips" "${WORKING_BASE}/OH_30mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/OK/clips" "${WORKING_BASE}/OK_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/OK/clips" "${WORKING_BASE}/OK_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/OK/clips" "${WORKING_BASE}/OK_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/OR/clips" "${WORKING_BASE}/OR_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/OR/clips" "${WORKING_BASE}/OR_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/OR/clips" "${WORKING_BASE}/OR_60mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/41" "${WORKING_BASE}/OR_30mo_NOAA"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_60}/41" "${WORKING_BASE}/OR_60mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/PA/clips" "${WORKING_BASE}/PA_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/PA/clips" "${WORKING_BASE}/PA_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/42" "${WORKING_BASE}/PA_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/RI/clips" "${WORKING_BASE}/RI_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/RI/clips" "${WORKING_BASE}/RI_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/44" "${WORKING_BASE}/RI_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/SC/clips" "${WORKING_BASE}/SC_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/SC/clips" "${WORKING_BASE}/SC_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/45" "${WORKING_BASE}/SC_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/SD/clips" "${WORKING_BASE}/SD_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/SD/clips" "${WORKING_BASE}/SD_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/SD/clips" "${WORKING_BASE}/SD_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/TN/clips" "${WORKING_BASE}/TN_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/TN/clips" "${WORKING_BASE}/TN_30mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/TX/clips" "${WORKING_BASE}/TX_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/TX/clips" "${WORKING_BASE}/TX_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/TX/clips" "${WORKING_BASE}/TX_60mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/48" "${WORKING_BASE}/TX_30mo_NOAA"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_60}/48" "${WORKING_BASE}/TX_60mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/UT/clips" "${WORKING_BASE}/UT_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/UT/clips" "${WORKING_BASE}/UT_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/UT/clips" "${WORKING_BASE}/UT_60mo_USGS"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/VA/clips" "${WORKING_BASE}/VA_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/VA/clips" "${WORKING_BASE}/VA_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/51" "${WORKING_BASE}/VA_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/VT/clips" "${WORKING_BASE}/VT_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/VT/clips" "${WORKING_BASE}/VT_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/50" "${WORKING_BASE}/VT_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/WA/clips" "${WORKING_BASE}/WA_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/WA/clips" "${WORKING_BASE}/WA_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/WA/clips" "${WORKING_BASE}/WA_60mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/53" "${WORKING_BASE}/WA_30mo_NOAA"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_60}/53" "${WORKING_BASE}/WA_60mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/WI/clips" "${WORKING_BASE}/WI_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/WI/clips" "${WORKING_BASE}/WI_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/55" "${WORKING_BASE}/WI_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_EASTERN}/WV/clips" "${WORKING_BASE}/WV_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/WV/clips" "${WORKING_BASE}/WV_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${NOAA_30}/54" "${WORKING_BASE}/WV_30mo_NOAA"

sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_18_WESTERN}/WY/clips" "${WORKING_BASE}/WY_18mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_30_ALL}/WY/clips" "${WORKING_BASE}/WY_30mo_USGS"
sbatch "${DATAPREP}" "${INI}" "${SOURCE_BASE}/${USGS_60_WESTERN}/WY/clips" "${WORKING_BASE}/WY_60mo_USGS"
