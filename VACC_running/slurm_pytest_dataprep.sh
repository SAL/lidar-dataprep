#!/bin/bash

### Slurm header section ###

# Job name
#SBATCH --job-name=pytest-LD

# Partition
# `short`: jobs < 3 hrs; `bluemoon`/`bigmem` < 30 hrs; `week`/`bigmemwk`: < 7 days
#SBATCH --partition=short
# #SBATCH --partition=bluemoon
# #SBATCH --partition=bigmem
# #SBATCH --partition=week
# #SBATCH --partition=bigmemwk

# Walltime (hh:mm:ss)
# End job if hits this to not run afoul of partition guideline
#SBATCH --time=3:00:00
# #SBATCH --time=30:00:00
# #SBATCH --time=7-00:00:00

# Nodes
#SBATCH --nodes=1

# Processes/Tasks per Node
#SBATCH --ntasks=1

# Cores per Process/Task
#SBATCH --cpus-per-task=11

# Memory (default is 1G, for entire job, can specify by entire job --mem=64GB or by cpu --mem-per-cpu=2GB)
#SBATCH --mem=12GB

# Email
#SBATCH --mail-type=ALL


### Executable section ###

# Echo each command to the log file
set -x

# Link log file to one with a preferred name, and in the output folder
ln -f "slurm-$SLURM_JOB_ID.out" "${SLURM_JOB_NAME}_$SLURM_JOB_ID.out"

# Export environment variables
export FME_TEMP=$HOME/scratch/temp_processing/fme_temp


# Run LiDAR Dataprep pytests

cd "${HOME}/code/lidar-dataprep"

# WARNING: If the working folder for pytest is specified with "--basetemp" be
#   careful where this is specified, as pytest will manage this folder,
#   including deleting everything in it beside the current text folder!
#  And so, for one thing, it must not be the same as the TempSpace defined
#  in software_paths.ini

# NOTES:
# 1) Parent folder of BaseTemp folder must exist
# 2) The `--live-stream` option for conda run can be added back when conda is
#      upgraded to v23 across the team (TBD). With this option, the testing
#      progress is written to the OUT file in real time.

# conda run -n dataprep pytest test/test_dataprep_mosaicked_tiles.py --basetemp="${HOME}/scratch/test_working/test_m"
# conda run -n dataprep pytest test/test_dataprep_qc.py --basetemp="${HOME}/scratch/test_working/test_q"
# conda run -n dataprep pytest test/test_run_subprocess.py --basetemp="${HOME}/scratch/test_working/test_s"
conda run -n dataprep pytest --basetemp="${HOME}/scratch/test_working/test_a"
