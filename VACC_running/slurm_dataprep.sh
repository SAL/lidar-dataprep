#!/bin/bash

### Slurm header section ###

# Job name
#SBATCH --job-name=lidar-dataprep

# Partition
# `short`: jobs < 3 hrs; `bluemoon`/`bigmem` < 30 hrs; `week`/`bigmemwk`: < 7 days
# #SBATCH --partition=short
#SBATCH --partition=bluemoon
# #SBATCH --partition=bigmem
# #SBATCH --partition=week
# #SBATCH --partition=bigmemwk

# Walltime (hh:mm:ss)
# End job if hits this to not run afoul of partition guideline
# #SBATCH --time=3:00:00
#SBATCH --time=30:00:00
# #SBATCH --time=7-00:00:00

# Nodes
#SBATCH --nodes=1

# Processes/Tasks per Node
#SBATCH --ntasks=1

# Cores per Process/Task
#SBATCH --cpus-per-task=11

# Memory (default is 1G, for entire job, can specify by entire job --mem=64GB or by cpu --mem-per-cpu=2GB)
#SBATCH --mem=16GB

# Email
#SBATCH --mail-type=ALL

# Rename log file to "<myjob>_<jobid>.out" -- Not working this way, so implemented differently, below
# #SBATCH –-output=%x_%j.out

### Executable section ###

# Echo each command to the log file
set -x

# Link log file to one with a preferred name, and in the output folder
# TODO: Add working folder stem to OUT log file name
ln -f "slurm-${SLURM_JOB_ID}.out" "${SLURM_JOB_NAME}_${SLURM_JOB_ID}.out"

# Export environment variables
export FME_TEMP=$HOME/scratch/temp_processing/fme_temp

# Working directory
#  Needed only if commands that follow (other than the `ln` command) have relative paths
#  Must be on user node (not `scratch`, etc)
cd "${HOME}/code/lidar-dataprep"

# Command-line arguments
INI=$1
SOURCE=$2
WORKING=$3

# Run LiDAR Dataprep
conda run -n dataprep python dataprep.py \
  "${INI}" \
  -s "${SOURCE}" \
  -w "${WORKING}" \
  -r \
  -o
