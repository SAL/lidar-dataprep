INI_BASE=${HOME}/code/lidar-dataprep/workflow_configuration_templates
SOURCE_BASE=/netfiles/salshare/Tools/PythonScripts/Terry/project_data/Mansfield_2014
WORKING_BASE=/netfiles/salshare/Tools/PythonScripts/Terry/workflow_processing
DATAPREP=${HOME}/code/lidar-dataprep/VACC_running/slurm_dataprep.sh

sbatch "${DATAPREP}" \
  "${INI_BASE}/workflow_Mansfield.ini" \
  "${SOURCE_BASE}/station_coverage_laz" \
  "${WORKING_BASE}/Mansfield_2014_laz_station_coverage"
