INI_BASE=${HOME}/code/lidar-dataprep/workflow_configuration_templates
SOURCE_BASE=${HOME}/code/lidar-dataprep/test/external_data/pointclouds
WORKING_BASE=${HOME}/scratch/dataprep_working
DATAPREP=${HOME}/code/lidar-dataprep/VACC_running/slurm_dataprep.sh

sbatch "${DATAPREP}" \
  "${INI_BASE}/workflow_QC_source_pointclouds.ini" \
  "${SOURCE_BASE}/burlington_subset" \
  "${WORKING_BASE}/qc_vacc"
