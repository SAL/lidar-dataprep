#!/bin/bash

### Slurm header section ###

# Job name
#SBATCH --job-name=archive_folder

# Partition
# `short`: jobs < 3 hrs; `bluemoon`/`bigmem` < 30 hrs; `week`/`bigmemwk`: < 7 days
# #SBATCH --partition=short
# #SBATCH --partition=bluemoon
#SBATCH --partition=week

# Walltime (hh:mm:ss)
# End job if hits this to not run afoul of partition guideline
# #SBATCH --time=3:00:00
# #SBATCH --time=30:00:00
#SBATCH --time=7-00:00:00

# Nodes
#SBATCH --nodes=1

# Processes/Tasks per Node
#SBATCH --ntasks=1

# Cores per Process/Task
#SBATCH --cpus-per-task=4

# Memory (default is 1G, for entire job, can specify by entire job --mem=64GB or by cpu --mem-per-cpu=2GB)
#SBATCH --mem=8GB

# Email
#SBATCH --mail-type=ALL

# Rename log file to "<myjob>_<jobid>.out" -- Not working this way, so implemented differently, below
# #SBATCH –-output=%x_%j.out

### Executable section ###

# Echo each command to the log file
set -x

# Command-line arguments
FOLDER=$1

# Link log file to one with a preferred name, and in the output folder
ln -f "slurm-${SLURM_JOB_ID}.out" "${SLURM_JOB_NAME}_${SLURM_JOB_ID}.out"

# Run shell script that archives a folder
bash ~/code/lidar-dataprep/setup/archive_folder.sh "${FOLDER}"
