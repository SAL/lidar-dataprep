INI_BASE=${HOME}/code/lidar-dataprep/workflow_configuration_templates
SOURCE_BASE=/netfiles/sal2/Data_Projects/States/VT/_Statewide/Data/LiDAR/2023/Pilot_20230929
WORKING_BASE=/netfiles/sal2/Data_Projects/States/VT/_Statewide/Data/LiDAR/2023/Pilot_20230929
DATAPREP=${HOME}/code/lidar-dataprep/VACC_running/slurm_dataprep.sh

## QC - as delivered by vendor
#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_2023_QC_source_pointclouds.ini" \
#  "${SOURCE_BASE}/point_cloud/tilecls" \
#  "${WORKING_BASE}/LiDAR_DataPrep/vt_2023_qc_as_delivered/qc02"

## Cleanup pointclouds and QC
#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_2023_Cleanup_and_QC_pointclouds.ini" \
#  "${SOURCE_BASE}/point_cloud/tilecls" \
#  "${WORKING_BASE}/LiDAR_DataPrep/vt_2023_cleanup_and_qc/cleanup_01"

# Balance of the workflow
sbatch "${DATAPREP}" \
  "${INI_BASE}/workflow_VT_2023_mosaic_workflow.ini" \
  "${SOURCE_BASE}/LiDAR_DataPrep/vt_2023_cleanup_and_qc/cleanup_01/step_1_pointclouds_cleaned" \
  "${WORKING_BASE}/LiDAR_DataPrep/vt_2023_balance_of_workflow/workflow_03"
