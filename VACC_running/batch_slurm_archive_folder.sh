SCRIPT=~/code/lidar-dataprep/VACC_running/slurm_archive_folder.sh

FOLDER_BASE=/gpfs1/home/t/c/tcbarret/data/FIA
#FOLDER_BASE=/gpfs2/scratch/tcbarret/data_alt/FIA

#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_1_NRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_1_PNW"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_1_SRS"

#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_2_NRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_2_RMRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_2_SRS"

#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_01_retrievals_NRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_01_retrievals_PNW"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_01_retrievals_RMRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_01_retrievals_SRS"

#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_1_RMRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_3_PNW"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_3_RMRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_4_NRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_4_PNW"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_4_RMRS"
#sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_03_processing_4_SRS"

sbatch "${SCRIPT}" "${FOLDER_BASE}/fia_01_retrievals"
