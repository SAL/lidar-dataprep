INI_BASE=${HOME}/code/lidar-dataprep/workflow_configuration_templates
#SOURCE_BASE=/netfiles/salshare/Data_Projects/States/VT
#SOURCE_BASE=/gpfs2/scratch/tcbarret/source_pointclouds
SOURCE_BASE=/gpfs2/scratch/tcbarret/dataprep_working
WORKING_BASE=${HOME}/scratch/dataprep_working
DATAPREP=${HOME}/code/lidar-dataprep/VACC_running/slurm_dataprep.sh

# QC - all tiles in collects

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_Cleanup_and_QC_source_pointclouds.ini" \
#  "${SOURCE_BASE}/_Statewide/Data/LiDAR/EasternVT_2014/las/original" \
#  "${WORKING_BASE}/Eastern_VT_2014_cleanup_qc_vacc"
#
#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_Cleanup_and_QC_source_pointclouds.ini" \
#  "${SOURCE_BASE}/Connecticut_River_lidar/laz" \
#  "${WORKING_BASE}/Connecticut_River_VT_2016_cleanup_qc_vacc"

# Processing - summit-to-shore subsets of collects

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL.ini" \
#  "${SOURCE_BASE}/Eastern_VT_2014_summit_to_shore_subset" \
#  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01"
#
#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL.ini" \
#  "${SOURCE_BASE}/Connecticut_River_VT_2016_summit_to_shore_subset" \
#  "${WORKING_BASE}/Connecticut_River_VT_2016_CRREL_subset_01"

# Processing - continue after timeout of initial run

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL_continue_lidr_index.ini" \
#  "${SOURCE_BASE}/Eastern_VT_2014_CRREL_subset_01/step_9_pointclouds_noise_dropped_tiled_buffered_height_replaced_classified_buffer_removed" \
#  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_1"
#
#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL_continue_lidr_index.ini" \
#  "${SOURCE_BASE}/Connecticut_River_VT_2016_CRREL_subset_01/step_9_pointclouds_noise_dropped_tiled_buffered_height_replaced_classified_buffer_removed" \
#  "${WORKING_BASE}/Connecticut_River_VT_2016_CRREL_subset_01_continue_1"

# Processing continue - lidR tiles

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL_continue_lidr_tiles.ini" \
#  "${SOURCE_BASE}/Eastern_VT_2014_CRREL_subset_01/step_8_pointclouds_noise_dropped_tiled_buffered_height_replaced_classified" \
#  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_2"

# Processing continue - surface models

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL_continue_qc_classified.ini" \
#  "${SOURCE_BASE}/Eastern_VT_2014_CRREL_subset_01/step_10_pointclouds_noise_dropped_tiled_buffered_height_replaced_classified_buffer_removed_tiling_restored" \
#  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_3"

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL_continue_surface_models_non_normalized.ini" \
#  "${SOURCE_BASE}/Eastern_VT_2014_CRREL_subset_01/step_4_pointclouds_noise_dropped_tiled_buffered" \
#  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_4"

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL_continue_surface_models_non_normalized_A.ini" \
#  "${SOURCE_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_4/step_1_dsm_spike_free_tiles_buffered" \
#  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_4a"

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL_continue_surface_models_non_normalized_B.ini" \
#  "${SOURCE_BASE}/Eastern_VT_2014_CRREL_subset_01/step_4_pointclouds_noise_dropped_tiled_buffered" \
#  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_4b"

#sbatch "${DATAPREP}" \
#  "${INI_BASE}/workflow_VT_CRREL_continue_surface_models_normalized.ini" \
#  "${SOURCE_BASE}/Eastern_VT_2014_CRREL_subset_01/step_8_pointclouds_noise_dropped_tiled_buffered_height_replaced_classified" \
#  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_5"

sbatch "${DATAPREP}" \
  "${INI_BASE}/workflow_VT_CRREL_continue_surface_models_non_normalized_C.ini" \
  "${SOURCE_BASE}/Eastern_VT_2014_CRREL_subset_01/step_4_pointclouds_noise_dropped_tiled_buffered" \
  "${WORKING_BASE}/Eastern_VT_2014_CRREL_subset_01_continue_4c"

sbatch "${DATAPREP}" \
  "${INI_BASE}/workflow_VT_CRREL_continue_surface_models_non_normalized_C.ini" \
  "${SOURCE_BASE}/Connecticut_River_VT_2016_CRREL_subset_01/step_4_pointclouds_noise_dropped_tiled_buffered" \
  "${WORKING_BASE}/Connecticut_River_VT_2016_CRREL_subset_01_continue_4c"
