## Defining a workflow

### Notes on settings related to parallel processing 
The standard way to configure the number of cores for multi-processed steps 
in LD2.0 is by setting the `ParallelProcesses` parameter in 
`software_paths.ini`; the INI no longer needs to refer to cores at all. 
There is no longer a need to specify a "cores" parameter in every 
multiprocessed step and then
change them all when you need to run on a different machine, as was 
required with LD1.7. This new method has the benefit of using the same INI for a 
sample (run on few cores) and a full run (run on many cores), or for 
switching between two machines with different numbers of cores. Note: The 
code reads `software_paths.ini` when a run is initiated, so whatever value of 
`ParallelProcesses` is in it at that time will be used. You can start a run 
with one setting for `ParallelProcesses`, and then change it for another run 
(and start that run while the first is still running), but best not to 
change other settings in `software_paths.ini` this way.

If desired, you can override the `ParallelProcesses` setting for an 
individual step by specifying "cores" in the step -- this is available 
for `LAStools` submodules, and these `FileManagement` submodules: 
`QC_Pointclouds`, `MultiprocessPDAL`, `MultiprocessFusion`, and
`MultiprocessLidr`. 
If this is done, that step will use the specific value of cores you 
specified in the step, and all the others will use the value from 
`ParallelProcesses`.

Additional info for VACC running: When you run on the VACC, the Slurm 
file header section defines the resources that will be allocated to your 
job. This is like setting up a virtual machine for you to run your job 
on. It is good practice to request one more core than LD will use (based on its 
`ParallelProcesses` setting in `software_paths.ini`) so there is one core 
to run the main program, handle file copying, etc. And so: Anytime you 
run a VACC LD job, make sure that that "cpus-per-task" in the Slurm file 
and `ParallelProcesses` in `software_paths.ini` are in alignment, otherwise 
you may be letting requested cores sit idle or overtaxing cores with 
too many processes at once.

### I. Path Step

#### Required Parameters (unless specified in the command-line call)
- `SourceFolder`
  - ex: `SourceFolder = O:\input\pointclouds`
- `WorkingFolder`
  - ex: `WorkingFolder = O:\working\test_01`

Guidelines for these parameters (which also apply to when they are 
specified on the command line):
- WorkingFolder cannot be the same as SourceFolder
- WorkingFolder cannot be in the SourceFolder
- SourceFolder cannot be in the WorkingFolder
- All point clouds in the SourceFolder must have the same exact
  definition for coordinate reference system (CRS)

#### Optional parameters
- `RetainAll`
    - `RetainAll = True`
        - Retains all step result folders, overriding any and all 
        RetainResult settings in individual steps
            - This is the default run behavior if this parameter is 
            not specified
    - `RetainAll = False`
        - Deletes step result folders (for all steps that don't have 
        `RetainResult = True`) after they are no longer needed in the workflow
- `HangCheckInterval`
    - When run on Windows 10, LAStools processes are killed after 30 minutes 
    with no increase in the size of the associated step-result folder; if a 
    different check interval is desired, specify with this parameter (in 
    minutes). 
      - ex: `HangCheckInterval = 10`
    - Set to zero to turn off this monitoring:
        - `HangCheckInterval = 0`
        - This is currently recommend for all running on Windows Server, as this
        functionality is not yet fully implemented on that version of Windows.

### II. Processing Steps

#### Required parameters for each step
- `Module`
  - ex: `Module = LAStools`
- `Submodule`
  - ex: `Submodule = blast2dem`

#### Optional parameters for each step
- `InputDir`
    - If not specified, the input for a step will be the output of the 
    previous step (or the `SourceFolder` if this is the first step)
      - If the output of a step other than the previous is the required 
      input for this step, enter its number (ex: `InputDir = 3`) 
        - `InputDir = 0` will use the `SourceFolder` as input
- `OutputDir`
    - If specified, specify as `OutputDir = DeliveryFolder` and the 
    output will go directly to the `delivery` folder
      - Note: Only use this if the output of this step will not be needed 
      by any following steps
- `ResultName`
    - If not using `OutputDir`, this parameter may be used to add text 
    to the output folder name
- `RetainResult`
    - `RetainResult = True`
        - This step's result folder will always be retained, even when 
        `RetainAll = False` in the Paths section
    - `RetainResult = False` (or this parameter is not specified)
        - This step's result folder will be deleted after it is no 
        longer needed in the workflow, as long as `RetainAll = False`
        is in the Paths section

##### Step numbering
- Start with [1]
- Increase by one with each step

##### Module choices

- `FileManagement` module
    - Submodules: `CopyFiles`, `CopyFolder`, `SetDeliveryTifCRS`, 
    `RenameRaster`, `CalcDeliveryTifStats`, `CalcDeliveryTifPyramids`, 
    `QC_Pointclouds`, `MultiprocessPDAL`, `MultiprocessFusion`, 
    `MultiprocessLidR`
        - `CopyFiles`
          - copies all files from `InputDir` to `OutputDir`
        - `CopyFolder`
          - copies all files from `InputDir` to `OutputDir`, into a folder 
          with the same name as the input folder
        - `SetDeliveryTifCRS` (uses ArcPro and LAStools)
          - Sets TIF files in the Delivery folder (and all its sub-folders) 
          to the CRS of the first viable LAS or LAZ file it 
          finds in the Source (or other specified) folder. If Arc cannot read
          the CRS (and so won't be able to set it properly), the tool exits.
            - Optional additional parameters, for example:
                - `CrsDir = 1`
                  - Specifies a non-SourceFolder workspace directory (by 
                  its step number) to use to find the CRS
          - Note: When using this tool with ArcPro version 3, make sure that 
          the point clouds used for the reference CRS are LAS, not LAZ, 
          otherwise the CRS will not be set correctly. This can be accomplished 
          by having one of the LAStools steps in the workflow output as LAS 
          (using the setting `CompressOutput = False`) and then pointing this
          tool to those LAS pointclouds with the `CrsDir` option.
        - `RenameRaster` (uses ArcPro)
          - Note: May not work with files in synced or remote folders; use 
          with local folders
            - Required parameters, for example:
                - `InputFilename = dem_.tif`       
                - `OutputFilename = dem_3m.tif`   
        - `CalcDeliveryTifStats` (uses ArcPro)
            - Calculates statistics for all TIF files in the Delivery 
            folder (and all its sub-folders). Before each file is processed 
            it is checked to see if Arc can read its CRS; if it cannot, the 
            tool exits.  
        - `CalcDeliveryTifPyramids` (uses ArcPro)
            - Calculates pyramids for all TIF files in the Delivery 
            folder (and all its sub-folders). Before each file is processed 
            it is checked to see if Arc can read its CRS; if it cannot, the 
            tool exits.
        - `QC_Pointclouds` (uses LAStools)
            - Reports the `lasinfo` output for each pointcloud file; 
            summarizes the `lasinfo` metrics for 
            the set of files; performs and reports additional quality 
            checks (check if LASversion/CRS match, 
            value statistics, missing classifications, etc.); and joins 
            the file-by-file quality metrics to a 
            pointcloud index shapefile, if an index has been generated 
            in a previous step (and the step is 
            specified with `InputIndex`)
            - Optional additional parameters, for example:
                - `cores = 7`
                - `InputIndex = 2`
        - `MultiprocessPDAL` (uses PDAL)
          - Required parameters, for example:
            - `Pipeline = Ground` 
              - The JSON file that defines the ground filter, located in 
              `src/workspaces/pdal/Ground.json`, can be modified as desired 
              before running a workflow that utilizes it. For more info on 
              developing PDAL JSON pipelines, consult the PDAL user manual.
              - Other pipeline JSONs can be created, placed in the 
              `src/workspaces/pdal` folder, and selected 
              for processing using this parameter
          - Optional additional parameters, for example:
            - `Cores = 5`
        - `MultiprocessFusion` (implements Fusion's GridMetrics tool; 
        currently works only on Windows; more Fusion tools may be added 
        in future)
          - This implementation of the GridMetrics tool expects 
          height-normalized point clouds as input. Non-height-normalized 
          point clouds will run through the tool, but many of the resulting 
          metrics will be meaningless. A more general implementation of 
          the tool can be developed if there is interest.
          - Required parameters, for example (for more info on these, 
          consult the Fusion user manual):
            - `HeightBreak` (in point cloud vertical units)
              - Height break for cover calculation
            - `CellSize` (in point cloud horizontal units)
              - Grid size for metric abstraction
          - Optional additional parameters, for example:
            - `Cores = 5`
        - `MultiprocessLidR` (calculates lidR canopy metrics; currently 
        works only on Linux)
          - This implementation of the lidR expects height-normalized 
          point clouds as input. 
          Non-height-normalized point clouds will run through the tool, 
          but many of the resulting metrics 
          will be meaningless. A more general implementation of the tool 
          can be developed if there is interest.
          - Required parameters, for example:
            - `CellSize` (in point cloud horizontal units)
              - Grid size for metric abstraction
          - Optional additional parameters, for example:
            - `Cores = 5`

- `LAStools` module
    - Submodules: `las2las`, `lasheight`, `lasclassify`, `lastile`, `blast2dem`, 
    `las2dem`, `lasindex`, `lassplit`, `lasground_new`
        - OS implementation variations
          - `blast2dem` is currently Windows only, as there is no 64bit 
          version for Linux.
          - `lasindex` and `lastile` currently always run on one core on Linux, 
          no matter how many are requested; this is due to the multi-core 
          aspect of these tools being managed by a 32bit process.
        - Additional parameters 
            - `InputFilter = (las/laz)`
            - `CompressOutput = (True(default)/False)`
                - Not required for `lasindex`, `blast2dem`, `las2dem`
            - Any additional LAStools arguments, for example: (See each 
            LAStool's README for allowable additional arguments)
                - `drop_classification = 7`
                - `drop_classification = 7 18`
                - `cores = 16`
                - `replace_z =` (arguments with no value must have '=' after)
                - `kill = 30`
                  - Note: This argument, which thresholds TIN 
                  triangle size when rastering with `las2dem` or 
                  `blast2dem`, is always in meters regardless of the CRS
                - If need to specify the same LAStool argument twice 
                in a single step:
                  - Prefix each usual argument line with 
                  `<unique-characters>|`, for example:
                    - To reclassify two classes in a single `las2las` step:
                      - `1|change_classification_from_to = 0 1`
                      - `2|change_classification_from_to = 8 2`
            - Notes: 
                - If the horizontal and/or vertical units are undefined, 
                or ambiguously defined, in the input pointclouds, 
                some LAStools (`lastile`, `lasclassify`, `lasheight`, etc) will 
                not function properly. In this case, additional arguments 
                to specify the horizontal and vertical units may be required; 
                see the each tool's README for which arguments 
                to specify. Note: For `lasclassify` use the arguments 
                (`feet =`, `elevation_feet =`) for both feet and survey_feet
                 - When tiling/surface-modeling, specify the tile and 
                buffer size as multiples of the surface-modeled cell 
                size. For example: 3000ft tile with 300ft buffer for tiles 
                that will be used to generate a 3ft raster.
                                
- `FME` module
    - Submodules: `Mosaic_DEM`, `Mosaic_DSM`, `Mosaic_DTM`, `Mosaic_INT`, 
    `Pointcloud_Tile_Index`, `Tile_Pointclouds`, 
    `Tile_Delivery_Imagery_TIF`, `LAZ_WebMercator`
        - `Mosaic_DEM`, `Mosaic_DSM`, `Mosaic_DTM`, `Mosaic_INT`
            - All Mosaic submodules do the same mosaic process, they 
            only differ in how they name the output mosaic
            - Mosaic submodules currently mosaic TIF rasters        
        - `Pointcloud_Tile_Index `    
            - Creates a shapefile index of the input pointcloud tiles, 
            attributed with their CRS and filenames
        - `Tile_Pointclouds`, `Tile_Delivery_Imagery_TIF`
            - Required parameters, for example:
                - `InputIndex = 2`
            - `Tile_Pointclouds`
                - Re-tiles pointclouds generated in the workflow (such as 
                buffered/classifed/debuffered tiles from LAStools 
                steps) using a shapefile index generated in the workflow 
                (by `Pointcloud_Tile_Index`). The new tiles are 
                named from the `Name` attribute in the shapefile, which are 
                the names of the originally-indexed pointcloud files
            - `Tile_Delivery_Imagery_TIF`
                - Required parameters, for example:
                    - `ImageryFile = dem_`
                - Tiles a mosaic TIF in the Delivery folder using a 
                shapefile index generated in the workflow (by 
                `Pointcloud_Tile_Index`) 
        - `LAZ_WebMercator`
          - Adds Web Mercator CRS to LAZ pointclouds 
 