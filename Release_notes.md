## Release 2.0.0
#### High-level changes
- Codebase now runs on Linux in addition to Windows (Tested on RedHat 7.9, 
Windows 10, and Windows Server 2016)
- Codebase now runs on high-performance clusters (such as those of the VACC) 
in addition to desktop
- Codebase has been updated and tested with the more recent versions of the 
required programs (see `setup/Software_dependencies.md`)
  
#### Run-configuration changes
- Software paths and number-of-parallel-processes (for most LAStools and some 
File Management submodules) are now read from a new file `software_paths.ini`, 
with automatic detection of 
runtime system and so selection of correct set of paths/processes; Process 
count can still be set in individual steps, overriding the general number
- The main script, `dataprep.py`, now has additional optional command-line 
arguments (all required for running on the VACC):
    - Flag to skip workflow review
    - Flag to skip question to user about overwriting working folder
    - SourceFolder path (overrides any defined in workflow INI)
    - WorkingFolder path (overrides any defined in workflow INI)
- Workflow configurations 
  - The SourceFolder can now be used as input to steps after step 1, with 
  specification of `InputDir = 0`
  - LAStools-hanging-monitoring (enabled by default for Windows running) 
  can be turned off by specifying `HangCheckInterval = 0`  in the Paths section
  - New template configs added for an isolated-tile, tree-canopy workflow 
  (based on the Forest Service workflow), one that demonstrate the 
  improvements to the quality tool, and template configs covering all 
  submodules in the application, old and new.

#### FileManagement module 
- QC_Pointclouds
    - Added option to join the quality metrics to a pointcloud-index shapefile
    - Made robust to pointcloud files which are header only (no points) 
    and to those that result in non-standard characters in `lasinfo` reports
    - Added to CSV reporting: min/max/range for X,Y,Z of each file
    - Added to LOG reporting: warning messages found in lasinfo reports
- MultiprocessFusion (new) - Calculates canopy metrics using the Fusion 
GridMetrics tool (Windows only)
- MultiprocessLidR (new) - Calculates canopy metrics using the lidR 
tool (Linux only)
- MultiprocessPDAL (new, for development purposes only, not ready for 
production)
- Removed two submodules that are no longer used - PulseSpacing, TIN_EdgeLength
  - Both these tools were used to find a starting point for the las2dem 
  spike_free parameter, but the typical way the starting point is found in 
  production is by multiplying the quality report's average pulse spacing 
  for the collect by three, and so these tools are no longer needed.
- SetDeliveryTifCRS, CalcDeliveryTifStats, and CalcDeliveryTifPyramids 
now detect if Arc cannot read a tool-relevant CRS (the CRS of a 
reference pointcloud for SetDeliveryTifCRS or the CRS of each Delivery TIF 
for the other two tools), message this fact, and stop the processing of the 
tool so that non-useful (and often lengthy) processing is not continued.

#### LAStools module
- Multiple instances of the same LAStools argument in a single step is now 
supported (useful, for example, to reclassify more than one class in a 
`las2las` step)
  
#### FME module
- LAZ_WebMercator (new) - Adds Web Mercator CRS to LAZ pointclouds, needed 
because LAStools does not implement Web Mercator
- Removed five submodules that are no longer used - Tile_External_Imagery_IMG,
Tile_External_JP2_Imagery_Tiles_LeafOn, Tile_External_JP2_Imagery_Tiles_LeafOff,
Tile_External_TIF_Imagery_Tiles_LeafOn, Tile_External_TIF_Imagery_Tiles_LeafOff
- The 80-character limit for the working folder path has been eliminated. A 
warning remains when the working folder path exceed this, to continue to 
gather evidence that this limit is no longer needed in any circumstance.
- FME logging is now redirected to the LiDAR Dataprep log

#### Reporting
- A new reporting script, `combine_quality_reports.py`, that collects all 
quality reports in a project into one report and optionally joins this to 
a tile metadata table, such as that produced by the USGS Downloader application.
- Additional post-processing and analysis scripts from a recent Forest 
Service project, provided as a starting point for similar processing that 
may be needed on future projects.

#### Automated testing
- The automated testing suite now has full INI-testing coverage of all 
submodules in the application, as well as significant testing coverage of 
individual code components. 


## Release 1.7.0
- FME module: Tile_External_TIF_Imagery_Tiles_LeafOn & 
Tile_External_TIF_Imagery_Tiles_LeafOff added - for use in
tiling external, tiled leaf-on/leaf-off (TIF) imagery to the source 
pointcloud tiling scheme
- Added an automated unit-testing harness and a number of tests
- Bug fix: Fixed a bug that prevented some FileManagement tools (and the 
Task Killer monitoring process) from working when dataprep.py was run from 
the command line. A side benefit of the fix is that dataprep.py can now be 
run on the command line from any directory; one no longer needs to cd to the 
codebase root before initiating a command-line run.
- Codebase updated and tested with the latest versions of the required 
programs: LAStools 27Jan2019, ArcPro 2.3.0, FME 2018.1.1.2

## Release 1.6.4
- FileManagement module: QC_Pointclouds - Now more robustly finds and 
reports both types of projection attribution: 1) VLR (LAS 1.0-1.3 
and "legacy" 1.4) and 2) EVLR ("actual" LAS 1.4)
- FileManagement module has been refactored to provide for easier 
debug/extension of existing tools and to facilitate addition of new tools
- Added steps to the OH workflow template to copy the final 
normalized/classified pointclouds to the delivery folder
- LAStools module: White-listed the lasground_new tool (as 64bit) for 
trial use in the Measure project

## Release 1.6.3
- LAStools module: Submodules are now run in temporary working directories, 
and so intermediate processing files are no longer created (and orphaned on 
interrupt) in the codebase's root folder
- LAStools module: Capability added to automatically kill hanging 
LAStools processes after a (selectable) amount of time has passed without 
change in the associated step-result folder

## Release 1.6.2
- FileManagement module: QC_Pointclouds - Added a fourth projection type
- Capability added to optionally delete selected (or all) step-result 
folders after they are no longer needed in the workflow, reducing 
working-folder disk space

## Release 1.6.1
- LAStools module: las2las, lasclassify, lasheight, lastile, lasindex, 
lassplit upgraded from 32bit to 64bit execution, improving performance of 
these tools
- LAStools module: Verified that blast2dem (still 32bit) still handles 
larger tiles than las2dem (now 64bit) and still does not have las2dem's 
spike-free option
- FileManagement module: QC_Pointclouds - added analysis of extended classes
- FileManagement module: QC_Pointclouds and PulseSpacing's use of lasinfo 
upgraded from 32bit to 64bit execution, improving performance of these tools
- Capability added for a seam-free, void-less (kill=10000) surface-modeling 
workflow; see example usage in the MedinaCountyOH and 
workflow_NYC_QuantumSpatial_2017 template workflows 
- Capability added for Direct-to-nDSM; see the new WesternVT_Addison workflow
- Improved reporting by moving the stderr and stdout messages from the 
CSV report to a new log-file report
- Improved in-progress status messaging by displaying the module and tool 
that are currently running, and by moving other step setup details to the 
log file
- Further improved validation of source and working folder locations - the 
code now raises an exception if the source folder is in the working folder
- Added a reminder to the README to make tile/buffer sizes even multiples 
of the raster cell size to help ensure best surface model results
- Added ignore_class argument to lasclassify in relevant template 
workflows so that bridge_deck, etc. is not reclassified to building

## Release 1.6.0
- FileManagement module: QC_Pointclouds - Refactored to enable 
multi-core processing with lasinfo, speeding up this tool
- FileManagement module: QC_Pointclouds - Added a third projection type
- Improved validation of working folder location - the code now raises an 
exception if the working folder is in the source folder
- Codebase updated and tested with the latest versions of the required 
programs - LAStools 17Sept2018, ArcPro 2.2.2, FME 2018.1

## Release 1.5.1
- Updated README to emphasize that when running this codebase through 
Windows Command, one must first cd into the root directory of the codebase
- Bug fix: Changed the variables that hold point counts in the 
QC_Pointclouds tool to int64 to allow for very large counts

## Release 1.5.0
- FME module: Improved DEM, DTM, DSM mosaic'ers by doing Average instead 
of Last in overlap regions, thereby reducing the magnitude of occasional 
seamline effects at the boundaries of intermediate-processing tiles
- FileManagement module: QC_Pointclouds added - Parses and reports the 
lasinfo output for each pointcloud file; summarizes values for the set of 
files; and performs and reports on some QC operations (check if LASversion/CRS
match, value statistics, missing classifications, etc.)

## Release 1.4.0
- FileManagement module: RenameRaster added
- FileManagement module: CalcDeliveryTifStats added
- FileManagement module: CalcDeliveryTifPyramids added
- FileManagement module: SetDeliveryTifCRS improved to also set the CRS 
in subfolders of the Delivery folder
- FileManagement module: PulseSpacing added - calculates pulse spacing of 
merged pointclouds
- FileManagement module: TIN_EdgeLength added - calculates 99th percentile 
last-return TIN edge length of merged pointclouds
- FME module: Tile_External_JP2_Imagery_Tiles_LeafOn and 
Tile_External_JP2_Imagery_Tiles_LeafOff added - for use in tiling external, 
tiled leaf-on/off imagery to the source pointcloud tiling scheme
- Verified workflow configuration added for SeattleWA, for a total of 
four verification/template workflows of validation datasets
- "Spike-free DSM" workflow configurations added (covering all four 
validation datasets) which use the new FileManagement tools to provide 
support for choosing the appropriate freeze value
- Template INI files are now in their own project folder and the 
parsing function is now in /src, cleaning up the project structure
- Improved validation of parameters specified in LAStools steps
- Improved capture and reporting of errors and messages from arcpy in 
FileManagement tools
- Improved the workflow configuration files of the four validation 
datasets so they leverage the new codebase capabilities
- Bug fix to correct a file copying issue when dataprep.py and the INI 
are in different folders

## Release 1.3.0
- LAStools module: lasindex added - for use before a multi-core, 
non-"remove_buffer" lastile step, to speed up processing
- LAStools module: las2dem added - for use if don't have the extra license 
for blast2dem
- LAStools module: lassplit added - for use in restoring pointcloud tiling 
after (single-core) use of lastile with -faf
- FME module: Pointcloud_Tile_Index added - for use in restoring pointcloud 
tiling
- FME module: Tile_Pointclouds added - for use in restoring pointcloud tiling
- FME module: Tile_Delivery_Imagery_TIF added - for use in tiling surface 
models to pointcloud tiling
- FME module: Tile_External_Imagery_IMG added - for use in tiling external 
mosaics of leaf-on/off imagery to pointcloud tiling
- Verified workflow configurations added for EasternVT_Burlington (full 
tiling workflow), MedinaCountyOH, and SanDiegoCountyCA
- The new FME submodules, coupled with a new buffer/classify/model/debuffer 
configuration of the current LAStools submodules, enables a workflow that 
can produce classified pointclouds, surface models, and leaf-on/off imagery on
the input pointcloud tiling scheme, all without the edge effects of the POR 
workflow

## Release 1.2.0
- FileManagement module: SetDeliveryTifCRS added - sets the CRS of the 
TIF mosaics in the Delivery folder to that of the point clouds in the 
Source (or other specified) folder
- FileManagement module: CopyTree renamed to CopyFiles
- FileManagement module: CopyFolder added
- Bug fix regarding folder generation

## Release 1.1.0
- Simplified folder specification - only two folders need specification (source 
and working)
- Automatic validation, generation, and naming of working folders
- Option to create and use a "delivery" folder
- Application validation (to ensure all required applications exist before 
starting the run)
- Reset of workspace
- C: drive protection warning
- FME workspaces incorporated into the codebase (so tracked under version 
control and so no longer need to specify them in the INI file)
- Processing time is reported (total, and for each processing step)
- All communication from each process (both stdout and stderr) is captured 
and reported
- A copy of the INI file used for a run is put in the working folder with 
the version number of this codebase needed to rerun it added to the file 
name (for documentation and reproducibility)
- A CSV report is written to the working folder with all information from 
the run (for documentation and debugging)
- Validation that minimum parameters are specified in each section

## Release 1.0.0
- Workflow configuration with INI file that runs on a single script
- Function library for module types and running subprocesses
- Feedback on configuration of each step
