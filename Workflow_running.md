## Running a Workflow

### Parallel Processing
See "Notes on settings related to parallel processing" on 
the [Workflow Definition](./Workflow_definition.md) page

### Running a workflow on Windows
1. In a text editor:
   - Create a new workflow configuration INI file, or copy 
   and edit from a template workflow included in this codebase (see folder 
   `workflow_configuration_templates`)
     - Guidance on this: [Defining a workflow](Workflow_definition.md)
2. In a terminal:
    - Activate the `dataprep` conda environment
      - `> conda activate dataprep`
    - Run the workflow on the environment: 
      - A reference for specifying LiDAR Dataprep command-line calls is 
      available from the command line as `> python dataprep.py --help`
        - Ex: `(dataprep) C:\Users\tcbarret\PycharmProjects\lidar-dataprep> python dataprep.py --help`
        ```
        python dataprep.py --help
        
        usage: dataprep.py [-h] [-r] [-o] [-s SOURCE_FOLDER] [-w WORKING_FOLDER]
                           workflow_config_filepath
        
        Run LiDAR Dataprep
        
        positional arguments:
          workflow_config_filepath
                                Filepath of a workflow configuration INI file
        
        optional arguments:
          -h, --help            show this help message and exit
          -r, --skip_review     Add this flag to skip user review of interpreted
                                workflow
          -o, --skip_ask_overwrite
                                Add this flag to skip user-authorized overwrite
          -s SOURCE_FOLDER, --source_folder SOURCE_FOLDER
                                Absolute path of folder of input pointcloud LAS/LAZ
                                files
          -w WORKING_FOLDER, --working_folder WORKING_FOLDER
                                Absolute path of folder in which to output
        ```
   - Example with paths specified in the command-line call, and with the 
   optional flags "skip workflow review" and "skip asking about overwriting": 
     - `(dataprep) C:\Users\tcbarret\PycharmProjects\lidar-dataprep> python dataprep.py workflow_configuration_templates\workflow_QC_source_pointclouds.ini -r -o -s E:\dataprep_2p0_testing\pointclouds\burlington_subset -w E:\dataprep_2p0_testing\working\qc01` 
   - Same example, but in LiDAR Dataprep 1.7 style, using the paths specified 
   in the INI: 
     - `(dataprep) C:\Users\tcbarret\PycharmProjects\lidar-dataprep> python dataprep.py workflow_configuration_templates\workflow_QC_source_pointclouds.ini` 

- While the workflow runs, feedback from each step with be shown on the 
screen; this information is also written to a CSV report in the root of 
the working directory that was specified.
- A copy of the input INI file is saved to the working directory, with 
the version of the codebase appended to the name; this can be used for 
reference, and also to reproduce the workflow results at a later date 
if desired.
- If an error occurs, refer to the last line of the error message. This 
will be a custom message from the codebase, or a message from Python if 
there is no custom message, advising what the problem is. Above that last 
line is the "trace back", which shows the lines of code immediately 
preceding the line of code that experienced the error. Edit the INI file 
accordingly and re-run the workflow as above; if the problem persists, 
report the issue to the codebase developers.

### Running a workflow on a Linux virtual machine (VM)
1. Start the VM
   - `> cd` to the root of this codebase
   - `> vagrant up`
2. Enter the VM
   - `> vagrant ssh`
3. `cd` to the root of the codebase
   - `$ cd /vagrant`
4. Activate the `dataprep` environment
   - `$ conda activate dataprep`
5. Run a workflow on the environment
   - `$ python dataprep.py workflow_configuration_templates/workflow_QC_source_pointclouds.ini -r -o -s /vagrant/Edrive/dataprep_2p0_testing/pointclouds/burlington_subset -w /vagrant/Edrive/dataprep_2p0_testing/working/qc01_vm` 

### Running a workflow on the VACC
1. Create and test a workflow on the VM - or on Windows, if don't access 
to a VM.
2. Update the "Slurm header section" of the Slurm-dataprep script 
`VACC_running/slurm_dataprep.sh` with modified resource requests, etc
3. Modify `VACC_running/batch_slurm_dataprep.sh` to configure one or more 
Dataprep runs
4. Upload the above two files to the VACC
5. `> ssh` to a user node on the VACC
6. `$ cd` to directory to run from, ex: `cd ~/batch_working`
   - Must be on the user node (and not on `scratch`, etc.)
   - This is where the OUT log files will be written
   - Good practice is to use a folder not used for anything else but 
   batch running
7. Submit the Dataprep jobs to the cluster for processing
   - `$ bash ../code/lidar-dataprep/VACC_running/batch_slurm_dataprep.sh`

Note: The first time a user runs Arc on a VACC node all its libraries are copied 
locally to the node, and they are big, so this takes a while. Next time on the 
same node Arc executes much faster.  This is sometimes reset as well, 
likely when maintenance/cleanup is done on the cluster. You may specify a 
specific node in your Slurm parameters, and so intentionally use 
the same node again to avoid the copy time (as long as the node is free, 
otherwise your job will be queued until the node is free).
